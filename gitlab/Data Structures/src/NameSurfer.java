
/*
 * Student information for assignment: Replace <NAME> in the following with your
 * name. You are stating, on your honor you did not copy any other code on this
 * assignment and have not provided your code to anyone. 
 * 
 * On my honor, Mark Grubbs, this programming assignment is my own work 
 * and I have not provided this code
 * to any other student. 
 * UTEID: msg2772
 * email address: siegbalicula@gmail.com 
 * Number of slip days I am using:
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class NameSurfer {

    // CS314 students, explain your menu option 7 here:
    // Before CS312 ended, I made a generator for generating names not by using
    // whole Strings of names but instead by using chars to piece together a name.
    // This would result in some interesting names like Aopixis or Aevii. The way
    // the generator worked was by creating a string that had alternating
    // possibilities of having a consonant appended to it. It would start with the
    // name having a chance and then the name not have the ability. The more common
    // pattern in the names would have been CVCVCV... and names could be as long as
    // specified. By making the method that finds the names with more vowels than
    // consonants or vice versa, I could use the data from the simple search to
    // refine the way I would generate names. The method for menu option 7 gets
    // all the name records of names with more vowels or more consonants in them.
    // CS314 students, Explain your interesting search / trend here:
    // The name Xzavier, ranked in the top 1000 only once in the decade 2000, is
    // interesting. Based on web searches on the name, the name is unlike some
    // other one off exotic names, like Rock, in that no famous person's name is
    // really attached to the name other than a football player. Additionally,
    // websites that "tell the meaning" of names state that it is an
    // Americanization of the Spanish name "Xavier." There are probably
    // many names like this and they may also grow with time. One conclusion on
    // why this name is popular could be that certain fictional characters have
    // the names Xavier or Zavier, with Xavier being pronounced Xzavier, and people
    // wanted their baby to have a name phoenetically equivalent.
    // CS314 students, add test code for NameRecord class here:
    public static void NameRecordTest() {
        String jeffrey = "mynameisjeff 0 0 0 0 0 1 0 0 0 0"; // decade 2010 has this be popular
        int baseDecade = 1960;
        NameRecord jeff = new NameRecord(jeffrey, baseDecade); // test constructor
        int expectedBestDecade = 2010;
        System.out.println("expected bestDecade " + expectedBestDecade);
        System.out.println("actual bestDecade " + jeff.bestDecade());
        if (jeff.bestDecade() == expectedBestDecade) {
            System.out.println("passed bestDecade test");
        } else {
            System.out.println("failed bestDecade test");
        }
        System.out.println("expected onlyRankedTop1000Once " + true);
        System.out.println("actual onlyRankedTop1000Once " + jeff.onlyRankedTop1000Once());
        if (jeff.onlyRankedTop1000Once()) {
            System.out.println("passed onlyRankedTop1000Once test");
        } else {
            System.out.println("failed onlyRankedTop1000Once test");
        }
        jeffrey = "Jeff 1 2 3 4 5 21 22 25 161 177";
        jeff = new NameRecord(jeffrey, baseDecade);
        String expected = "Jeff\n1960: 1\n1970: 2\n1980: 3\n1990: 4\n2000: 5\n2010: 21\n2020: 22\n2030: 25\n2040: 161\n2050: 177\n";
        System.out.println("expected toString\n" + expected);
        System.out.println("actual toString\n" + jeff.toString());
        if (expected.equals(jeff.toString())) {
            System.out.println("passed toString test");
        } else {
            System.out.println("failed toString test");
        }
        System.out.println("expected isLosingPopularity " + true);
        System.out.println("actual isLosingPopularity " + jeff.isLosingPopularity());
        if (true && jeff.isLosingPopularity()) {
            System.out.println("passed isLosingPopularity test");
        } else {
            System.out.println("failed isLosingPopularity test");
        }
        System.out.println("expected isGettingPopularity " + false);
        System.out.println("actual isGettingPopularity " + jeff.isGettingPopularity());
        if (!(jeff.isGettingPopularity())) {
            System.out.println("passed isGettingPopularity test");
        } else {
            System.out.println("failed isGettingPopularity test");
        }
        String actual = jeff.getName();
        expected = "Jeff";
        System.out.println("expected getName " + expected);
        System.out.println("actual getName " + actual);
        if (actual.equals(expected)) {
            System.out.println("passed getName test");
        } else {
            System.out.println("failed getName test");
        }
        int expectedTop1000 = 10;
        System.out.println("expected numTimesTop1000 " + expectedTop1000);
        System.out.println("actual numTimesTop1000 " + jeff.numTimesTop1000());
        if (expectedTop1000 == jeff.numTimesTop1000()) {
            System.out.println("passed numTimesTop1000 test");
        } else {
            System.out.println("failed numTimesTop1000 test");
        }
        System.out.println("expected alwaysRankedTop1000 " + true);
        System.out.println("actual alwaysRankedTop1000 " + jeff.alwaysRankedTop1000());
        if (jeff.alwaysRankedTop1000()) {
            System.out.println("passed alwaysRankedTop1000 test");
        } else {
            System.out.println("failed alwaysRankedTop1000 test");
        }
        String caits = "Cait 10 10 10 10 1 10 10 1 1 4";
        NameRecord cait = new NameRecord(caits, baseDecade);
        boolean expectedCompareTo = true;
        boolean actualCompareTo = jeff.compareTo(cait) > 0;
        System.out.println("expected compareTo " + expectedCompareTo);
        System.out.println("actual compareTo " + actualCompareTo);
        if (actualCompareTo) {
            System.out.println("passed compareTo test");
        } else {
            System.out.println("failed compareTo test");
        }
    }

    //namesTest test data: 
//    1900
//    10
//    Jupiter 641 883 664 955 0 0 0 19 8 4
//    Mars 13 0 0 0 0 0 0 0 0 0
//    Mmrnmhrm 1 0 0 0 0 0 0 0 0 0
//    Sky 123 123 123 123 123 123 123 123 123 123
//    Loser 0 0 0 0 0 0 0 0 0 0
//    Duck 311 41 23 4 53 123 345 678 4 234
//    Sara 876 451 432 234 232 222 212 112 101 58
//    Aras 98 130 200 345 346 414 514 556 653 997
    public static void namesTest() throws FileNotFoundException {
        Scanner fileScanner = new Scanner(new File("smolnames.txt"));
        Names names = new Names(fileScanner);
        testMethods(names);
    }

    public static void testMethods(Names names) {
        System.out.println(names);
        System.out.println(names.rankedEveryDecade()); // expect 4 names; returns 4 names
        System.out.println(names.rankedOnlyOneDecade()); // expect 2 names; returns 2 names
        System.out.println(names.alwaysLessPopular()); // expect Aras; returns Aras
        System.out.println(names.alwaysMorePopular()); // expect Sara; returns Sara
        System.out.println(names.getMatches("ra")); // returns Sara and Aras NameRecords
        System.out.println(names.getName("Mmrnmhrm")); // returns Mmrnmhrm's record
        System.out.println(names.namesWithMoreVowelsOrConsonants(0)); // expected nothing; returns empty list
        System.out.println(names.namesWithMoreVowelsOrConsonants(1)); // expected all names; returns list with all names
    }

    // A few simple tests for the Names and NameRecord class.
    public static void simpleTest() {
        // raw data for Jake. Alter as necessary for your NameRecord
        String jakeRawData = "Jake 262 312 323 479 484 630 751 453 225 117 97";
        int baseDecade = 1900;
        NameRecord jakeRecord = new NameRecord(jakeRawData, baseDecade); // complete with your constructor
        String expected = "Jake\n1900: 262\n1910: 312\n1920: 323\n1930: 479\n1940: "
                + "484\n1950: 630\n1960: 751\n1970: 453\n1980: 225\n1990: 117\n2000: 97\n";
        String actual = jakeRecord.toString();
        System.out.println("expected string:\n" + expected);
        System.out.println("actual string:\n" + actual);
        if (expected.equals(actual)) {
            System.out.println("passed Jake toString test.");
        } else {
            System.out.println("FAILED Jake toString test.");
        }

        // Some getName Tests

        Names names = new Names(getFileScannerForNames("names.txt"));
        String[] testNames = { "Isabelle", "isabelle", "sel", "X1178", "ZZ", "via", "kelly" };
        boolean[] expectNull = { false, false, true, true, true, true, false };
        for (int i = 0; i < testNames.length; i++) {
            performGetNameTest(names, testNames[i], expectNull[i]);
        }
    }

    private static void performGetNameTest(Names names, String name, boolean expectNull) {
        System.out.println("Performing test for this name: " + name);
        if (expectNull) {
            System.out.println("Expected return value is null");
        } else {
            System.out.println("Expected return value is not null");
        }
        NameRecord result = names.getName(name);
        if ((expectNull && result == null) || (!expectNull && result != null)) {
            System.out.println("PASSED TEST.");
        } else {
            System.out.println("Failed test");
        }
    }

    // main method. Driver for the whole program
    public static void main(String[] args) throws FileNotFoundException {
        // NameRecordTest();
        //namesTest();
        final String NAME_FILE = "names.txt";
        Scanner fileScanner = getFileScannerForNames(NAME_FILE);
        Names namesDatabase = new Names(fileScanner);
        fileScanner.close();
        runOptions(namesDatabase);
    }

    // pre: namesDatabase != null
    // ask user for options to perform on the given Names object.
    // Creates a Scanner connected to System.in.
    private static void runOptions(Names namesDatabase) {
        Scanner keyboard = new Scanner(System.in);
        MenuChoices[] menuChoices = MenuChoices.values();
        MenuChoices menuChoice;
        do {
            showMenu();
            int userChoice = getChoice(keyboard) - 1;
            menuChoice = menuChoices[userChoice];
            if (menuChoice == MenuChoices.SEARCH) {
                search(namesDatabase, keyboard);
            } else if (menuChoice == MenuChoices.ONE_NAME) {
                oneName(namesDatabase, keyboard);
            } else if (menuChoice == MenuChoices.APPEAR_ONCE) {
                appearOnce(namesDatabase);
            } else if (menuChoice == MenuChoices.APPEAR_ALWAYS) {
                appearAlways(namesDatabase);
            } else if (menuChoice == MenuChoices.ALWAYS_MORE) {
                alwaysMore(namesDatabase);
            } else if (menuChoice == MenuChoices.ALWAYS_LESS) {
                alwaysLess(namesDatabase);
            } else if (menuChoice == MenuChoices.STUDENT_SEARCH) {
                moreVowelsOrConsonants(namesDatabase, keyboard);
            }
        } while (menuChoice != MenuChoices.QUIT);
        keyboard.close();
    }

    // pre: namesDatabase != null
    // returns all the names with more vowels or more consonants depending on what
    // the client wants
    // this code also checks to make sure that the client enters a value of 0 or 1
    private static void moreVowelsOrConsonants(Names namesDatabase, Scanner keyboard) {
        if (namesDatabase == null) {
            throw new IllegalArgumentException("The parameter namesDatabase cannot be null");
        }
        System.out.print("Enter a value 0 or 1; 0 is more vowels, 1 is more consonants: ");
        int response = keyboard.nextInt();
        System.out.println();
        while (response != 0 && response != 1) {
            System.out.println("That is not a valid option.");
            System.out.print("Enter a value 0 or 1; 0 is more vowels, 1 is more consonants: ");
            response = keyboard.nextInt();
            System.out.println();
        }
        ArrayList<NameRecord> letterDomination = namesDatabase.namesWithMoreVowelsOrConsonants(response);
        System.out.println(letterDomination.size() + " names match that criteria.");
        if (letterDomination.size() != 0) {
            for (int i = 0; i < letterDomination.size(); i++) {
                System.out.println(letterDomination.get(i).getName() + " " + letterDomination.get(i).bestDecade());
            }
        }
    }

    // pre: fileName != null
    // create a Scanner and return connected to a File with the given name.
    private static Scanner getFileScannerForNames(String fileName) {
        Scanner sc = null;
        try {
            sc = new Scanner(new File(fileName));
        } catch (FileNotFoundException e) {
            System.out.println("Problem reading the data file. Returning null for Scanner"
                    + "object. Problems likely to occur." + e);
        }
        return sc;
    }

    // method that shows names that have appeared in every decade
    // pre: n != null
    // post: print out names that have appeared in every decade
    private static void appearAlways(Names namesDatabase) {
        if (namesDatabase == null) {
            throw new IllegalArgumentException("The parameter namesDatabase cannot be null");
        }
        ArrayList<String> every = namesDatabase.rankedEveryDecade();
        System.out.println(every.size() + " names appear in every decade. The names are:");
        for (String name : every) {
            System.out.println(name);
        }
    }

    // method that shows names that have appeared in only one decade
    // pre: n != null
    // post: print out names that have appeared in only one decade
    private static void appearOnce(Names namesDatabase) {
        if (namesDatabase == null) {
            throw new IllegalArgumentException("The parameter namesDatabase cannot be null");
        }
        ArrayList<String> once = namesDatabase.rankedOnlyOneDecade();
        System.out.println(once.size() + " names appear in exactly one decade. The names are:");
        for (String name : once) {
            System.out.println(name);
        }
    }

    // method that shows names that have gotten more popular
    // in each successive decade
    // pre: n != null
    // post: print out names that have gotten more popular in each decade
    private static void alwaysMore(Names namesDatabase) {
        if (namesDatabase == null) {
            throw new IllegalArgumentException("The parameter namesDatabase cannot be null");
        }
        ArrayList<String> more = namesDatabase.alwaysMorePopular();
        System.out.println(more.size() + " names are more popular in every decade.");
        for (String name : more) {
            System.out.println(name);
        }
    }

    // method that shows names that have gotten less popular
    // in each successive decade
    // pre: n != null
    // post: print out names that have gotten less popular in each decade
    private static void alwaysLess(Names namesDatabase) {
        if (namesDatabase == null) {
            throw new IllegalArgumentException("The parameter namesDatabase cannot be null");
        }
        ArrayList<String> less = namesDatabase.alwaysLessPopular();
        System.out.println(less.size() + " names are less popular in every decade.");
        for (String name : less) {
            System.out.println(name);
        }
    }

    // method that shows data for one name, or states that name has never been
    // ranked
    // pre: n != null, keyboard != null and is connected to System.in
    // post: print out the data for n or a message that n has never been in the
    // top 1000 for any decade
    private static void oneName(Names namesDatabase, Scanner keyboard) {
        // Note, no way to check if keyboard actually connected to System.in
        // so we simply assume it is.
        if (namesDatabase == null || keyboard == null) {
            throw new IllegalArgumentException("The parameters cannot be null");
        }
        System.out.print("Enter a name: ");
        String search = keyboard.next();
        System.out.println();
        NameRecord searchedRecord = namesDatabase.getName(search);
        if (!(searchedRecord == null)) {
            System.out.println(searchedRecord);
        } else {
            System.out.println(search + " does not appear in any decade.");
        }
    }

    // method that shows all names that contain a substring from the user
    // and the decade they were most popular in
    // pre: n != null, keyboard != null and is connected to System.in
    // post: show the correct data
    private static void search(Names namesDatabase, Scanner keyboard) {
        // Note, no way to check if keyboard actually connected to System.in
        // so we simply assume it is.
        if (namesDatabase == null || keyboard == null) {
            throw new IllegalArgumentException("The parameters cannot be null");
        }
        System.out.print("Enter a partial name: ");
        String partialName = keyboard.next();
        System.out.println();
        ArrayList<NameRecord> matched = namesDatabase.getMatches(partialName);
        int matches = matched.size();
        System.out.println("There are " + matches + " for " + partialName + ".");
        System.out.println();
        if (matches != 0) {
            System.out.println("The matches with their highest ranking decade are:");
            for (NameRecord nr : matched) {
                System.out.println(nr.getName() + " " + nr.bestDecade());
            }
        }
    }

    // get choice from the user
    // keyboard != null and is connected to System.in
    // return an int that is >= SEARCH and <= QUIT
    private static int getChoice(Scanner keyboard) {
        // Note, no way to check if keyboard actually connected to System.in
        // so we simply assume it is.
        if (keyboard == null) {
            throw new IllegalArgumentException("The parameter keyboard cannot be null");
        }
        int choice = getInt(keyboard, "Enter choice: ");
        keyboard.nextLine();
        // add one due to zero based indexing of enums, but 1 based indexing of menu
        final int MAX_CHOICE = MenuChoices.QUIT.ordinal() + 1;
        while (choice < 1 || choice > MAX_CHOICE) {
            System.out.println();
            System.out.println(choice + " is not a valid choice");
            choice = getInt(keyboard, "Enter choice: ");
            keyboard.nextLine();
        }
        return choice;
    }

    // ensure an int is entered from the keyboard
    // pre: s != null and is connected to System.in
    private static int getInt(Scanner s, String prompt) {
        // Note, no way to check if keyboard actually connected to System.in
        // so we simply assume it is.
        if (s == null) {
            throw new IllegalArgumentException("The parameter s cannot be null");
        }
        System.out.print(prompt);
        while (!s.hasNextInt()) {
            s.next();
            System.out.println("That was not an int.");
            System.out.print(prompt);
        }
        return s.nextInt();
    }

    // show the user the menu
    private static void showMenu() {
        System.out.println();
        System.out.println("Options:");
        System.out.println("Enter 1 to search for names.");
        System.out.println("Enter 2 to display data for one name.");
        System.out.println("Enter 3 to display all names that appear in only " + "one decade.");
        System.out.println("Enter 4 to display all names that appear in all " + "decades.");
        System.out.println("Enter 5 to display all names that are more popular " + "in every decade.");
        System.out.println("Enter 6 to display all names that are less popular " + "in every decade.");
        System.out.println("Enter 7 to display names with more consonants or vowels, " + "client's choice.");
        System.out.println("Enter 8 to quit.");
        System.out.println();
    }

}
