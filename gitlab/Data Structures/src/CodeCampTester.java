import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

// CodeCamp.java - CS314 Assignment 1 - Tester class

/*
 * Student information for assignment: 
 * Name: Mark Grubbs
 * email address: siegbalicula@gmail.com
 * UTEID: msg2772
 * Section 5 digit ID: 
 * Grader name: 
 * Number of slip days used on this assignment:
 */

/*
 * CS314 Students: place results of shared birthdays experiments in this
 * comment.
 * The average number of pairs from 1,000,000 runs of 182 people and 365 days is 45 pairs.
 * The point at which there is a 50% chance of getting at least one pair is 23 people on average.
Num people: 2, number of experiments with one or more shared birthday: 110, percentage: 0.22
Num people: 3, number of experiments with one or more shared birthday: 391, percentage: 0.78
Num people: 4, number of experiments with one or more shared birthday: 806, percentage: 1.61
Num people: 5, number of experiments with one or more shared birthday: 1366, percentage: 2.73
Num people: 6, number of experiments with one or more shared birthday: 2028, percentage: 4.06
Num people: 7, number of experiments with one or more shared birthday: 2771, percentage: 5.54
Num people: 8, number of experiments with one or more shared birthday: 3677, percentage: 7.35
Num people: 9, number of experiments with one or more shared birthday: 4836, percentage: 9.67
Num people: 10, number of experiments with one or more shared birthday: 5857, percentage: 11.71
Num people: 11, number of experiments with one or more shared birthday: 6936, percentage: 13.87
Num people: 12, number of experiments with one or more shared birthday: 8316, percentage: 16.63
Num people: 13, number of experiments with one or more shared birthday: 9739, percentage: 19.48
Num people: 14, number of experiments with one or more shared birthday: 11230, percentage: 22.46
Num people: 15, number of experiments with one or more shared birthday: 12770, percentage: 25.54
Num people: 16, number of experiments with one or more shared birthday: 14215, percentage: 28.43
Num people: 17, number of experiments with one or more shared birthday: 15813, percentage: 31.63
Num people: 18, number of experiments with one or more shared birthday: 17297, percentage: 34.59
Num people: 19, number of experiments with one or more shared birthday: 19156, percentage: 38.31
Num people: 20, number of experiments with one or more shared birthday: 20443, percentage: 40.89
Num people: 21, number of experiments with one or more shared birthday: 22112, percentage: 44.22
Num people: 22, number of experiments with one or more shared birthday: 23788, percentage: 47.58
Num people: 23, number of experiments with one or more shared birthday: 25186, percentage: 50.37
Num people: 24, number of experiments with one or more shared birthday: 26876, percentage: 53.75
Num people: 25, number of experiments with one or more shared birthday: 28443, percentage: 56.89
Num people: 26, number of experiments with one or more shared birthday: 29752, percentage: 59.50
Num people: 27, number of experiments with one or more shared birthday: 31367, percentage: 62.73
Num people: 28, number of experiments with one or more shared birthday: 32728, percentage: 65.46
Num people: 29, number of experiments with one or more shared birthday: 33929, percentage: 67.86
Num people: 30, number of experiments with one or more shared birthday: 35274, percentage: 70.55
Num people: 31, number of experiments with one or more shared birthday: 36704, percentage: 73.41
Num people: 32, number of experiments with one or more shared birthday: 37641, percentage: 75.28
Num people: 33, number of experiments with one or more shared birthday: 38897, percentage: 77.79
Num people: 34, number of experiments with one or more shared birthday: 39818, percentage: 79.64
Num people: 35, number of experiments with one or more shared birthday: 40780, percentage: 81.56
Num people: 36, number of experiments with one or more shared birthday: 41654, percentage: 83.31
Num people: 37, number of experiments with one or more shared birthday: 42410, percentage: 84.82
Num people: 38, number of experiments with one or more shared birthday: 43322, percentage: 86.64
Num people: 39, number of experiments with one or more shared birthday: 43922, percentage: 87.84
Num people: 40, number of experiments with one or more shared birthday: 44537, percentage: 89.07
Num people: 41, number of experiments with one or more shared birthday: 45163, percentage: 90.33
Num people: 42, number of experiments with one or more shared birthday: 45600, percentage: 91.20
Num people: 43, number of experiments with one or more shared birthday: 46222, percentage: 92.44
Num people: 44, number of experiments with one or more shared birthday: 46688, percentage: 93.38
Num people: 45, number of experiments with one or more shared birthday: 47110, percentage: 94.22
Num people: 46, number of experiments with one or more shared birthday: 47481, percentage: 94.96
Num people: 47, number of experiments with one or more shared birthday: 47689, percentage: 95.38
Num people: 48, number of experiments with one or more shared birthday: 48079, percentage: 96.16
Num people: 49, number of experiments with one or more shared birthday: 48260, percentage: 96.52
Num people: 50, number of experiments with one or more shared birthday: 48412, percentage: 96.82
Num people: 51, number of experiments with one or more shared birthday: 48648, percentage: 97.30
Num people: 52, number of experiments with one or more shared birthday: 48902, percentage: 97.80
Num people: 53, number of experiments with one or more shared birthday: 49064, percentage: 98.13
Num people: 54, number of experiments with one or more shared birthday: 49145, percentage: 98.29
Num people: 55, number of experiments with one or more shared birthday: 49310, percentage: 98.62
Num people: 56, number of experiments with one or more shared birthday: 49406, percentage: 98.81
Num people: 57, number of experiments with one or more shared birthday: 49537, percentage: 99.07
Num people: 58, number of experiments with one or more shared birthday: 49560, percentage: 99.12
Num people: 59, number of experiments with one or more shared birthday: 49648, percentage: 99.30
Num people: 60, number of experiments with one or more shared birthday: 49704, percentage: 99.41
Num people: 61, number of experiments with one or more shared birthday: 49762, percentage: 99.52
Num people: 62, number of experiments with one or more shared birthday: 49782, percentage: 99.56
Num people: 63, number of experiments with one or more shared birthday: 49846, percentage: 99.69
Num people: 64, number of experiments with one or more shared birthday: 49846, percentage: 99.69
Num people: 65, number of experiments with one or more shared birthday: 49855, percentage: 99.71
Num people: 66, number of experiments with one or more shared birthday: 49895, percentage: 99.79
Num people: 67, number of experiments with one or more shared birthday: 49912, percentage: 99.82
Num people: 68, number of experiments with one or more shared birthday: 49931, percentage: 99.86
Num people: 69, number of experiments with one or more shared birthday: 49946, percentage: 99.89
Num people: 70, number of experiments with one or more shared birthday: 49965, percentage: 99.93
Num people: 71, number of experiments with one or more shared birthday: 49968, percentage: 99.94
Num people: 72, number of experiments with one or more shared birthday: 49971, percentage: 99.94
Num people: 73, number of experiments with one or more shared birthday: 49985, percentage: 99.97
Num people: 74, number of experiments with one or more shared birthday: 49977, percentage: 99.95
Num people: 75, number of experiments with one or more shared birthday: 49994, percentage: 99.99
Num people: 76, number of experiments with one or more shared birthday: 49986, percentage: 99.97
Num people: 77, number of experiments with one or more shared birthday: 49987, percentage: 99.97
Num people: 78, number of experiments with one or more shared birthday: 49995, percentage: 99.99
Num people: 79, number of experiments with one or more shared birthday: 49996, percentage: 99.99
Num people: 80, number of experiments with one or more shared birthday: 49995, percentage: 99.99
Num people: 81, number of experiments with one or more shared birthday: 49997, percentage: 99.99
Num people: 82, number of experiments with one or more shared birthday: 49998, percentage: 100.00
Num people: 83, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 84, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 85, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 86, number of experiments with one or more shared birthday: 49999, percentage: 100.00
Num people: 87, number of experiments with one or more shared birthday: 49999, percentage: 100.00
Num people: 88, number of experiments with one or more shared birthday: 49999, percentage: 100.00
Num people: 89, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 90, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 91, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 92, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 93, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 94, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 95, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 96, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 97, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 98, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 99, number of experiments with one or more shared birthday: 50000, percentage: 100.00
Num people: 100, number of experiments with one or more shared birthday: 50000, percentage: 100.00
 */

public class CodeCampTester {

    public static void main(String[] args) {
        final String newline = System.getProperty("line.separator");

        // student test case
        // hamming distance
        int[] ha = new int[10000];
        int[] hb = new int[10000];
        ha[313] = 1337;
        hb[3133] = 42069;
        int expectedH1 = 2;
        int actualH1 = CodeCamp.hammingDistance(ha, hb);
        System.out.println(
                "Student Test 1 hamming distance: expected value: " + expectedH1 + " , actual value: " + actualH1);
        if (expectedH1 == actualH1) {
            System.out.println(" passed student test 1, hamming distance");
        } else {
            System.out.println(" ***** FAILED ***** student test 1, hamming distance");
        }

        int[] hc = new int[1000000];
        int[] hd = hc;
        hc[31] = 1337;
        hc[313] = 141;
        hc[3133] = 42069;
        hc[31331] = 31313;
        hc[313313] = hc[31];
        int expectedH2 = 0;
        int actualH2 = CodeCamp.hammingDistance(hc, hd);
        System.out.println(newline + "Student Test 2 hamming distance: expected value: " + expectedH2
                + " , actual value: " + actualH2);
        if (expectedH2 == actualH2) {
            System.out.println(" passed student test 2, hamming distance");
        } else {
            System.out.println(" ***** FAILED ***** student test 2, hamming distance");
        }

        // isPermutation
        int[] pa = { 11, 22, 33, 11, 22, 33, 11, 22, 33 };
        int[] pb = { 5 + 6, 6 + 16, 27 + 6, 7 + 3 + 1, 27 + 6, 16 + 6, 11, 22, 33 };
        boolean expectedP1 = true;
        boolean actualP1 = CodeCamp.isPermutation(pa, pb);
        System.out.println(newline + "Student Test 3 isPermutation: expected value: " + expectedP1 + ", actual value: "
                + actualP1);
        if (expectedP1 == actualP1) {
            System.out.println(" passed student test 3, isPermutation");
        } else {
            System.out.println(" ***** FAILED ***** student test 3, isPermutation");
        }

        int[] pc = { 1 + 2 / 3, 5 / 4, 244 / 243, 1414 / 1412, 4 % 3, 1, 1 * 1 * 1 * 1, 5 / 2 / 2, 0 * 1 + 2 - 1 };
        int[] pd = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        boolean expectedP2 = true;
        boolean actualP2 = CodeCamp.isPermutation(pc, pd);
        System.out.println(newline + "Student Test 4 isPermutation: expected value: " + expectedP2 + ", actual value: "
                + actualP2);
        if (expectedP2 == actualP2) {
            System.out.println(" passed student test 4, isPermutation");
        } else {
            System.out.println(" ***** FAILED ***** student test 4, isPermutation");
        }


        // mostVowels
        String[] arrayOfStringsM1 = { "xXN0obpwn3erXx", "P0n1eluver69444222000", "!aGamer", "g1tgudskrub267aAaAa",
                "             " };
        int expectedM1 = 3;
        int actualM1 = CodeCamp.mostVowels(arrayOfStringsM1);
        System.out.println(
                newline + "Student Test 5 mostVowels: expected result: " + expectedM1 + ", actual result: " + actualM1);
        if (actualM1 == expectedM1) {
            System.out.println("passed student test 5, mostVowels");
        } else {
            System.out.println("***** FAILED ***** student test 5, mostVowels");
        }

        String[] arrayOfStringsM2 = { "fewfwefweffwewefwefwefwefwefwefwefwefwef", "iIiIiIIIiIIIiI", "isthisgibberish",
                "catchthis", "13213123123123123123123123" };
        int expectedM2 = 1;
        int actualM2 = CodeCamp.mostVowels(arrayOfStringsM2);
        System.out.println(
                newline + "Student Test 6 mostVowels: expected result: " + expectedM2 + ", actual result: " + actualM2);
        if (actualM2 == expectedM2) {
            System.out.println("passed student test 6, mostVowels");
        } else {
            System.out.println("***** FAILED ***** student test 6, mostVowels");
        }

        // sharedBirthdays
        int pairsS1 = CodeCamp.sharedBirthdays(53, 1);
        int expectedSharedS1 = (int) (53.0 / 2 * 52);
        System.out.println(
                newline + "Student Test 7 sharedBirthdays: expected " + expectedSharedS1 + ", actual: " + pairsS1);
        if (pairsS1 == expectedSharedS1) {
            System.out.println("passed student test 7, sharedBirthdays");
        } else {
            System.out.println("***** FAILED ***** student test 7, sharedBirthdays");
        }

        int pairsS2 = CodeCamp.sharedBirthdays(85, 365);
        System.out.println(
                newline + "Student Test 8 sharedBirthdays: expected a number greater than 0, actual: " + pairsS2);
        if (pairsS2 > 0) {
            System.out.println("passed student test 8, sharedBirthdays");
        } else {
            System.out.println("***** FAILED ***** student test 8, sharedBirthdays");
        }


        // queensAreSafe
        char[][] boardQ1 = { { '.', '.', '.', '.' }, { '.', '.', '.', '.' }, { '.', '.', '.', '.' },
                { '.', '.', '.', '.' } };
        boolean expectedQ1 = true;
        boolean actualQ1 = CodeCamp.queensAreSafe(boardQ1);
        System.out.println(newline + "Student Test 9 queensAreSafe: expected value true, actual value: " + actualQ1);
        if (expectedQ1 && actualQ1) {
            System.out.println(" passed student test 9, queensAreSafe");
        } else {
            System.out.println(" ***** FAILED ***** student test 9, queensAreSafe");
        }

        char[][] boardQ2 = { { '.', '.', '.', '.', 'q' }, { '.', '.', 'q', '.', '.' }, { 'q', '.', '.', '.', '.' },
                { '.', '.', '.', 'q', '.' }, { '.', 'q', '.', '.', '.' } };
        boolean expectedQ2 = true;
        boolean actualQ2 = CodeCamp.queensAreSafe(boardQ2);
        System.out.println(newline + "Student Test 10 queensAreSafe: expected value true, actual value: " + actualQ2);
        if (expectedQ2 && actualQ2) {
            System.out.println(" passed student test 10, queensAreSafe");
        } else {
            System.out.println(" ***** FAILED ***** student test 9, queensAreSafe");
        }

        // getValueOfMostValuablePlot
        int[][] cityG1 = { { -1, 1, 3, 4, -5, 6, -5, -6, 96 } };
        int expectedG1 = 96;
        int actualG1 = CodeCamp.getValueOfMostValuablePlot(cityG1);
        System.out.println(newline + "Student Test 11 getValueOfMostValuablePlot: expected value: " + expectedG1
                + ", actual: " + actualG1);
        if (expectedG1 == actualG1) {
            System.out.println(" passed student test 11, getValueOfMostValuablePlot");
        } else {
            System.out.println(" ***** FAILED ***** test 11, getValueOfMostValuablePlot");
        }

        int[][] cityG2 = { { 1 }, { 2 }, { 3 }, { -4 }, { -3 }, { 3 }, { 6 }, { 7 } };
        int expectedG2 = 16;
        int actualG2 = CodeCamp.getValueOfMostValuablePlot(cityG2);
        System.out.println(newline + "Student Test 11 getValueOfMostValuablePlot: expected value: " + expectedG2
                + ", actual: " + actualG2);
        if (expectedG2 == actualG2) {
            System.out.println(" passed student test 11, getValueOfMostValuablePlot");
        } else {
            System.out.println(" ***** FAILED ***** test 11, getValueOfMostValuablePlot");
        }

        // DELETE THE ABOVE TESTS IN THE VERSION OF THE FILE YOU TURN IN

        // CS314 Students: add tests here.

    } // end of main method

    // pre: list != null
    private static int[] intListToArray(List<Integer> list) {
        if (list == null) {
            throw new IllegalArgumentException("list parameter may not be null.");
        }
        int[] result = new int[list.size()];
        int arrayIndex = 0;
        for (int x : list) {
            result[arrayIndex++] = x;
        }
        return result;
    }
}
