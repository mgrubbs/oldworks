
/*
 * Student information for assignment: 
 *
 * Number of slip days used:
 * Student 1 (Student whose turnin account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *
 * Student 2
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 */

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JFileChooser;
import javax.swing.UIManager;

/*
 * CS 314 Students, put your results to the experiments
 * and answers to questions here:
 * files : 
 * d1.txt : file size 1 kb
 * ***** CS314 SortedSet: *****
Time to add the elements in the text to this set: elapsed time: 0.0013483 seconds.
Total number of words in text including duplicates: 56
Number of distinct words in this text 56

****** CS314 UnsortedSet: *****
Time to add the elements in the text to this set: elapsed time: 0.0011737 seconds.
Total number of words in text including duplicates: 56
Number of distinct words in this text 56

***** Java HashSet ******
Time to add the elements in the text to this set: elapsed time: 5.246E-4 seconds.
Total number of words in text including duplicates: 56
Number of distinct words in this text 56

***** Java TreeSet ******
Time to add the elements in the text to this set: elapsed time: 6.167E-4 seconds.
Total number of words in text including duplicates: 56
Number of distinct words in this text 56

    d2.txt : file size 30 kb
    ***** CS314 SortedSet: *****
Time to add the elements in the text to this set: elapsed time: 0.0102942 seconds.
Total number of words in text including duplicates: 3927
Number of distinct words in this text 3927

****** CS314 UnsortedSet: *****
Time to add the elements in the text to this set: elapsed time: 0.0791293 seconds.
Total number of words in text including duplicates: 3927
Number of distinct words in this text 3927

***** Java HashSet ******
Time to add the elements in the text to this set: elapsed time: 0.004562 seconds.
Total number of words in text including duplicates: 3927
Number of distinct words in this text 3927

***** Java TreeSet ******
Time to add the elements in the text to this set: elapsed time: 0.0091372 seconds.
Total number of words in text including duplicates: 3927
Number of distinct words in this text 3927

    31617-0.txt : file size 552 kb
    ***** CS314 SortedSet: *****
Time to add the elements in the text to this set: elapsed time: 0.0532458 seconds.
Total number of words in text including duplicates: 4638
Number of distinct words in this text 2433

****** CS314 UnsortedSet: *****
Time to add the elements in the text to this set: elapsed time: 0.0537868 seconds.
Total number of words in text including duplicates: 4638
Number of distinct words in this text 2433

***** Java HashSet ******
Time to add the elements in the text to this set: elapsed time: 0.02579 seconds.
Total number of words in text including duplicates: 4638
Number of distinct words in this text 2433

***** Java TreeSet ******
Time to add the elements in the text to this set: elapsed time: 0.0259987 seconds.
Total number of words in text including duplicates: 4638
Number of distinct words in this text 2433

    d3.txt : file size 163 kb
    ***** CS314 SortedSet: *****
Time to add the elements in the text to this set: elapsed time: 0.0175723 seconds.
Total number of words in text including duplicates: 19911
Number of distinct words in this text 19911

****** CS314 UnsortedSet: *****
Time to add the elements in the text to this set: elapsed time: 1.4855905 seconds.
Total number of words in text including duplicates: 19911
Number of distinct words in this text 19911

***** Java HashSet ******
Time to add the elements in the text to this set: elapsed time: 0.0224396 seconds.
Total number of words in text including duplicates: 19911
Number of distinct words in this text 19911

***** Java TreeSet ******
Time to add the elements in the text to this set: elapsed time: 0.0214023 seconds.
Total number of words in text including duplicates: 19911
Number of distinct words in this text 19911

 *1 : UnsortedSet : O(M^2) SortedSet : O(M) HashSet : O(N) TreeSet : O(N)
 *2 : The add method of the unsortedSet is O(N) and the sortedSet is O(N). I think that the add method of HashSet is O(M) and the TreeSet is O(N).
 *I think that the add method for these is also dependent on the duplicate words.
 *3 : HashSet prints the contents faster and in a random order while TreeSet is slightly slower and prints in lexicographical order.
 * CS314 Students, why is it unwise to implement all three of the
 * intersection, union, and difference
 * methods in the AbstractSet class:
 * It is unwise to implement all three operations in AbstractSet because methods may have to call themselves to get the solution.
 */

public class SetTester {

    public static void main(String[] args) {

        ISet<String> st1 = new UnsortedSet<>();
        st1.add("L");
        st1.add("LL".substring(0, 1));
        st1.add("LL");
        st1.add("3");
        // add unsorted
        showTestResults(st1.contains("L"), 1, "add and contains methods UnsortedSet student test");
        st1.remove((new Integer(3)).toString());
        // remove the same between sorted and unsorted
        showTestResults(!st1.contains("3"), 2, "remove method UnsortedSet student test");
        ISet<String> st2 = new UnsortedSet<>();
        st2.add("L");
        st2.add("LL");
        st2.add("LLL");
        // containsAll unsorted
        showTestResults(st2.containsAll(st1), 3, "containsAll method UnsortedSet student test");
        st2.remove("LLL");
        // equals unsorted
        showTestResults(st2.equals(st1), 4, "equals method UnsortedSet student test");
        st1.clear();
        ISet<String> st3 = new UnsortedSet<>();
        // clear the same between sorted and unsorted
        showTestResults(st1.equals(st3), 5, "clear method UnsortedSet student test");
        st1.addAll(st2);
        // addAll unsorted
        showTestResults(st1.equals(st2), 6, "addAll method UnsortedSet student test");
        st1.add("LLLL");
        st1 = st1.difference(st2);
        st3.add("LLLL");
        // difference unsorted
        showTestResults(st3.equals(st1), 7, "difference method UnsortedSet student test");
        st3.add("LLLLL");
        st3.add("llllll");
        ISet<String> st4 = new UnsortedSet<>();
        st4.add("LLLLL");
        st4.add("llllll");
        st1 = st1.union(st4);
        // union unsorted
        showTestResults(st3.equals(st1), 8, "union method UnsortedSet student test");
        st1 = st1.intersection(st2);
        st3.clear();
        // intersection - uses abstractSet which uses other methods
        showTestResults(st3.equals(st1), 9, "intersection method UnsortedSet student test");
        // size - is overridden in abstractSet but does not change between unsorted and
        // sorted
        showTestResults(st2.size() == 2, 10, "size method UnsortedSet student test");

        // add sorted and iterator (which is the same among unsorted and sorted)

        st1 = new SortedSet<String>();
        st1.add("C");
        st1.add("A");
        st1.add("B");
        Iterator<String> stIterator = st1.iterator();
        String firstLetter = stIterator.next();
        showTestResults(firstLetter.equals("A"), 11,
                "add and iterator methods SortedSet student test; iterator method is same between sorted and unsorted");
        // containsAll sorted
        st2 = new SortedSet<String>();
        st2.add("C");
        st2.add("B");
        showTestResults(st1.containsAll(st2), 12, "containsAll method SortedSet student test");
        // equals sorted
        st2.add("A");
        showTestResults(st1.equals(st2), 13, "equals method SortedSet student test");
        // addAll sorted
        st3 = new SortedSet<String>();
        st3.addAll(st2);
        showTestResults(st3.equals(st1), 14, "addAll method SortedSet student test");
        // difference sorted
        st3.add("M");
        st3.add("R");
        st3.add("X");
        st3 = st3.difference(st2);
        st4 = new SortedSet<>();
        st4.add("M");
        st4.add("R");
        st4.add("X");
        showTestResults(st3.equals(st4), 15, "difference method SortedSet student test");
        // union sorted
        st3 = st3.union(st2);
        st4.add("A");
        st4.add("B");
        st4.add("C");
        showTestResults(st3.equals(st4), 16, "union method SortedSet student test");
        // intersection sorted
        st3 = st3.intersection(st2);
        showTestResults(st3.equals(st1), 17, "intersection method SortedSet student test");
        
        
        // CS314 Students. Uncomment this section when ready to
        // run your experiments
//        try {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//        } catch (Exception e) {
//            System.out.println("Unable to change look and feel");
//        }
//        Scanner sc = new Scanner(System.in);
//        String response = "";
//        do {
//            largeTest();
//            System.out.print("Another file? Enter y to do another file: ");
//            response = sc.next();
//        } while (response != null && response.length() > 0 && response.substring(0, 1).equalsIgnoreCase("y"));

    }

    // print out results of test
    // I used this; provided by professor
    private static void showTestResults(boolean passed, int testNumber, String testDescription) {
        if (passed) {
            System.out.print("Passed test ");
        } else {
            System.out.print("Failed test ");
        }
        System.out.println(testNumber + ": " + testDescription);
    }

    /*
     * Method asks user for file and compares run times to add words from file to
     * various sets, including CS314 UnsortedSet and SortedSet and Java's TreeSet
     * and HashSet.
     */
    private static void largeTest() {
        System.out.println();
        System.out.println("Opening Window to select file. You may have to minimize other windows.");
        String text = convertFileToString();
        System.out.println();
        System.out.println("***** CS314 SortedSet: *****");
        processTextCS314Sets(new SortedSet<String>(), text);
        System.out.println("****** CS314 UnsortedSet: *****");
        processTextCS314Sets(new UnsortedSet<String>(), text);
        System.out.println("***** Java HashSet ******");
        processTextJavaSets(new HashSet<String>(), text);
        System.out.println("***** Java TreeSet ******");
        processTextJavaSets(new TreeSet<String>(), text);
    }

    /*
     * pre: set != null, text != null Method to add all words in text to the given
     * set. Words are delimited by white space. This version for CS314 sets.
     */
    private static void processTextCS314Sets(ISet<String> set, String text) {
        Stopwatch s = new Stopwatch();
        Scanner sc = new Scanner(text);
        int totalWords = 0;
        s.start();
        while (sc.hasNext()) {
            totalWords++;
            set.add(sc.next());
        }
        s.stop();

        showResultsAndWords(set, s, totalWords, set.size());
    }

    /*
     * pre: set != null, text != null Method to add all words in text to the given
     * set. Words are delimited by white space. This version for Java Sets.
     */
    private static void processTextJavaSets(Set<String> set, String text) {
        Stopwatch s = new Stopwatch();
        Scanner sc = new Scanner(text);
        int totalWords = 0;
        s.start();
        while (sc.hasNext()) {
            totalWords++;
            set.add(sc.next());
        }
        s.stop();

        showResultsAndWords(set, s, totalWords, set.size());
    }

    /*
     * Show results of add words to given set.
     */
    private static <E> void showResultsAndWords(Iterable<E> set, Stopwatch s, int totalWords, int setSize) {

        System.out.println("Time to add the elements in the text to this set: " + s.toString());
        System.out.println("Total number of words in text including duplicates: " + totalWords);
        System.out.println("Number of distinct words in this text " + setSize);

        System.out.print("Enter y to see the contents of this set: ");
        Scanner sc = new Scanner(System.in);
        String response = sc.next();

        if (response != null && response.length() > 0 && response.substring(0, 1).equalsIgnoreCase("y")) {
            for (Object o : set)
                System.out.println(o);
        }
        System.out.println();
    }

    /*
     * Ask user to pick a file via a file choosing window and convert that file to a
     * String. Since we are evalutatin the file with many sets convert to string
     * once instead of reading through file multiple times.
     */
    private static String convertFileToString() {
        // create a GUI window to pick the text to evaluate
        JFileChooser chooser = new JFileChooser(".");
        StringBuilder text = new StringBuilder();
        int retval = chooser.showOpenDialog(null);

        chooser.grabFocus();

        // read in the file
        if (retval == JFileChooser.APPROVE_OPTION) {
            File source = chooser.getSelectedFile();
            try {
                Scanner s = new Scanner(new FileReader(source));

                while (s.hasNextLine()) {
                    text.append(s.nextLine());
                    text.append(" ");
                }

                s.close();
            } catch (IOException e) {
                System.out.println("An error occured while trying to read from the file: " + e);
            }
        }

        return text.toString();
    }
}
