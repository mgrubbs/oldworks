
/*  Student information for assignment:  
 *
 *  On <MY|OUR> honor, <Mark Grubbs> and <NAME2), this programming assignment is <MY|OUR> own work
 *  and <I|WE> have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose turnin account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *
 *  Student 2
 *  UTEID:
 *  email address:
 *
 */
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HuffTree {

    // used to traverse tree
    private TreeNode root;
    // keeps nodes of the tree in priority queue
    private HuffPriorityQueue<TreeNode> nodes;
    // size for store_tree header
    private int size;

    // creates a new HuffTree by using counted values from the original file to put
    // into the tree and create compressed codes
    // uses fair priority queue to make tree
    public HuffTree(int[] frequencies) throws IOException {
        nodes = setUpPriorityQueue(frequencies);
        buildTree();
        size = 0;
    }
    
    // used for building tree from a priority queue
    // skips steps from other constructor
    // used by STORE_TREE format
    public HuffTree(HuffPriorityQueue<TreeNode> nodes) {
        this.nodes = nodes;
        buildTree();
        size = 0;
    }

    // creates a priority queue to be used for building the tree by inserting
    // frequencies greater than one as new tree nodes
    private HuffPriorityQueue<TreeNode> setUpPriorityQueue(int[] frequencies) {
        HuffPriorityQueue<TreeNode> frequencyNodes = new HuffPriorityQueue<>();
        for (int frequency = 0; frequency < frequencies.length; frequency++) {
            if (frequencies[frequency] != 0) {
                frequencyNodes.enqueue(new TreeNode(frequency, frequencies[frequency]));
            }
        }
        return frequencyNodes;
    }

    // builds tree with compressed data by going through each element in the queue
    // and creating a new treeNode with the first two elements of the queue
    private void buildTree() {
        while (nodes.size() > 1) {
            nodes.enqueue(new TreeNode(nodes.dequeue(), ' ', nodes.dequeue()));
        }
        root = nodes.dequeue();
    }

    // makes a mapping of the values in the input by using the algorithm from
    // class
    public Map<Integer, String> getMapping() {
        Map<Integer, String> compressedData = new HashMap<>();
        makeMapping(compressedData, "", root);
        return compressedData;
    }

    // goes through tree and makes codes for each frequency in the input
    // left = append 0; right = append 1
    private void makeMapping(Map<Integer, String> compressedData, String compress, TreeNode node) {
        if (node.isLeaf()) {
            compressedData.put(node.getValue(), compress);
        } else {
            makeMapping(compressedData, compress + "0", node.getLeft());
            makeMapping(compressedData, compress + "1", node.getRight());
        }
    }

    // counts bit size for use with rewriting in format of STORE_TREE
    // always add 1 due to value added from writing whether a node was a leaf or a
    // tree; if a node is a leaf add 9 bits
    private void countBitSize(TreeNode node) {
        if (node != null) {
            size++;
            if (node.isLeaf()) {
                size += IHuffConstants.BITS_PER_WORD + 1;
            } else {
                countBitSize(node.getLeft());
                countBitSize(node.getRight());
            }
        }
    }

    // call this to output the header format for STORE_TREE format
    public void headerIsTree(BitOutputStream bitOut) {
        writeTreeHeader(bitOut, root);
    }

    // writes recoding data for STORE_TREE data
    // if node is not a leaf, write a 0
    // otherwise write a one and a 9 bit integer of the value of the node
    private void writeTreeHeader(BitOutputStream bitOut, TreeNode node) {
        if (node != null) {
            if (!node.isLeaf()) {
                bitOut.writeBits(1, 0);
                writeTreeHeader(bitOut, node.getLeft());
                writeTreeHeader(bitOut, node.getRight());
            } else {
                bitOut.writeBits(1, 1);
                bitOut.writeBits(IHuffConstants.BITS_PER_WORD + 1, node.getValue());
            }
        }
    }

    // returns bits written for STORE_TREE
    public int treeBitSize() {
        countBitSize(root);
        return size + IHuffConstants.BITS_PER_INT;
    }

    // decompresses using tree from making the tree with the header
    // code from the assignment howto page
    public void decompress(BitInputStream bitIn, BitOutputStream bitOut) throws IOException {
        TreeNode node = root;
        boolean run = true;
        while (run) {
            int bit = bitIn.readBits(1);
            if (bit == -1) {
                throw new IOException(
                        "Error reading compressed file. \n" + "unexpected end of input. No PSEUDO_EOF value");
            } else {
                if (bit == 0) {
                    node = node.getLeft();
                } else if (bit == 1) {
                    node = node.getRight();
                }
                if (node.isLeaf()) {
                    if (node.getValue() == IHuffConstants.PSEUDO_EOF) {
                        run = false;
                    } else {
                        bitOut.writeBits(IHuffConstants.BITS_PER_WORD, node.getValue());
                        node = root;
                    }
                }
            }
        }
    }


}
