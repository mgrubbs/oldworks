Calgary
total bytes read: 3251493
total compressed bytes 1845608
total percent compression 43.238
compression time: 0.538

waterloo
total bytes read: 12466304
total compressed bytes 10205297
total percent compression 18.137
compression time: 1.455

Books and HTML
total bytes read: 9788581
total compressed bytes 5828133
total percent compression 40.460
compression time: 1.265

TIF files had a significantly smaller compression amount while the other files
compressed more overall. When a group of HF files is to be compressed, it creates
a "compressed file" that is larger than the original for each file.