import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/* CS 314 STUDENTS: FILL IN THIS HEADER AND THEN COPY AND PASTE IT TO YOUR
 * LetterInventory.java AND AnagramSolver.java CLASSES.
 *
 * Student information for assignment:
 *
 *  On my honor, Mark Grubbs, this programming assignment is my own work
 *  and I have not provided this code to any other student.
 *
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *  Number of slip days I am using: 1
 */

public class AnagramFinderTester {

    private static final String testCaseFileName = "testCaseAnagrams.txt";
    private static final String dictionaryFileName = "d3.txt";

    /**
     * main method that executes tests.
     * 
     * @param args Not used.
     */
    public static void main(String[] args) {

        cs314StudentTestsForLetterInventory();
        // tests on the anagram solver itself
//        boolean displayAnagrams = getChoiceToDisplayAnagrams();
//        AnagramSolver solver = new AnagramSolver(AnagramMain.readWords(dictionaryFileName));
//        runAnagramTests(solver, displayAnagrams);
    }
    
    private static void cs314StudentTestsForLetterInventory() {
        // CS314 Students, delete the above tests when you turn in your assignment
        // CS314 Students add your LetterInventory tests here.
        LetterInventory numbers = new LetterInventory("one2ThReE4FIVE6Neevs8Einn");
        LetterInventory notNumbers = new LetterInventory("NeinEontherevinesVfee");
        // get method
        int eeeeeEE = numbers.get('e');
        int nnnN = numbers.get('N');
        System.out.println("get Test 1 :");
        int expected = 7;
        System.out.println("expected : " + expected);
        System.out.println("actual : " + eeeeeEE);
        if (eeeeeEE == expected) {
            System.out.println("get test 1 passed");
        } else {
            System.out.println("get test 1 failed");
        }
        System.out.println("get Test 2 :");
        expected = 4;
        System.out.println("expected : " + expected);
        System.out.println("actual : " + nnnN);
        if (nnnN == expected) {
            System.out.println("get test 2 passed");
        } else {
            System.out.println("get test 2 failed");
        }
        // size method
        int size1 = numbers.size();
        int size2 = notNumbers.size();
        System.out.println("size Test 1 :");
        expected = 21;
        System.out.println("expected : " + expected);
        System.out.println("actual : " + size1);
        if (size1 == expected) {
            System.out.println("size test 1 passed");
        } else {
            System.out.println("size test 1 failed");
        }
        System.out.println("size Test 2 :");
        System.out.println("expected : " + expected);
        System.out.println("actual : " + size2);
        if (size2 == expected) {
            System.out.println("size test 2 passed");
        } else {
            System.out.println("size test 2 failed");
        }
        // isEmpty
        LetterInventory emptyQuestionMark = new LetterInventory("12348190709857123094871230498?!#?@!#@");
        boolean empty = emptyQuestionMark.isEmpty();
        boolean notEmpty = numbers.isEmpty();
        System.out.println("isEmpty Test 1 :");
        boolean isExpected = true;
        System.out.println("expected : " + isExpected);
        System.out.println("actual : " + empty);
        if (empty) {
            System.out.println("isEmpty test 1 passed");
        } else {
            System.out.println("isEmpty test 1 failed");
        }
        isExpected = false;
        System.out.println("isEmpty Test 2 :");
        System.out.println("expected : " + isExpected);
        System.out.println("actual : " + notEmpty);
        if (!notEmpty) {
            System.out.println("isEmpty test 2 passed");
        } else {
            System.out.println("isEmpty test 2 failed");
        }
        // toString
        String blank = emptyQuestionMark.toString(); // ""
        String notNums = notNumbers.toString();
        String sExpected = "";
        System.out.println("toString test 1 :");
        System.out.println("expected : " + sExpected);
        System.out.println("actual : " + blank);
        if(blank.equals(sExpected)) {
            System.out.println("toString test 1 passed");
        } else {
            System.out.println("toString test 1 failed");
        }
        sExpected = "eeeeeeefhiinnnnorstvv";
        System.out.println("toString test 2 :");
        System.out.println("expected : " + sExpected);
        System.out.println("actual : " + notNums);
        if(notNums.equals(sExpected)) {
            System.out.println("toString test 2 passed");
        } else {
            System.out.println("toString test 2 failed");
        }
        // add
        LetterInventory numbersAndNotNumbers = numbers.add(notNumbers);
        LetterInventory numbersAndNumbersAndNotNumbers = numbersAndNotNumbers.add(numbers);
        eeeeeEE = numbersAndNotNumbers.size();
        nnnN = numbersAndNumbersAndNotNumbers.get('e');
        expected = 42;
        System.out.println("add Test 1 :");
        System.out.println("expected : " + expected);
        System.out.println("actual : " + eeeeeEE);
        if (eeeeeEE == expected) {
            System.out.println("add test 1 passed");
        } else {
            System.out.println("add test 1 failed");
        }
        System.out.println("add Test 2 :");
        expected = 21;
        System.out.println("expected : " + expected);
        System.out.println("actual : " + nnnN);
        if (nnnN == expected) {
            System.out.println("add test 2 passed");
        } else {
            System.out.println("add test 2 failed");
        }
        // subtract
        numbersAndNotNumbers = numbersAndNumbersAndNotNumbers.subtract(numbers);
        LetterInventory newNumbers = numbers.subtract(emptyQuestionMark);
        System.out.println("subtract test 1 :");
        System.out.println("expected : remains the same (numbers - nothing = numbers)");
        if(newNumbers.equals(numbers)) {
            System.out.println("subtract test 1 passed");
        } else {
            System.out.println("subtract test 1 failed");
        }
        System.out.println("subtract test 2 :");
        System.out.println("expected : 42");
        System.out.println("actual : " + numbersAndNotNumbers.size());
        if(numbersAndNotNumbers.size() == 42) {
            System.out.println("subtract test 2 passed");
        } else {
            System.out.println("subtract test 2 failed");
        }
        // equals
        System.out.println("equals test 1 :");
        System.out.println("expected : numbers is equal to notNumbers");
        if(numbers.equals(notNumbers)) {
            System.out.println("equals test 1 passed");
        } else {
            System.out.println("equals test 1 failed");
        }
        System.out.println("equals test 2 :");
        System.out.println("expected : numbersAndNotNumbers + numbers = numbersAndNumbersAndNotNumbers");
        if(numbersAndNotNumbers.add(numbers).equals(numbersAndNumbersAndNotNumbers)) {
            System.out.println("equals test 2 passed");
        } else {
            System.out.println("equals test 2 failed");
        }
    }

    private static boolean getChoiceToDisplayAnagrams() {
        Scanner console = new Scanner(System.in);
        System.out.print("Enter y or Y to display anagrams during tests: ");
        String response = console.nextLine();
        console.close();
        return response.length() > 0 && response.toLowerCase().charAt(0) == 'y';
    }

    /**
     * Method to run tests on Anagram solver itself. pre: the files d3.txt and
     * testCaseAnagrams.txt are in the local directory
     * 
     * assumed format for file is <NUM_TESTS> <TEST_NUM> <MAX_WORDS> <PHRASE>
     * <NUMBER OF ANAGRAMS> <ANAGRAMS>
     */
    private static void runAnagramTests(AnagramSolver solver, boolean displayAnagrams) {
        int solverTestCases = 0;
        int solverTestCasesPassed = 0;
        Stopwatch st = new Stopwatch();
        try {
            Scanner sc = new Scanner(new File(testCaseFileName));
            final int NUM_TEST_CASES = Integer.parseInt(sc.nextLine().trim());
            System.out.println(NUM_TEST_CASES);
            for (int i = 0; i < NUM_TEST_CASES; i++) {
                // expected results
                TestCase currentTest = new TestCase(sc);
                solverTestCases++;
                st.start();
                // actual results
                List<List<String>> actualAnagrams = solver.getAnagrams(currentTest.phrase, currentTest.maxWords);
                st.stop();
                if (checkPassOrFailTest(currentTest, actualAnagrams)) {
                    solverTestCasesPassed++;
                }
                System.out.println();
                System.out.println("Time to find anagrams: " + st.time());
                if (displayAnagrams) {
                    displayAnagrams("actual anagrams", actualAnagrams);
                    System.out.println();
                    displayAnagrams("expected anagrams", currentTest.anagrams);
                }
                System.out.println("\n******************************************\n");
                // System.out.println("Number of calls to recursive helper method: " +
                // NumberFormat.getNumberInstance(Locale.US).format(AnagramSolver.callsCount));
            }
            sc.close();
        } catch (IOException e) {
            System.out.println("\nProblem while running test cases on AnagramSolver. Check"
                    + " that file testCaseAnagrams.txt is in the correct location.");
            System.out.println(e);
            System.out.println("AnagramSolver test cases run: " + solverTestCases);
            System.out.println("AnagramSolver test cases failed: " + (solverTestCases - solverTestCasesPassed));
        }
        System.out.println("\nAnagramSolver test cases run: " + solverTestCases);
        System.out.println("AnagramSolver test cases failed: " + (solverTestCases - solverTestCasesPassed));
    }

    // print out all of the anagrams in a list of anagram
    private static void displayAnagrams(String type, List<List<String>> anagrams) {

        System.out.println("Results for " + type);
        System.out.println("num anagrams: " + anagrams.size());
        System.out.println("anagrams: ");
        for (List<String> singleAnagram : anagrams)
            System.out.println(singleAnagram);
    }

    // determine if the test passed or failed
    private static boolean checkPassOrFailTest(TestCase currentTest, List<List<String>> actualAnagrams) {

        boolean passed = true;
        System.out.println();
        System.out.println("Test number: " + currentTest.testCaseNumber);
        System.out.println("Phrase: " + currentTest.phrase);
        System.out.println("Word limit: " + currentTest.maxWords);
        System.out.println("Expected Number of Anagrams: " + currentTest.anagrams.size());
        if (actualAnagrams.equals(currentTest.anagrams)) {
            System.out.println("Passed Test");
        } else {
            System.out.println("\n!!! FAILED TEST CASE !!!");
            System.out.println("Recall MAXWORDS = 0 means no limit.");
            System.out.println("Expected number of anagrams: " + currentTest.anagrams.size());
            System.out.println("Actual number of anagrams:   " + actualAnagrams.size());
            if (currentTest.anagrams.size() == actualAnagrams.size()) {
                System.out.println(
                        "Sizes the same, but either a difference in anagrams or anagrams not in correct order.");
            }
            System.out.println();
            passed = false;
        }
        return passed;
    }

    // class to handle the parameters for an anagram test
    // and the expected result
    private static class TestCase {

        private int testCaseNumber;
        private String phrase;
        private int maxWords;
        private List<List<String>> anagrams;

        // pre: sc is positioned at the start of a test case
        private TestCase(Scanner sc) {
            testCaseNumber = Integer.parseInt(sc.nextLine().trim());
            maxWords = Integer.parseInt(sc.nextLine().trim());
            phrase = sc.nextLine().trim();
            anagrams = new ArrayList<>();
            readAndStoreAnagrams(sc);
        }

        // pre: sc is positioned at the start of the resulting anagrams
        // read in the number of anagrams and then for each anagram:
        // - read in the line
        // - break the line up into words
        // - create a new list of Strings for the anagram
        // - add each word to the anagram
        // - add the anagram to the list of anagrams
        private void readAndStoreAnagrams(Scanner sc) {
            int numAnagrams = Integer.parseInt(sc.nextLine().trim());
            for (int j = 0; j < numAnagrams; j++) {
                String[] words = sc.nextLine().split("\\s+");
                ArrayList<String> anagram = new ArrayList<String>();
                for (String st : words) {
                    anagram.add(st);
                }
                anagrams.add(anagram);
            }
            assert anagrams.size() == numAnagrams : "Wrong number of angrams read or expected";
        }
    }
}
