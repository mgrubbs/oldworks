/* CS 314 STUDENTS: FILL IN THIS HEADER.
 *
 * Student information for assignment:
 *
 *  On my honor, Mark Grubbs, this programming assignment is my own work
 *  and I have not provided this code to any other student.
 *
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *  Number of slip days I am using: 1
 */


public class LetterInventory {

    private static final int TOTAL_LETTERS = 26;
    private static final int LETTER_SHIFT = 97;
    private int[] letterCounts = new int[TOTAL_LETTERS];
    private int size;

    // creates a new letter inventory from a string
    // pre: String given is not null
    // post: new object with the word's letters' frequencies stored in array
    // goes through string and increments values in letterCounts that appear in the
    // string
    // size is set to the words length
    public LetterInventory(String word) {
        if (word == null) {
            throw new IllegalArgumentException("Word/Phrase given cannot be null.");
        }
        final String LETTERS_TO_INVENTORY = word.toLowerCase();
        for (int i = 0; i < word.length(); i++) {
            char letterToAdd = LETTERS_TO_INVENTORY.charAt(i);
            if (letterToAdd >= 'a' && letterToAdd <= 'z') {
                letterCounts[letterToAdd - LETTER_SHIFT]++;
            }
        }
        setInitialSize();
    }
    
    private void setInitialSize() {
        for (int i = 0; i < letterCounts.length; i++) {
            size += letterCounts[i];
        }
    }

    // creates a new letterInventory with a given int of frequencies and total
    // letters
    private LetterInventory(int[] amountsOfLetters, int totalLetters) {
        letterCounts = amountsOfLetters;
        size = totalLetters;
    }

    // pre: letter is an english letter
    // post: return the frequency of the letter given
    public int get(char letter) {
        int indexOfLetter = (letter + "").toLowerCase().charAt(0) - LETTER_SHIFT;
        if (indexOfLetter < 0 || indexOfLetter >= TOTAL_LETTERS) {
            throw new IllegalArgumentException("character given must be an english letter");
        }
        return letterCounts[indexOfLetter];
    }

    // pre: none
    // post: returns the amount of letters in the LetterInventory
    public int size() {
        return size;
    }

    // pre: none
    // post: returns true if size of letterInventory = 0; false otherwise
    public boolean isEmpty() {
        return size == 0;
    }

    // pre: none
    // post: returns the letters currently in the letterInventory by groups of
    // amounts
    public String toString() {
        StringBuilder letters = new StringBuilder();
        for (int letter = 0; letter < letterCounts.length; letter++) {
            int letterAmount = letterCounts[letter];
            for (int currentAmount = 0; currentAmount < letterAmount; currentAmount++) {
                letters.append((char) (letter + LETTER_SHIFT));
            }
        }
        return letters.toString();
    }

    // pre: otherInventory is not null
    // post: return a new LetterInventory object with the inventories being added
    // together
    public LetterInventory add(LetterInventory otherInventory) {
        if (otherInventory == null) {
            throw new IllegalArgumentException("The inventory to add should not be null.");
        }
        int[] newInventory = new int[TOTAL_LETTERS];
        int newSize = combineInventories(false, otherInventory.letterCounts, newInventory);
        return new LetterInventory(newInventory, newSize);
    }

    private static final int NEGATIVE_FREQUENCY = -1;

    // combines the inventories of the calling inventory and the passed inventory
    // if the operation is subtraction, the numbers are subtracted first - second
    // else they are added
    // returns the newsize
    // does not alter the old inventories
    // if a subtraction results in a value below zero, the size -1 is returned to
    // indicate a negative frequency
    private int combineInventories(boolean isSubtraction, int[] otherInventoryCounts, int[] newInventory) {
        int size = 0;
        for (int i = 0; i < otherInventoryCounts.length; i++) {
            if (isSubtraction) {
                newInventory[i] = letterCounts[i] - otherInventoryCounts[i];
                if (newInventory[i] < 0) {
                    return NEGATIVE_FREQUENCY;
                }
            } else {
                newInventory[i] = letterCounts[i] + otherInventoryCounts[i];
            }
            size += newInventory[i];
        }
        return size;
    }

    // pre: otherInventory is not null
    // post: return a new LetterInventory object with the second inventory
    // subtracted from the first inventory i.e. first - second
    public LetterInventory subtract(LetterInventory otherInventory) {
        if (otherInventory == null) {
            throw new IllegalArgumentException("The inventory to add should not be null.");
        }
        int[] newInventory = new int[TOTAL_LETTERS];
        int newSize = combineInventories(true, otherInventory.letterCounts, newInventory);
        if (newSize == NEGATIVE_FREQUENCY) {
            return null;
        }
        return new LetterInventory(newInventory, newSize);
    }

    // pre: none
    // post: returns true if all the frequencies of the letters in each the calling
    // object and the object passed are equal, false otherwise
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof LetterInventory) {
            boolean equal = true;
            int index = 0;
            while (equal && index < TOTAL_LETTERS) {
                equal = letterCounts[index] == ((LetterInventory) (obj)).letterCounts[index];
                index++;
            }
            return equal;
        }
        return false;
    }
}
