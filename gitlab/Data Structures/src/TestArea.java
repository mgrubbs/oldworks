import java.io.File;
import java.io.IOException;
import java.util.Map;

public class TestArea {

    public static void main(String[] args) throws IOException {
        
        BitInputStream bitIn = new BitInputStream(new File("ciaFactBook2008.txt"));
        BitOutputStream bitOut = new BitOutputStream("testSmol.txt.hf");
        Compressor c = new Compressor(bitIn);
        c.outputCompression(bitOut, bitIn, IHuffConstants.STORE_COUNTS);
        System.out.println(c.calculateCompressedSize(IHuffConstants.STORE_COUNTS));
        bitIn = new BitInputStream(new File("testSmol.txt.hf"));
        Decompressor m = new Decompressor(bitIn);
        System.out.println(m.getSize());
    }
}
