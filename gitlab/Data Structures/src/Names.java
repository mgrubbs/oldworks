
/*  Student information for assignment:
*
*  On my honor, Mark Grubbs, this programming assignment is my own work
*  and I have not provided this code to any other student.
*
*  UTEID: msg2772
*  email address: siegbalicula@gmail.com
*  Number of slip days I am using:
*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * A collection of NameRecords. Stores NameRecord objects and provides methods
 * to select NameRecords based on various criteria.
 */
public class Names {

    private ArrayList<NameRecord> names;
    private int numRanks;

    /**
     * Construct a new Names object based on the data source the Scanner sc is
     * connected to. Assume the first two lines in the data source are the base year
     * and number of decades to use. Any lines without the correct number of decades
     * are discarded and are not part of the resulting Names object. Any names with
     * ranks of all 0 are discarded and not part of the resulting Names object.
     * 
     * @param sc Is connected to a data file with baby names and positioned at the
     *           start of the data source.
     */
    public Names(Scanner sc) {
        names = new ArrayList<NameRecord>();
        String line = "";
        int baseDecade = sc.nextInt();
        sc.nextLine();
        numRanks = sc.nextInt();
        while (sc.hasNextLine()) {
            line = sc.nextLine();
            String[] nameSplit = line.split("\\s+");
            int amountOfRanks = nameSplit.length;
            if (amountOfRanks == numRanks + 1 && seeIfRanked(nameSplit)) {
                NameRecord currentName = new NameRecord(line, baseDecade);
                names.add(currentName);
            }
        }
        Collections.sort(names);
    }

    // checks if the name has a rank
    private boolean seeIfRanked(String[] name) {
        for (int i = 1; i < name.length; i++) {
            if (Integer.parseInt(name[i]) > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns an ArrayList of NameRecord objects that contain a given substring,
     * ignoring case. The names must be in sorted order based on name.
     * 
     * @param partialName != null, partialName.length() > 0
     * @return an ArrayList of NameRecords whose names contains partialName. If
     *         there are no NameRecords that meet this criteria returns an empty
     *         list.
     */
    public ArrayList<NameRecord> getMatches(String partialName) {
        if (partialName == null || partialName.length() <= 0) {
            throw new IllegalArgumentException(
                    "search parameter, partialName, cannot be null and must have a length greater than 0");
        }
        ArrayList<NameRecord> matches = new ArrayList<NameRecord>();
        final String CHECK = partialName.toLowerCase();
        for (int recordedName = 0; recordedName < names.size(); recordedName++) {
            final String CURRENT_NAME = names.get(recordedName).getName().toLowerCase();
            for (int currentLetter = 0; currentLetter <= CURRENT_NAME.length()
                    - partialName.length(); currentLetter++) {
                final int LENGTH = currentLetter + partialName.length();
                if (CURRENT_NAME.substring(currentLetter, LENGTH).equals(CHECK)) {
                    matches.add(names.get(recordedName));
                }
            }
        }
        return matches;
    }

    /**
     * Returns an ArrayList of Strings of names that have been ranked in the top
     * 1000 or better for every decade. The Strings must be in sorted order based on
     * name.
     * 
     * @return A list of the names that have been ranked in the top 1000 or better
     *         in every decade. The list is in sorted ascending order. If there are
     *         no NameRecords that meet this criteria returns an empty list.
     */
    public ArrayList<String> rankedEveryDecade() {
        ArrayList<String> theBest = new ArrayList<String>();
        for (int i = 0; i < names.size(); i++) {
            if (names.get(i).alwaysRankedTop1000()) {
                theBest.add(names.get(i).getName());
            }
        }
        return theBest;
    }

    /**
     * Returns an ArrayList of Strings of names that have been ranked in the top
     * 1000 or better in exactly one decade. The Strings must be in sorted order
     * based on name.
     * 
     * @return A list of the names that have been ranked in the top 1000 or better
     *         in exactly one decade. The list is in sorted ascending order. If
     *         there are no NameRecords that meet this criteria returns an empty
     *         list.
     */
    public ArrayList<String> rankedOnlyOneDecade() {
        ArrayList<String> theLeast = new ArrayList<String>();
        for (int i = 0; i < names.size(); i++) {
            if (names.get(i).onlyRankedTop1000Once()) {
                theLeast.add(names.get(i).getName());
            }
        }
        return theLeast;
    }

    /**
     * Returns an ArrayList of Strings of names that have been getting more popular
     * every decade. The Strings must be in sorted order based on name.
     * 
     * @return A list of the names that have been getting more popular in every
     *         decade. The list is in sorted ascending order. If there are no
     *         NameRecords that meet this criteria returns an empty list.
     */
    public ArrayList<String> alwaysMorePopular() {
        ArrayList<String> theGrowing = new ArrayList<String>();
        for (int i = 0; i < names.size(); i++) {
            if (names.get(i).isGettingPopularity()) {
                theGrowing.add(names.get(i).getName());
            }
        }
        return theGrowing;
    }

    /**
     * Returns an ArrayList of Strings of names that have been getting less popular
     * every decade. The Strings must be in sorted order based on name.
     * 
     * @return A list of the names that have been getting less popular in every
     *         decade. The list is in sorted ascending order. If there are no
     *         NameRecords that meet this criteria returns an empty list.
     */
    public ArrayList<String> alwaysLessPopular() {
        ArrayList<String> theWaning = new ArrayList<String>();
        for (int i = 0; i < names.size(); i++) {
            if (names.get(i).isLosingPopularity()) {
                theWaning.add(names.get(i).getName());
            }
        }
        return theWaning;
    }

    /**
     * Return the NameRecord in this Names object that matches the given String.
     * <br>
     * <tt>pre: name != null</tt>
     * 
     * @param name The name to search for.
     * @return The name record with the given name or null if no NameRecord in this
     *         Names object contains the given name.
     */
    public NameRecord getName(String name) {
        if (name == null)
            throw new IllegalArgumentException("The parameter name cannot be null");
        for (int i = 0; i < names.size(); i++) {
            if (names.get(i).getName().toLowerCase().equals(name.toLowerCase())) {
                return names.get(i);
            }
        }
        return null;
    }

    /*
     * returns all names that have more vowels than consonants in them or vice versa
     * check for int done in NameSurfer
     * if the amount of vowels and consonants are equal, the consonants will win over the
     * vowels, meaning that if consonants were being looked for then the name will be added
     * to consonants but not added if vowels were sought after
     */
    public ArrayList<NameRecord> namesWithMoreVowelsOrConsonants(int i) {
        final String VOWELS_STRING = "aeiou";   
        ArrayList<Character> vowels = letterList(VOWELS_STRING);
        boolean lfVowels = i == 0;
        ArrayList<NameRecord> moreLetter = new ArrayList<>();
        for(int currentName = 0; currentName < names.size(); currentName++) {
            int totalConsonants = 0;
            int totalVowels = 0;
            for(int letter = 0; letter < names.get(currentName).getName().length(); letter++) {
                if(vowels.contains(names.get(currentName).getName().toLowerCase().charAt(letter))) {
                    totalVowels++;
                } else {
                    totalConsonants++;
                }
            }
            if(totalVowels > totalConsonants && lfVowels) {
                moreLetter.add(names.get(currentName));
            } else if(totalConsonants >= totalVowels && !lfVowels) {
                moreLetter.add(names.get(currentName));
            }
        }
        return moreLetter;
    }
    
    // turns string into a list of letters to be used in conjunction with contains
    private ArrayList<Character> letterList(String letters){
        ArrayList<Character> list = new ArrayList<>();
        for(int i = 0; i < letters.length(); i++) {
            list.add(letters.charAt(i));
        }
        return list;
    }

    

//    /*
//     * precon: decade is within decades ranked postcon: returns the char of the most
//     * used character in a name of that decade if that decade was that name's best
//     * decade
//     */
//    public char getWithTheTimes(int decade) {
//        final int DECADE = 10;
//        if ((decade - names.get(0).getBaseDecade()) / DECADE < 0
//                || (decade - names.get(0).getBaseDecade()) / DECADE > numRanks) { 
//            throw new IllegalArgumentException("Decade outside of the decades ranked.");
//        }
//        int[] chars = new int[26];
//        for (int i = 0; i < names.size(); i++) {
//            if (names.get(i).bestDecade() == decade) {
//                addChars(chars, names.get(i).getName());
//            }
//        }
//        return getHighest(chars);
//    }
//
//    // gets the highest amount of a char from the data set of a decade
//    private char getHighest(int[] chars) {
//        int largest = 0;
//        final int CHAR_SHIFT = 97;
//        for (int i = 1; i < chars.length; i++) {
//            if (chars[i] > chars[largest]) {
//                largest = i;
//            }
//        }
//        return (char) (largest + CHAR_SHIFT);
//    }
//
//    // adds the chars to the chars array counter
//    private void addChars(int[] chars, String name) {
//        String lcName = name.toLowerCase();
//        final int CHAR_SHIFT = 97;
//        for (int i = 0; i < name.length(); i++) {
//            chars[lcName.charAt(i) - CHAR_SHIFT]++;
//        }
//    }
}