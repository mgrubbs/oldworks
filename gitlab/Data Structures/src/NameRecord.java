/*
 * On my honor, Mark Grubbs, this programming assignment is my own work 
 * and I have not provided this code
 * to any other student. 
 * UTEID: msg2772
 * email address: siegbalicula@gmail.com 
 * Number of slip days I am using:
 */

import java.util.ArrayList;
import java.util.Scanner;

public class NameRecord implements Comparable<NameRecord> {

    private static final int DECADE = 10;

    private String name;
    private int baseDecade;
    private ArrayList<Integer> ranks;

    // constructor
    // builds a Name record with the string by using scanner
    // decade given becomes the baseDecade
    public NameRecord(String n, int decade) {
        Scanner stringChopper = new Scanner(n);
        name = stringChopper.next(); // first string is name, ranks follow
        baseDecade = decade;
        ranks = new ArrayList<Integer>();
        while (stringChopper.hasNextInt()) {
            ranks.add(stringChopper.nextInt());
        }
        stringChopper.close();
    }

    // returns stored name
    public String getName() {
        return name;
    }

    // returns stored baseDecade
    public int getBaseDecade() {
        return baseDecade;
    }

    /*
     * precon: Given decade is in the range of decades ranked. i.e. decade <
     * baseDecade or decade > lastDecade postcon: return the rank of a given decade
     * postcon: gives the rank of the decade asked for
     */
    public int getRank(int decade) {
        final int INDEX = (decade - baseDecade) / DECADE; // actual index in ranks
        if (INDEX < 0 || INDEX > ranks.size()) {
            throw new IllegalArgumentException("Given decade was not ranked.");
        }
        return ranks.get(INDEX);
    }

    // returns the decade with the highest value
    public int bestDecade() {
        int bestDecade = Integer.MAX_VALUE;
        for (int i = 0; i < ranks.size(); i++) {
            final int INDEX = baseDecade + i * DECADE;
            if (getRank(INDEX) < bestDecade && getRank(INDEX) > 0) {
                bestDecade = INDEX;
            }
        }
        return bestDecade;
    }

    // returns the amount of times in the top 1000
    public int numTimesTop1000() {
        int times1000 = 0;
        for (int i = 0; i < ranks.size(); i++) {
            final int INDEX = baseDecade + i * DECADE;
            if (getRank(INDEX) > 0) {
                times1000++;
            }
        }
        return times1000;
    }

    // this method and the one following this one have the same structure so both
    // call checkRanks
    public boolean alwaysRankedTop1000() {
        return checkRanks(false);
    }

    public boolean onlyRankedTop1000Once() {
        return checkRanks(true);
    }

    // performs the checks of alwaysRankedTop1000 and onlyRankedTop1000Once with a
    // boolean
    // count all the times that the name has ranked within the top 1000 and if this
    // is ever
    // over 1 and isOnlyOnceCheck, then return false immediately. Otherwise, keeps
    // counting
    // until a 0 is found. If a zero is found return false (for alwaysRankedTop1000)
    private boolean checkRanks(boolean isOnlyOnceCheck) {
        int times1000 = 0;
        for (int i = 0; i < ranks.size(); i++) {
            final int INDEX = baseDecade + i * DECADE;
            if (getRank(INDEX) == 0 && !isOnlyOnceCheck) {
                return isOnlyOnceCheck;
            }
            if (getRank(INDEX) != 0) {
                times1000++;
            }
            if (isOnlyOnceCheck && times1000 > 1) {
                return !isOnlyOnceCheck;
            }
        }
        if (isOnlyOnceCheck) {
            return isOnlyOnceCheck && times1000 != 0;
        }
        return !isOnlyOnceCheck;
    }

    // isGettingPopularity and isLosingPopularity are the same check, but in
    // different directions
    // popularityChange performs the checks
    public boolean isGettingPopularity() {
        return popularityChange(true);
    }

    public boolean isLosingPopularity() {
        return popularityChange(false);
    }

    // performs check in direction based on boolean from isGettingPopularity and
    // isLosingPopularity
    // isGettingPopularity goes forward, isLosingPopularity goes backward
    private boolean popularityChange(boolean gettingPopularity) {
        int direction = 1;
        int previousChecked = 0; // forward and start from front
        int currentChecked = 1;
        int limit = ranks.size();
        if (!gettingPopularity) {
            direction = -1;
            previousChecked = ranks.size() - 1; // backward and start from back
            currentChecked = ranks.size() - 2;
            limit = -1;
        }
        while (currentChecked != limit) {
            final int INDEX_1 = baseDecade + previousChecked * DECADE; // actual indices
            final int INDEX_2 = baseDecade + currentChecked * DECADE;
            if (getRank(INDEX_1) == 0 && getRank(INDEX_2) == 0) { // if the current two being checked are 0, stop
                return false;
            } else if (getRank(INDEX_1) <= getRank(INDEX_2) && getRank(INDEX_1) != 0) { // if the next is a worse rank
                                                                                        // stop
                return false;
            } else if (getRank(INDEX_2) == 0) { // if the next is 0 stop
                return false;
            }
            previousChecked += direction;
            currentChecked += direction;
        }
        return true;
    }

    // toString prints the name and then the ranks in format decade: rank
    public String toString() {
        final String NEW_LINE = "\n";
        StringBuilder output = new StringBuilder();
        output.append(getName());
        output.append(NEW_LINE);
        for (int i = baseDecade; i < baseDecade + ranks.size() * DECADE; i += DECADE) {
            output.append("" + i);
            output.append(": " + getRank(i));
            output.append(NEW_LINE);
        }
        return output.toString();
    }

    // precon: other is not null
    // compareTo compares the string of the other NameRecord to the calling
    // NameRecord
    public int compareTo(NameRecord other) {
        if (other == null) {
            throw new IllegalArgumentException("Other NameRecord cannot be null");
        }
        return getName().compareTo(other.getName());
    }

}
