/*  Student information for assignment:
 *
 *  On MY honor, Mark Grubbs, this programming assignment is MY own work
 *  and I have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose Canvas account is being used) Mark Grubbs
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name: 
 *  Section number:
 *
 *  Student 2
 *  UTEID: doesn't exist
 *  email address:
 *
 */

//imports

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.awt.Graphics;
import java.awt.Color;

/**
 * Various recursive methods to be implemented. Given shell file for CS314
 * assignment.
 */
public class Recursive {

    /**
     * Problem 1: convert a base 10 int to binary recursively. <br>
     * pre: n != Integer.MIN_VALUE<br>
     * <br>
     * post: Returns a String that represents N in binary. All chars in returned
     * String are '1's or '0's. Most significant digit is at position 0
     * 
     * @param n the base 10 int to convert to base 2
     * @return a String that is a binary representation of the parameter n
     */
    public static String getBinary(int n) {
        if (n == Integer.MIN_VALUE) {
            throw new IllegalArgumentException(
                    "Failed precondition: getBinary. " + "n cannot equal Integer.MIN_VALUE. n: " + n);
        }
        if (n == 1 || n == -1) {
            return n + ""; // base case n = 1
        }
        return getBinary(n / 2) + Math.abs(n % 2);
    }

    /**
     * Problem 2: reverse a String recursively.<br>
     * pre: stringToRev != null<br>
     * post: returns a String that is the reverse of stringToRev
     * 
     * @param stringToRev the String to reverse.
     * @return a String with the characters in stringToRev in reverse order.
     */
    public static String revString(String stringToRev) {
        if (stringToRev == null) {
            throw new IllegalArgumentException("Failed precondition: revString. parameter may not be null.");
        }
        if (stringToRev.length() == 1)
            return stringToRev; // base case there is only on last letter
        return stringToRev.charAt(stringToRev.length() - 1)
                + revString(stringToRev.substring(0, stringToRev.length() - 1));
    }

    /**
     * Problem 3: Returns the number of elements in data that are followed directly
     * by value that is double that element. pre: data != null post: return the
     * number of elements in data that are followed immediately by double the value
     * 
     * @param data The array to search.
     * @return The number of elements in data that are followed immediately by a
     *         value that is double the element.
     */
    public static int nextIsDouble(int[] data) {
        if (data == null) {
            throw new IllegalArgumentException("Failed precondition: nextIsDouble. parameter may not be null.");
        }
        final int STARTING_POSITION = 0;
        return getNextIsDouble(data, STARTING_POSITION);
    }

    // helper method for nextIsDouble; gets the number of times the next number in
    // the list is double the current number by going through the list until the
    // next number to check is the end of the list
    private static int getNextIsDouble(int[] data, int pos) {
        int totalNextIsDouble = 0;
        if (pos + 1 == data.length) {
            return 0; // base case at the end of the data
        }
        if (data[pos + 1] == data[pos] * 2) {
            totalNextIsDouble++;
        }
        totalNextIsDouble += getNextIsDouble(data, pos + 1);
        return totalNextIsDouble;
    }

    /**
     * Problem 4: Find all combinations of mnemonics for the given number.<br>
     * pre: number != null, number.length() > 0, all characters in number are
     * digits<br>
     * post: see tips section of assignment handout
     * 
     * @param number The number to find mnemonics for
     * @return An ArrayList<String> with all possible mnemonics for the given number
     */
    public static ArrayList<String> listMnemonics(String number) {
        if (number == null || number.length() == 0 || !allDigits(number)) {
            throw new IllegalArgumentException("Failed precondition: listMnemonics");
        }
        ArrayList<String> result = new ArrayList<>();
        recursiveMnemonics(result, "", number);
        return result;
    }

    /*
     * Helper method for listMnemonics mnemonics stores completed mnemonics
     * mneominicSoFar is a partial (possibly complete) mnemonic digitsLeft are the
     * digits that have not been used from the original number
     */
    private static void recursiveMnemonics(ArrayList<String> mnemonics, String mnemonicSoFar, String digitsLeft) {
        if (digitsLeft.length() == 1) { // base case is that the amount of digits left is one
            // in this case just append the possible choices for that digit to the
            // mnemonicSoFar
            final String LETTERS_OF_DIGIT = digitLetters(digitsLeft.charAt(0));
            for (int characterOfDigit = 0; characterOfDigit < LETTERS_OF_DIGIT.length(); characterOfDigit++) {
                mnemonics.add(mnemonicSoFar + LETTERS_OF_DIGIT.charAt(characterOfDigit));
            }
        } else {
            // in any other case, loop through choices and append the choices and pass the
            // new choice to the recursive loop with mnemonicSoFar
            final String LETTERS = digitLetters(digitsLeft.charAt(0));
            for (int currentLetter = 0; currentLetter < LETTERS.length(); currentLetter++) {
                recursiveMnemonics(mnemonics, mnemonicSoFar + LETTERS.charAt(currentLetter),
                        digitsLeft.substring(1, digitsLeft.length()));
            }
        }
    }

    // used by method digitLetters
    private static final String[] letters = { "0", "1", "ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ" };

    /*
     * helper method for recursiveMnemonics pre: ch is a digit '0' through '9' post:
     * return the characters associated with this digit on a phone keypad
     */
    private static String digitLetters(char ch) {
        if (ch < '0' || ch > '9') {
            throw new IllegalArgumentException("parameter ch must be a digit, 0 to 9. Given value = " + ch);
        }
        int index = ch - '0';
        return letters[index];
    }

    /*
     * helper method for listMnemonics pre: s != null post: return true if every
     * character in s is a digit ('0' through '9')
     */
    private static boolean allDigits(String s) {
        if (s == null) {
            throw new IllegalArgumentException("Failed precondition: allDigits. String s cannot be null.");
        }
        boolean allDigits = true;
        int i = 0;
        while (i < s.length() && allDigits) {
            allDigits = s.charAt(i) >= '0' && s.charAt(i) <= '9';
            i++;
        }
        return allDigits;
    }

    /**
     * Problem 5: Draw a Sierpinski Carpet.
     * 
     * @param size  the size in pixels of the window
     * @param limit the smallest size of a square in the carpet.
     */
    public static void drawCarpet(int size, int limit) {
        DrawingPanel p = new DrawingPanel(size, size);
        Graphics g = p.getGraphics();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, size, size);
        g.setColor(Color.WHITE);
        drawSquares(g, size, limit, 0, 0);
    }

    /*
     * Helper method for drawCarpet Draw the individual squares of the carpet.
     * 
     * @param g The Graphics object to use to fill rectangles
     * 
     * @param size the size of the current square
     * 
     * @param limit the smallest allowable size of squares
     * 
     * @param x the x coordinate of the upper left corner of the current square
     * 
     * @param y the y coordinate of the upper left corner of the current square
     */
    private static void drawSquares(Graphics g, int size, int limit, double x, double y) {
        final double WHITE_SQUARE_VARIABLES = 1.0 / 3 * size;
        final int SEPARATION_FACTOR = 3;
        // only draw white square
        g.fillRect((int) (x + WHITE_SQUARE_VARIABLES), (int) (y + WHITE_SQUARE_VARIABLES), (int) WHITE_SQUARE_VARIABLES,
                (int) WHITE_SQUARE_VARIABLES); // base case is that 1 / 3 * size < limit
        if ((int) (WHITE_SQUARE_VARIABLES) >= limit) {
            for (int currentX = 0; currentX < SEPARATION_FACTOR; currentX++) {
                int actualX = (int) (x + currentX * WHITE_SQUARE_VARIABLES); // segment the square into 9 pieces
                for (int currentY = 0; currentY < SEPARATION_FACTOR; currentY++) {
                    int actualY = (int) (y + currentY * WHITE_SQUARE_VARIABLES);
                    drawSquares(g, (int) WHITE_SQUARE_VARIABLES, limit, actualX, actualY);
                }
            }
        }
    }

    /**
     * Problem 6: Determine if water at a given point on a map can flow off the map.
     * <br>
     * pre: map != null, map.length > 0, map is a rectangular matrix, 0 <= row <
     * map.length, 0 <= col < map[0].length <br>
     * post: return true if a drop of water starting at the location specified by
     * row, column can reach the edge of the map, false otherwise.
     * 
     * @param map The elevations of a section of a map.
     * @param row The starting row of a drop of water.
     * @param col The starting column of a drop of water.
     * @return return true if a drop of water starting at the location specified by
     *         row, column can reach the edge of the map, false otherwise.
     */
    public static boolean canFlowOffMap(int[][] map, int row, int col) {
        if (map == null || map.length == 0 || !isRectangular(map) || !inbounds(row, col, map)) {
            throw new IllegalArgumentException("Failed precondition: canFlowOffMap");
        }
        return canFlowOff(map, row, col);
    }

    // helper method for canFlowOffMap; uses an unraveled loop to check all
    // directions of the map for an exit to the edge.
    private static boolean canFlowOff(int[][] map, int row, int col) {
        if (row - 1 < 0 || row + 1 == map.length || col - 1 < 0 || col + 1 == map[0].length)
            return true; // base case is that cell at is the edge
        boolean canFlow = false;
        if (map[row - 1][col] < map[row][col]) // north
            canFlow = canFlow || canFlowOff(map, row - 1, col);
        if (canFlow) // this check is here to save time potentially
            return true;
        if (map[row + 1][col] < map[row][col]) // south
            canFlow = canFlow || canFlowOff(map, row + 1, col);
        if (canFlow)
            return true;
        if (map[row][col - 1] < map[row][col]) // west
            canFlow = canFlow || canFlowOff(map, row, col - 1);
        if (canFlow)
            return true;
        if (map[row][col + 1] < map[row][col]) // east
            canFlow = canFlow || canFlowOff(map, row, col + 1);
        return canFlow; // if we get here either east worked or nothing worked
    }

    /*
     * helper method for canFlowOfMap - CS314 students you should not have to call
     * this method, pre: mat != null,
     */
    private static boolean inbounds(int r, int c, int[][] mat) {
        if (mat == null) {
            throw new IllegalArgumentException("Failed precondition: inbounds. The 2d array mat may not be null.");
        }
        return r >= 0 && r < mat.length && mat[r] != null && c >= 0 && c < mat[r].length;
    }

    /*
     * helper method for canFlowOfMap - CS314 students you should not have to call
     * this method, pre: mat != null, mat.length > 0 post: return true if mat is
     * rectangular
     */
    private static boolean isRectangular(int[][] mat) {
        if ((mat == null) || (mat.length == 0)) {
            throw new IllegalArgumentException("Failed precondition: isRectangular. "
                    + "The argument mat may not be null and must have at least one row.");
        }

        boolean correct = true;
        final int NUM_COLS = mat[0].length;
        int row = 0;
        while (correct && row < mat.length) {
            correct = (mat[row] != null) && (mat[row].length == NUM_COLS);
            row++;
        }
        return correct;
    }

    /**
     * Problem 7: Find the minimum difference possible between teams based on
     * ability scores. The number of teams may be greater than 2. The goal is to
     * minimize the difference between the team with the maximum total ability and
     * the team with the minimum total ability. <br>
     * pre: numTeams >= 2, abilities != null, abilities.length >= numTeams <br>
     * post: return the minimum possible difference between the team with the
     * maximum total ability and the team with the minimum total ability.
     * 
     * @param numTeams  the number of teams to form. Every team must have at least
     *                  one member
     * @param abilities the ability scores of the people to distribute
     * @return return the minimum possible difference between the team with the
     *         maximum total ability and the team with the minimum total ability.
     *         The return value will be greater than or equal to 0.
     */
    public static int minDifference(int numTeams, int[] abilities) {
        if (numTeams < 2 || abilities == null || abilities.length < numTeams) {
            throw new IllegalArgumentException(
                    "numTeams cannot be less than 2; abilities cannot be null; amount of people must be enough to make an amount of numTeams");
        }
        int[] teams = new int[numTeams];
        int[] teamMembers = new int[numTeams];
        int firstPerson = 0;
        return getMinDiff(teams, abilities, firstPerson, teamMembers);
    }

    // method for getting the minDifference
    private static int getMinDiff(int[] teams, int[] abilities, int currentPerson, int[] numTeamMembers) {
        // base case is that there is no more people to divide up
        // if there are any empty teams, this is invalid
        if (currentPerson == abilities.length) {
            if (checkIfTeamsEmpty(numTeamMembers)) {
                return Integer.MAX_VALUE;
            }
            return getCurrentMinDiff(teams);
        }
        // recursively backtrack through teams by adding the person to the teams
        int currentMember = abilities[currentPerson];
        int currentDiff = Integer.MAX_VALUE;
        for (int i = 0; i <= Math.min(currentPerson, teams.length - 1); i++) {
            teams[i] += currentMember;
            numTeamMembers[i]++;
            int currentDifference = getMinDiff(teams, abilities, currentPerson + 1, numTeamMembers);
            if (currentDiff > currentDifference) {
                currentDiff = currentDifference;
                // currentDiff = getMinDiff(teams, abilities, currentPerson + 1,
                // numTeamMembers);

            }
            teams[i] -= currentMember;
            numTeamMembers[i]--;
        }
        return currentDiff;
    }

    // checks if a team has no team members
    private static boolean checkIfTeamsEmpty(int[] numTeamMembers) {
        for (int i = 0; i < numTeamMembers.length; i++) {
            if (numTeamMembers[i] == 0) {
                return true;
            }
        }
        return false;
    }

    // gets the current difference between the highest team and the lowest team
    private static int getCurrentMinDiff(int[] teams) {
        int highest = Integer.MIN_VALUE;
        int lowest = Integer.MAX_VALUE;
        for (int i = 0; i < teams.length; i++) {
            if (highest < teams[i]) {
                highest = teams[i];
            }
            if (lowest > teams[i]) {
                lowest = teams[i];
            }
        }
        return highest - lowest;
    }

    /**
     * Problem 8: Maze solver. Return 2 if it is possible to escape the maze after
     * collecting all the coins. Return 1 if it is possible to escape the maze but
     * without collecting all the coins. Return 0 if it is not possible to escape
     * the maze. More details in the assignment handout. <br>
     * pre: board != null <br>
     * pre: board is a rectangular matrix <br>
     * pre: board only contains characters 'S', 'E', '$', 'G', 'Y', and '*' <br>
     * pre: there is a single 'S' character present <br>
     * post: rawMaze is not altered as a result of this method. Return 2 if it is
     * possible to escape the maze after collecting all the coins. Return 1 if it is
     * possible to escape the maze but without collecting all the coins. Return 0 if
     * it is not possible to escape the maze. More details in the assignment
     * handout.
     * 
     * @param rawMaze represents the maze we want to escape. rawMaze is not altered
     *                as a result of this method.
     * @return per the post condition
     */
    public static int canEscapeMaze(char[][] rawMaze) {
        if (!checkPreconditions(rawMaze)) {
            throw new IllegalArgumentException(
                    "Maze given is either null, not rectangular, contains invalid characters, or has more than one starting position.");
        }
        Maze realMaze = new Maze(rawMaze);
        if (!realMaze.hasExit) {
            return 0;
        }
        return canEscape(realMaze, realMaze.x, realMaze.y);
    }

    // goes through the different directions and finds whatever path is the greatest
    private static int canEscape(Maze realMaze, int row, int col) {
        if (realMaze.countCoins() == 0 && realMaze.theMaze[row][col] == 'E') {
            return 2; // base case that all coins collected and at exit
        }
        if (realMaze.theMaze[row][col] == 'E') {
            return 1; // base case that just at exit
        }
        final char CURRENT_CELL = realMaze.theMaze[row][col];
        int result = 0;
        // each of the four checks does an update where it changes the cell to what
        // it should be next, a change of the result to whatever is bigger, result
        // or canEscape in the direction it goes, a return of 2 if the result is 2,
        // and a restoration change if the above failed
        // checks the east direction
        if (inbounds(realMaze, row, col + 1)) {
            update(realMaze, row, col);
            result = Math.max(result, canEscape(realMaze, row, col + 1));
            if (result == 2) {
                return result;
            }
            realMaze.changeCell(row, col, CURRENT_CELL);
        }
        // checks the west direction
        if (inbounds(realMaze, row, col - 1)) {
            update(realMaze, row, col);
            result = Math.max(result, canEscape(realMaze, row, col - 1));
            if (result == 2) {
                return result;
            }
            realMaze.changeCell(row, col, CURRENT_CELL);
        }
        // checks the south directions
        if (inbounds(realMaze, row + 1, col)) {
            update(realMaze, row, col);
            result = Math.max(result, canEscape(realMaze, row + 1, col));
            if (result == 2) {
                return result;
            }
            realMaze.changeCell(row, col, CURRENT_CELL);
        }
        // checks the north direction
        if (inbounds(realMaze, row - 1, col)) {
            update(realMaze, row, col);
            result = Math.max(result, canEscape(realMaze, row - 1, col));
            if (result == 2) {
                return result;
            }
            realMaze.changeCell(row, col, CURRENT_CELL);
        }
        return result;
    }

    // checks if next option is inbounds of the maze
    private static boolean inbounds(Maze maze, int row, int col) {
        return maze.theMaze.length > row && row >= 0 && maze.theMaze[0].length > col && col >= 0
                && maze.theMaze[row][col] != '*';
    }

    // updates the maze depending on what cell was left
    private static void update(Maze maze, int row, int col) {
        char currentCell = maze.theMaze[row][col];
        if (currentCell == '$') {
            maze.theMaze[row][col] = 'Y';
        } else if (currentCell == 'G') {
            maze.theMaze[row][col] = 'Y';
        } else if (currentCell == 'Y') {
            maze.theMaze[row][col] = '*';
        } else if (currentCell == 'S') {
            maze.theMaze[row][col] = 'G';
        }
    }

    // checks every precondition of the rawMaze
    private static boolean checkPreconditions(char[][] rawMaze) {
        if (rawMaze == null || rawMaze.length == 0) {
            throw new IllegalArgumentException("Maze cannot be null or empty.");
        }
        boolean passesPrecon = true;
        passesPrecon = passesPrecon && isRect(rawMaze);
        passesPrecon = passesPrecon && checkCharacters(rawMaze);
        return passesPrecon;
    }

    private static final List<Character> validTiles = Arrays.asList('S', 'E', '$', 'G', 'Y', '*');

    // checks the number of S and checks if all the characters are in validTiles
    private static boolean checkCharacters(char[][] rawMaze) {
        int countS = 0;
        for (int row = 0; row < rawMaze.length; row++) {
            for (int column = 0; column < rawMaze[row].length; column++) {
                if (!validTiles.contains(rawMaze[row][column])) {
                    return false;
                }
                if (rawMaze[row][column] == 'S') {
                    countS++;
                }
            }
        }
        return countS == 1;
    }

    // checks if the maze is rectangular by checking if each row has
    // equal length
    private static boolean isRect(char[][] rawMaze) {
        int firstRow = rawMaze[0].length;
        for (int i = 0; i < rawMaze.length; i++) {
            if (rawMaze[i].length != firstRow) {
                return false;
            }
        }
        return true;
    }

    private static class Maze {

        private char[][] theMaze;
        private int coinsLeft;
        private int x;
        private int y;
        private boolean hasExit;

        // instantiates maze to have the same values as the char[][] given
        // instantiates x and y to where S is
        // instantiates hasExit
        public Maze(char[][] maze) {
            theMaze = new char[maze.length][maze[0].length];
            for (int x = 0; x < maze.length; x++) {
                for (int y = 0; y < maze[0].length; y++) {
                    theMaze[x][y] = maze[x][y];
                    if (maze[x][y] == 'S') {
                        this.x = x;
                        this.y = y;
                    }
                    if (maze[x][y] == 'E') {
                        hasExit = true;
                    }
                }
            }
            coinsLeft = countCoins();
        }

        // counts the coins in the maze for seeing if 2 is possible
        public int countCoins() {
            int coins = 0;
            for (int i = 0; i < theMaze.length; i++) {
                for (int j = 0; j < theMaze[i].length; j++) {
                    if (theMaze[i][j] == '$') {
                        coins++;
                    }
                }
            }
            return coins;
        }

        // changes a cell to another type
        public void changeCell(int row, int col, char changeTo) {
            theMaze[row][col] = changeTo;
        }

        // decrements coin when coin found
        public void takeCoin() {
            coinsLeft--;
        }
    }
}