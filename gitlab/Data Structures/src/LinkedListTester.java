/*  Student information for assignment:
*
*  On my honor, Mark Grubbs, this programming assignment is my own work
*  and I have not provided this code to any other student.
*
*  Name: Mark Grubbs
*  email address: siegbalicula@gmail.com
*  UTEID: msg2772
*  Grader name:
*  Number of slip days used on this assignment:
*/

//Experiment results. CS314 students, place your experiment
//results here:
// a) The operations that are about the same are adding at the end and getting all using the iterator.
// The operations that LinkedList does better are removing from the front and adding at the front.
// The operations that ArrayList does better are getting a random value from the list and getting
// all using the get method.
// b) The Big O of adding at the end and getting all using the iterator are around O(1) because the times
// double when N doubles. The Big O of getting a random value from the list and getting
// all using the get method are around O(N^2) because the time increases by a factor of 4 when N is doubled.
// The Big O of removing from the front and adding at the front are around O(1) because time doubles when N
// doubles.

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;

public class LinkedListTester {

    public static void main(String[] args) {

        // CS314 students. Add your tests here:
        // As a preface, toString() is not tested as every test uses toString()
        LinkedList<Object> l = new LinkedList<>();
        Object[] expected = new Object[0];
        String actual = l.toString();
        // constructor
        test("Testing constructor", 1, expected, actual);
        // addFirst 3 tests
        l.addFirst("banana");
        expected = new Object[] { "banana" };
        actual = l.toString();
        test("Testing addFirst", 2, expected, actual);
        expected = new Object[] { 64, 66, "banana" };
        l.addFirst(66);
        l.addFirst(64);
        actual = l.toString();
        test("Testing addFirst", 3, expected, actual);
        expected = new Object[] { 5.1, 3.2, 64, 66, "banana" };
        l.addFirst(3.2);
        l.addFirst(5.1);
        actual = l.toString();
        test("Testing addFirst", 4, expected, actual);
        // addLast
        l.addLast("Oh no");
        expected = new Object[] { 5.1, 3.2, 64, 66, "banana", "Oh no" };
        actual = l.toString();
        test("Testing addLast", 5, expected, actual);
        l.addLast(123.123);
        expected = new Object[] { 5.1, 3.2, 64, 66, "banana", "Oh no", 123.123 };
        actual = l.toString();
        test("Testing addLast", 6, expected, actual);
        l.addLast(8);
        expected = new Object[] { 5.1, 3.2, 64, 66, "banana", "Oh no", 123.123, 8 };
        actual = l.toString();
        test("Testing addLast", 7, expected, actual);
        // removeFirst
        l.removeFirst();
        expected = new Object[] { 3.2, 64, 66, "banana", "Oh no", 123.123, 8 };
        actual = l.toString();
        test("Testing removeFirst", 8, expected, actual);
        l.removeFirst();
        l.removeFirst();
        expected = new Object[] { 66, "banana", "Oh no", 123.123, 8 };
        actual = l.toString();
        test("Testing removeFirst", 9, expected, actual);
        l.removeFirst();
        expected = new Object[] { "banana", "Oh no", 123.123, 8 };
        actual = l.toString();
        test("Testing removeFirst", 10, expected, actual);
        // removeLast
        l.removeLast();
        expected = new Object[] { "banana", "Oh no", 123.123 };
        actual = l.toString();
        test("Testing removeLast", 11, expected, actual);
        l.removeLast();
        expected = new Object[] { "banana", "Oh no" };
        actual = l.toString();
        test("Testing removeLast", 12, expected, actual);
        l.removeLast();
        l.removeLast();
        expected = new Object[0];
        actual = l.toString();
        test("Testing removeLast", 13, expected, actual);
        // add
        l.add("bananas");
        l.add("apples");
        double bread = 3.99;
        l.add(bread);
        expected = new Object[] { "bananas", "apples", 3.99 };
        actual = l.toString();
        test("Testing add", 14, expected, actual);
        l.add("chips");
        l.add(Integer.parseInt("143123"));
        expected = new Object[] { "bananas", "apples", 3.99, "chips", 143123 };
        actual = l.toString();
        test("Testing add", 15, expected, actual);
        l.add(l.toString());
        expected = new Object[] { "bananas", "apples", 3.99, "chips", 143123,
                "[bananas, apples, 3.99, chips, 143123]" };
        actual = l.toString();
        test("Testing add", 16, expected, actual);
        // set, size and get
        l.set(l.size() - 1, l.get(l.size() - 2));
        expected = new Object[] { "bananas", "apples", 3.99, "chips", 143123, 143123 };
        actual = l.toString();
        test("Testing set, size and get", 17, expected, actual);
        l.set(0, l.get(l.size() / 2));
        expected = new Object[] { "chips", "apples", 3.99, "chips", 143123, 143123 };
        actual = l.toString();
        test("Testing set, size and get", 18, expected, actual);
        l.set(l.size() - 1, l.set(0, l.get(2)));
        expected = new Object[] { 3.99, "apples", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing set, size and get", 19, expected, actual);
        // insert
        l.insert(1, 6);
        expected = new Object[] { 3.99, 6, "apples", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing insert", 20, expected, actual);
        l.insert(0, 6);
        expected = new Object[] { 6, 3.99, 6, "apples", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing insert", 21, expected, actual);
        l.insert(l.size() / 2, "Kudamono");
        expected = new Object[] { 6, 3.99, 6, "apples", "Kudamono", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing insert", 22, expected, actual);
        // remove(pos) & remove(obj)
        l.remove(l.remove(0));
        expected = new Object[] { 3.99, "apples", "Kudamono", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing remove", 22, expected, actual);
        l.remove(l.remove(0));
        expected = new Object[] { "apples", "Kudamono", "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing remove", 23, expected, actual);
        l.remove(l.remove(2));
        expected = new Object[] { "apples", "Kudamono", 143123 };
        actual = l.toString();
        test("Testing remove", 24, expected, actual);
        // equals test
        equalsTest();
        // subList and removeRange
        IList<Object> subList = l.getSubList(0, 2);
        expected = new Object[] { "apples", "Kudamono" };
        actual = subList.toString();
        test("Testing subList", 28, expected, actual);
        for (Object o : subList) {
            l.add(o);
        }
        l = (LinkedList<Object>) l.getSubList(0, 3);
        expected = new Object[] { "apples", "Kudamono", 143123 };
        actual = l.toString();
        test("Testing subList", 29, expected, actual);
        l.add(((LinkedList<Object>) l.getSubList(0, 2)).toString());
        expected = new Object[] { "apples", "Kudamono", 143123, "[apples, Kudamono]" };
        actual = l.toString();
        test("Testing subList", 30, expected, actual);
        l.removeRange(1, l.size());
        expected = new Object[] { "apples" };
        actual = l.toString();
        test("Testing removeRange", 31, expected, actual);
        l.removeRange(0, 1);
        expected = new Object[] {};
        actual = l.toString();
        test("Testing removeRange", 32, expected, actual);
        l = new LinkedList<>();
        l.add("hahaha");
        l.add("hihihi");
        l.add("hohoho");
        l.add("hehehe");
        l.add("huhuhu");
        l.removeRange(1, 4);
        actual = l.toString();
        expected = new Object[] { "hahaha", "huhuhu" };
        test("Testing removeRange", 33, expected, actual);
        // makeEmpty()
        l.makeEmpty();
        actual = l.toString();
        expected = new Object[] {};
        test("Testing removeRange", 34, expected, actual);
        l.add(l.toString());
        l.makeEmpty();
        actual = l.toString();
        expected = new Object[] {};
        test("Testing removeRange", 35, expected, actual);
        for (int i = 0; i < 1000000; i++) {
            l.add(i);
        }
        l.makeEmpty();
        actual = l.toString();
        expected = new Object[] {};
        test("Testing removeRange", 36, expected, actual);
        // indexOf(pos) and indexOf(pos, start)
        indexTest();
        // iterator
        iteratorTests(9, 43);
        // iteratorTests(8, 45);
        // iteratorTests(5, 47);

        // CS314 Students:
        // uncomment the following line to run tests comparing
        // your LinkedList class to the java ArrayList class.
        comparison();
    }

    private static void iteratorTests(int n, int testNum) {
        LinkedList<Object> l = new LinkedList<>();
        Random r = new Random();
        StringBuilder e = new StringBuilder();
        int currentInt = 0;
        for (int i = 0; i < n; i++) {
            currentInt = r.nextInt(26);
            e.append((char) (currentInt + 97));
            l.add((char) (currentInt + 97));
        }
        Iterator<Object> i = l.iterator();
        StringBuilder a = new StringBuilder();
        while (i.hasNext()) {
            a.append(i.next());
        }
        i = l.iterator();
        ArrayList<Character> a2 = new ArrayList<>();
        while (i.hasNext()) {
            a2.add((Character) i.next());
            if (i.hasNext()) {
                i.next();
                i.remove();
            }
        }
        System.out.println("\nTesting iterator next/hasNext Test number: " + testNum);
        System.out.println("Expected: " + e.toString());
        System.out.println("Actual: " + a.toString());
        if (e.toString().equals(a.toString())) {
            System.out.println("Test number: " + testNum + " passed");
        } else {
            System.out.println("Test number: " + testNum + " failed");
        }
        System.out.println("\nTesting iterator remove Test number: " + (testNum + 1));
        System.out.println("Expected: " + a2.toString());
        System.out.println("Actual: " + l.toString());
        if (l.toString().equals(a2.toString())) {
            System.out.println("Test number: " + (testNum + 1) + " passed");
        } else {
            System.out.println("Test number: " + (testNum + 1) + " failed");
        }

    }

    private static void indexTest() {
        LinkedList<Object> l = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            l.add(i);
            l.add(i * 2);
            l.add(i * 4);
        }
        System.out.println("\nTesting indexOf Test number: " + 37);
        System.out.println("Expected: " + 0);
        System.out.println("Actual: " + l.indexOf(0));
        if (l.indexOf(0) == 0) {
            System.out.println("Test number: " + 37 + " passed");
        } else {
            System.out.println("Test number: " + 37 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 38);
        System.out.println("Expected: " + 2);
        System.out.println("Actual: " + l.indexOf(0, 2));
        if (l.indexOf(0, 2) == 2) {
            System.out.println("Test number: " + 38 + " passed");
        } else {
            System.out.println("Test number: " + 38 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 39);
        System.out.println("Expected: " + 5);
        System.out.println("Actual: " + l.indexOf(4));
        if (l.indexOf(4) == 5) {
            System.out.println("Test number: " + 39 + " passed");
        } else {
            System.out.println("Test number: " + 39 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 40);
        System.out.println("Expected: " + 7);
        System.out.println("Actual: " + l.indexOf(4, 6));
        if (l.indexOf(4, 6) == 7) {
            System.out.println("Test number: " + 40 + " passed");
        } else {
            System.out.println("Test number: " + 40 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 41);
        System.out.println("Expected: " + 8);
        System.out.println("Actual: " + l.indexOf(8));
        if (l.indexOf(8) == 8) {
            System.out.println("Test number: " + 41 + " passed");
        } else {
            System.out.println("Test number: " + 41 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 42);
        System.out.println("Expected: " + 13);
        System.out.println("Actual: " + l.indexOf(8, 9));
        if (l.indexOf(8, 9) == 13) {
            System.out.println("Test number: " + 42 + " passed");
        } else {
            System.out.println("Test number: " + 42 + " failed");
        }
    }

    private static void test(String testCase, int testNumber, Object[] expect, String LLString) {
        System.out.println("\n" + testCase + " Test number: " + testNumber);
        System.out.println("Expected: " + Arrays.toString(expect));
        System.out.println("Actual: " + LLString);
        if (Arrays.toString(expect).equals(LLString)) {
            System.out.println("Test number: " + testNumber + " passed");
        } else {
            System.out.println("Test number: " + testNumber + " failed");
        }
    }

    private static void equalsTest() {
        LinkedList<Object> unga = new LinkedList<>();
        LinkedList<Object> bunga = new LinkedList<>();
        System.out.println("\nTesting Equals Test number: " + 25);
        System.out.println("Expected: " + true);
        System.out.println("Actual: " + unga.equals(bunga));
        if (unga.equals(bunga)) {
            System.out.println("Test number: " + 25 + " passed");
        } else {
            System.out.println("Test number: " + 25 + " failed");
        }
        unga.add("banana");
        bunga.add("apple");
        System.out.println("\nTesting Equals Test number: " + 26);
        System.out.println("Expected: " + false);
        System.out.println("Actual: " + unga.equals(bunga));
        if (!unga.equals(bunga)) {
            System.out.println("Test number: " + 26 + " passed");
        } else {
            System.out.println("Test number: " + 26 + " failed");
        }
        unga.insert(0, "apple");
        bunga.add("banana");
        System.out.println("\nTesting Equals Test number: " + 27);
        System.out.println("Expected: " + true);
        System.out.println("Actual: " + unga.equals(bunga));
        if (unga.equals(bunga)) {
            System.out.println("Test number: " + 27 + " passed");
        } else {
            System.out.println("Test number: " + 27 + " failed");
        }
    }

    private static Object[] toArray(LinkedList<String> list) {
        Object[] result = new Object[list.size()];
        Iterator<String> it = list.iterator();
        int index = 0;
        while (it.hasNext()) {
            result[index] = it.next();
            index++;
        }
        return result;
    }

    // pre: none
    private static boolean arraysSame(Object[] one, Object[] two) {
        boolean same;
        if (one == null || two == null) {
            same = (one == two);
        } else {
            // neither one or two are null
            assert one != null && two != null;
            same = one.length == two.length;
            if (same) {
                int index = 0;
                while (index < one.length && same) {
                    same = (one[index].equals(two[index]));
                    index++;
                }
            }
        }
        return same;
    }

    public static final int NUM_DOUBLINGS_OF_N = 5;
    private static final int NUM_REPEATS_OF_TEST = 100;

    // A method to be run to compare the LinkedList you are completing and the Java
    // ArrayList class
    public static void comparison() {
        Stopwatch s = new Stopwatch();

        int initialN = 30000;
        addEndArrayList(s, initialN, NUM_DOUBLINGS_OF_N);
        addEndLinkedList(s, initialN, NUM_DOUBLINGS_OF_N);

        initialN = 2000;
        addFrontArrayList(s, initialN, NUM_DOUBLINGS_OF_N);
        initialN = 10000;
        addFrontLinkedList(s, initialN, NUM_DOUBLINGS_OF_N);

        initialN = 2000;
        removeFrontArrayList(s, initialN, NUM_DOUBLINGS_OF_N);
        initialN = 5000;
        removeFrontLinkedList(s, initialN, NUM_DOUBLINGS_OF_N);

        initialN = 10000;
        getRandomArrayList(s, initialN, NUM_DOUBLINGS_OF_N);
        initialN = 1000;
        getRandomLinkedList(s, initialN, NUM_DOUBLINGS_OF_N);

        initialN = 50000;
        getAllArrayListUsingIterator(s, initialN, NUM_DOUBLINGS_OF_N);
        getAllLinkedListUsingIterator(s, initialN, NUM_DOUBLINGS_OF_N);

        initialN = 100000;
        getAllArrayListUsingGetMethod(s, initialN, NUM_DOUBLINGS_OF_N);
        initialN = 1000;
        getAllLinkedListUsingGetMethod(s, initialN, NUM_DOUBLINGS_OF_N);

    }

    // These pairs of method illustarte a failure to use polymorphism
    // If the students had implemented the Java list interface there
    // could be a single method. Also we could use function objects to
    // reduce the awful repitition of code.
    public static void addEndArrayList(Stopwatch s, int initialN, int numTests) {

        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            for (int i = 0; i < numTests; i++) {
                ArrayList<Integer> javaList = new ArrayList<>();
                s.start();
                for (int j = 0; j < n; j++) {
                    javaList.add(j);
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Adding at end: ArrayList", totalTimes, initialN);
    }

    private static void showResults(String title, double[] times, int initialN) {
        System.out.println();
        System.out.println("Num Repeats: " + NUM_REPEATS_OF_TEST);
        System.out.println(title);
        for (double time : times) {
            System.out.print("N = " + initialN + ", total time: ");
            System.out.printf("%7.4f\n", time);
            initialN *= 2;
        }
        System.out.println();
    }

    public static void addEndLinkedList(Stopwatch s, int initialN, int numTests) {
        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            for (int i = 0; i < numTests; i++) {
                LinkedList<Integer> studentList = new LinkedList<>();
                s.start();
                for (int j = 0; j < n; j++) {
                    studentList.add(j);
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Adding at end: LinkedList", totalTimes, initialN);
    }

    public static void addFrontArrayList(Stopwatch s, int initialN, int numTests) {

        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            for (int i = 0; i < numTests; i++) {
                ArrayList<Integer> javaList = new ArrayList<>();
                s.start();
                for (int j = 0; j < n; j++) {
                    javaList.add(0, j);
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Adding at front: ArrayList", totalTimes, initialN);
    }

    public static void addFrontLinkedList(Stopwatch s, int initialN, int numTests) {
        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            for (int i = 0; i < numTests; i++) {
                LinkedList<Integer> studentList = new LinkedList<>();
                s.start();
                for (int j = 0; j < n; j++) {
                    studentList.insert(0, j);
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Adding at front: LinkedList", totalTimes, initialN);
    }

    public static void removeFrontArrayList(Stopwatch s, int initialN, int numTests) {
        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            for (int i = 0; i < numTests; i++) {
                ArrayList<String> javaList = new ArrayList<>();
                for (int j = 0; j < n; j++) {
                    javaList.add(j + "");
                }
                s.start();
                while (!javaList.isEmpty()) {
                    javaList.remove(0);
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Removing from front: ArrayList", totalTimes, initialN);
    }

    public static void removeFrontLinkedList(Stopwatch s, int initialN, int numTests) {
        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            for (int i = 0; i < numTests; i++) {
                LinkedList<String> studentList = new LinkedList<>();
                for (int j = 0; j < n; j++) {
                    studentList.add(j + "");
                }
                s.start();
                while (studentList.size() != 0) {
                    studentList.removeFirst();
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("removing from front: LinkedList", totalTimes, initialN);
    }

    public static void getRandomArrayList(Stopwatch s, int initialN, int numTests) {
        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            int total = 0;
            Random r = new Random();
            for (int i = 0; i < numTests; i++) {
                ArrayList<Integer> javaList = new ArrayList<>();
                for (int j = 0; j < n; j++) {
                    javaList.add(j);
                }
                s.start();
                for (int j = 0; j < n; j++) {
                    total += javaList.get(r.nextInt(javaList.size()));
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Getting random: ArrayList", totalTimes, initialN);
    }

    public static void getRandomLinkedList(Stopwatch s, int initialN, int numTests) {
        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            int total = 0;
            Random r = new Random();
            for (int i = 0; i < numTests; i++) {
                LinkedList<Integer> studentList = new LinkedList<>();
                for (int j = 0; j < n; j++) {
                    studentList.add(j);
                }
                s.start();
                for (int j = 0; j < n; j++) {
                    total += studentList.get(r.nextInt(studentList.size()));
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Getting random: LinkedList", totalTimes, initialN);
    }

    public static void getAllArrayListUsingIterator(Stopwatch s, int initialN, int numTests) {

        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            int total = 0;
            for (int i = 0; i < numTests; i++) {
                ArrayList<Integer> javaList = new ArrayList<>();
                for (int j = 0; j < n; j++) {
                    javaList.add(j);
                }
                Iterator<Integer> it = javaList.iterator();
                s.start();
                while (it.hasNext()) {
                    total += it.next();
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Getting all using iterator: ArrayList", totalTimes, initialN);
    }

    public static void getAllLinkedListUsingIterator(Stopwatch s, int initialN, int numTests) {
        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            int total = 0;
            for (int i = 0; i < numTests; i++) {
                LinkedList<Integer> studentList = new LinkedList<>();
                for (int j = 0; j < n; j++) {
                    studentList.add(j);
                }
                Iterator<Integer> it = studentList.iterator();
                s.start();
                while (it.hasNext()) {
                    total += it.next();
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Getting all using iterator: LinkedList", totalTimes, initialN);
    }

    public static void getAllArrayListUsingGetMethod(Stopwatch s, int initialN, int numTests) {
        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            for (int i = 0; i < numTests; i++) {
                ArrayList<Integer> javaList = new ArrayList<>();
                for (int j = 0; j < n; j++) {
                    javaList.add(j);
                }
                s.start();
                int x = 0;
                for (int j = 0; j < javaList.size(); j++) {
                    x += javaList.get(j);
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Getting all using get method: ArrayList", totalTimes, initialN);
    }

    public static void getAllLinkedListUsingGetMethod(Stopwatch s, int initialN, int numTests) {
        double[] totalTimes = new double[numTests];
        for (int t = 0; t < NUM_REPEATS_OF_TEST; t++) {
            int n = initialN;
            for (int i = 0; i < numTests; i++) {
                LinkedList<Integer> studentList = new LinkedList<>();
                for (int j = 0; j < n; j++) {
                    studentList.add(j);
                }
                s.start();
                int x = 0;
                for (int j = 0; j < studentList.size(); j++) {
                    x += studentList.get(j);
                }
                s.stop();
                totalTimes[i] += s.time();
                n *= 2;
            }
        }
        showResults("Getting all using get method: LinkedList", totalTimes, initialN);
    }

    // for future changes
    private static interface ArrayListOp {
        public <E> void op(ArrayList<E> list, E data);
    }

    private ArrayListOp addAL = new ArrayListOp() {
        public <E> void op(ArrayList<E> list, E data) {
            list.add(data);
        }
    };

    private ArrayListOp addFrontAL = new ArrayListOp() {
        public <E> void op(ArrayList<E> list, E data) {
            list.add(0, data);
        }
    };

    private ArrayListOp removeFrontAL = new ArrayListOp() {
        public <E> void op(ArrayList<E> list, E data) {
            list.remove(0);
        }
    };

    private static interface LinkedListOp<E> {
        public void op(LinkedList<E> list);
    }
}
