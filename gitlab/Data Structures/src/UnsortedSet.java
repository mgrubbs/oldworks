
/*  Student information for assignment: 
 *
 *  On <MY|OUR> honor, Mark Grubbs and REDACTED, this programming assignment is <MY|OUR> own work
 *  and <I|WE> have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose turnin account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *  Section number:
 *  
 *  Student 2 
 *  UTEID:
 *  email address:
 *  Grader name:
 *  Section number:
 *  
 */

import java.util.Iterator;
import java.util.ArrayList;

/**
 * A simple implementation of an ISet. Elements are not in any particular order.
 * Students are to implement methods that were not implemented in AbstractSet
 * and override methods that can be done more efficiently. An ArrayList must be
 * used as the internal storage container.
 *
 */
public class UnsortedSet<E> extends AbstractSet<E> {
    private ArrayList<E> myCon;

    // makes a new UnsortedSet
    public UnsortedSet() {
        myCon = new ArrayList<>();
    }

    /**
     * Add an item to this set. <br>
     * item != null
     * 
     * @param item the item to be added to this set. item may not equal null.
     * @return true if this set changed as a result of this operation, false
     *         otherwise.
     * this is O(N)
     */
    public boolean add(E item) {
        if (item == null) {
            throw new IllegalArgumentException("Member to be added cannot be null.");
        }
        int oldSize = myCon.size();
        if (!contains(item)) {
            myCon.add(item);
        }
        return oldSize != myCon.size();
    }

    /**
     * Return the number of elements of this set.
     * pre: none
     * @return the number of items in this set
     * this is O(1)
     */
    public int size() {
        return myCon.size();
    }

    /**
     * A union operation. Add all items of otherSet that are not already present in
     * this set to this set.
     * 
     * @param otherSet != null
     * @return true if this set changed as a result of this operation, false
     *         otherwise.
     * this is O(N^2)
     */
    public boolean addAll(ISet<E> otherSet) {
        otherSetPrecon(otherSet);
        int oldSize = myCon.size();
        for (E item : otherSet) {
            add(item);
        }
        return oldSize != myCon.size();
    }

    // checks if other set is null
    private void otherSetPrecon(ISet<E> otherSet) {
        if (otherSet == null) {
            throw new IllegalArgumentException("otherSet given cannot be null.");
        }
    }

    /**
     * Return an Iterator object for the elements of this set.
     * pre: none
     * @return an Iterator object for the elements of this set
     * this is O(1)
     */
    public Iterator<E> iterator() {
        return myCon.iterator();
    }

    /**
     * Create a new set that is the union of this set and otherSet.
     * <br>pre: otherSet != null
     * <br>post: returns a set that is the union of this set and otherSet.
     * Neither this set or otherSet are altered as a result of this operation.
     * <br> pre: otherSet != null
     * @param otherSet != null
     * @return a set that is the union of this set and otherSet
     * this is O(N^2)
     */
    public ISet<E> union(ISet<E> otherSet) {
        otherSetPrecon(otherSet);
        UnsortedSet<E> unionSet = new UnsortedSet<E>();
        unionSet.addAll(this);
        unionSet.addAll(otherSet);
        return unionSet;
    }

    /**
     * Create a new set that is the difference of this set and otherSet. Return an ISet of 
     * elements that are in this Set but not in otherSet. Also called
     * the relative complement. 
     * <br>Example: If ISet A contains [X, Y, Z] and ISet B contains [W, Z] then
     * A.difference(B) would return an ISet with elements [X, Y] while
     * B.difference(A) would return an ISet with elements [W]. 
     * <br>pre: otherSet != null
     * <br>post: returns a set that is the difference of this set and otherSet.
     * Neither this set or otherSet are altered as a result of this operation.
     * <br> pre: otherSet != null
     * @param otherSet != null
     * @return a set that is the difference of this set and otherSet
     * this is O(N^2)
     */
    public ISet<E> difference(ISet<E> otherSet) {
        otherSetPrecon(otherSet);
        UnsortedSet<E> differenceSet = new UnsortedSet<E>();
        differenceSet.addAll(this);
        for(E item : otherSet) {
            if(differenceSet.contains(item)) {
                differenceSet.remove(item);
            }
        }
        return differenceSet;
    }

}
