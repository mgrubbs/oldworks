
/*
 * Student information for assignment: On my honor, Mark Grubbs, this programming
 * assignment is my own work and I have not provided this code to any other
 * student. 
 * UTEID: msg2772
 * email address: siegbalicula@gmail.com
 * Grader name: 
 * Number of slip days I am using:
 */

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<E> implements IList<E> {
    
    // HEADER makes the addition of nodes to front and back easier
    // also making the list circular is easier
    private final DoubleListNode<E> HEADER = new DoubleListNode<E>();
    // CS314 students. Add you instance variables here.
    // size is for checking if particular parameters are wrong
    private int size;
    // front is for making a loop go through the list with this as the first node
    private DoubleListNode<E> front;
    // end helps with going backwards through the list
    private DoubleListNode<E> end;
    // You decide what instance variables to use.
    // Must adhere to assignment requirements. No ArrayLists or Java
    // LinkedLists.

    // CS314 students, add constructors here:
    // Constructor sets HEADER to be connected to itself
    // HEADER becomes front and end
    public LinkedList() {
        HEADER.setNext(HEADER);
        HEADER.setPrev(HEADER);
        end = HEADER;
        front = HEADER;
    }
    // CS314 students, add methods here:

    /**
     * add item to the front of the list. <br>
     * pre: item != null <br>
     * post: size() = old size() + 1, get(0) = item
     * 
     * @param item the data to add to the front of this list
     * this method is order O(1)
     */
    public void addFirst(E item) {
        doNullCheck(item);
        size++;
        DoubleListNode<E> newFront = new DoubleListNode<E>(HEADER, item, front);
        front.setPrev(newFront);
        front = newFront;
        HEADER.setNext(front);
        if(front.getNext() == HEADER) {
            end = newFront;
            HEADER.setPrev(end);
        }
    }

    // this method is order O(1)
    // this method checks the precondition that an added object is not null
    private void doNullCheck(E item) {
        if(item == null) {
            throw new IllegalArgumentException("Node data cannot be null.");
        }
    }
    /**
     * add item to the end of the list. <br>
     * pre: item != null <br>
     * post: size() = old size() + 1, get(size() -1) = item
     * 
     * @param item the data to add to the end of this list
     * this method is order O(1)
     */
    public void addLast(E item) {
        doNullCheck(item);
        size++;
        DoubleListNode<E> newBack = new DoubleListNode<>(end, item, HEADER);
        end.setNext(newBack);
        end = newBack;
        HEADER.setPrev(end);
        if(end.getPrev() == HEADER) {
            front = newBack;
            HEADER.setNext(newBack);
        }
    }

    /**
     * remove and return the first element of this list. <br>
     * pre: size() > 0 <br>
     * post: size() = old size() - 1
     * 
     * @return the old first element of this list
     * this method is order O(1)
     */
    public E removeFirst() {
        doSizeCheck();
        size--;
        E removed = HEADER.getNext().getData();
        front = HEADER.getNext().getNext();
        HEADER.setNext(front);
        front.setPrev(HEADER);
        return removed;
    }

    // this method is O(1)
    // checks if the list has elements and throws exception if not
    private void doSizeCheck() {
        if(size <= 0) {
            throw new IllegalStateException("The list is empty. Cannot remove from empty list.");
        }
    }
    /**
     * remove and return the last element of this list. <br>
     * pre: size() > 0 <br>
     * post: size() = old size() - 1
     * 
     * @return the old last element of this list
     * this method is O(1)
     */
    public E removeLast() {
        doSizeCheck();
        size--;
        E removed = HEADER.getPrev().getData();
        end = HEADER.getPrev().getPrev();
        HEADER.setPrev(end);
        end.setNext(HEADER);
        return removed;
    }

    // this method is O(1)
    // this is just the addLast method
    public void add(E item) {
        addLast(item);
    }

    // this method is O(N), but the T(N) is different
    // this method inserts an item E at position pos by finding the node at pos
    // and changing the node's previous node to a new node with the E item
    // the node that was previously at index pos - 1 has their next node changed
    // to the new node
    // also size incremented by 1
    // special case: if the position is the size, the method is just addLast
    // if the position is 0, the method is just addFirst
    public void insert(int pos, E item) {
        doNullCheck(item);
        if(pos < 0 || pos > size) {
            throw new IllegalArgumentException("pos given must be within the list's bounds.");
        }
        if(pos == size) {
            addLast(item);
        } else if(pos == 0) {
            addFirst(item);
        } else {
            DoubleListNode<E> nodeBeforePos = getNodeAtPosition(pos).getPrev();
            DoubleListNode<E> newNode = new DoubleListNode<>(nodeBeforePos, item, nodeBeforePos.getNext());
            nodeBeforePos.getNext().setPrev(newNode);
            nodeBeforePos.setNext(newNode);
            size++;
        }
    }

    // this method is O(N)
    // gets the node at the index pos
    // goes from back and front to get the node at index pos by having two different
    // indices check if the node at the back or the front is the node wanted
    private DoubleListNode<E> getNodeAtPosition(int pos) {
        final int LIMIT = size / 2;
        int index = 0;
        int backIndex = size - 1;
        DoubleListNode<E> frontNode = front;
        DoubleListNode<E> backNode = end;
        while(index <= LIMIT && backIndex >= LIMIT) {
            if(pos == index) {
                return frontNode;
            }
            if(pos == backIndex) {
                return backNode;
            }
            index++;
            backIndex--;
            frontNode = frontNode.getNext();
            backNode = backNode.getPrev();
        }
        return null;
    }

    // this method is O(1)
    // this method checks if the position is inside of the boundaries of the list
    private void doPositionCheck(int pos) {
        if(pos < 0 || pos >= size) {
            throw new IllegalArgumentException("pos given must be within the list's bounds");
        }
    }

    // this method is O(N)
    // this method uses getNodeAtPosition(pos) to get the node at index pos and sets that
    // node's data to E item; it stores nodeAtPos's old data temporarily and returns it
    public E set(int pos, E item) {
        doNullCheck(item);
        doPositionCheck(pos);
        DoubleListNode<E> nodeAtPos = getNodeAtPosition(pos);
        E tempData = nodeAtPos.getData();
        nodeAtPos.setData(item);
        return tempData;
    }

    // this method is O(N)
    // this method IS getNodeAtPosition(pos) with the only difference being that it returns the data
    // and not just the node
    public E get(int pos) {
        doPositionCheck(pos);
        return getNodeAtPosition(pos).getData();
    }

    // this method is O(N)
    // this method removes the node at position and decrements size
    // removal is done by setting the nodes around it to link to each other
    public E remove(int pos) {
        doPositionCheck(pos);
        if(pos == 0) {
            return removeFirst();
        } 
        if(pos == size() - 1) {
            return removeLast();
        }
        DoubleListNode<E> nodeAtPos = getNodeAtPosition(pos);
        size--;
        E removedItem = nodeAtPos.getData();
        nodeAtPos.getPrev().setNext(nodeAtPos.getNext());
        nodeAtPos.getNext().setPrev(nodeAtPos.getPrev());
        return removedItem;
    }

    // this method is O(N)
    // this method finds the first instance of E obj in the data of the nodes and creates a NodeIndexPair
    // for access to the node at that position
    // that node is removed and this returns true in that case
    // otherwise returns false to show nothing changed
    public boolean remove(E obj) {
        doNullCheck(obj);
        NodeIndexPair nodeIndex = getIndexOfNode(obj, 0);
        if(nodeIndex.index != -1) {
            nodeIndex.node.getPrev().setNext(nodeIndex.node.getNext());
            nodeIndex.node.getNext().setPrev(nodeIndex.node.getPrev());
            return true;
        }
        return false;
    }

    // This method is O(N)
    // gets sublist by making a new IList<E> and getting the node at position start
    // then it runs through the nodes adding nodes from start inclusive to stop exclusive to the new list
    public IList<E> getSubList(int start, int stop) {
        if(start < 0 || start > size() || stop < start || stop > size()) {
            throw new IllegalArgumentException("Index to start or stop at are not within the list boundaries or start is more than stop.");
        }
        IList<E> subList = new LinkedList<>();
        DoubleListNode<E> startingNode = getNodeAtPosition(start);
        for(int currentNode = start; currentNode < stop; currentNode++) {
            subList.add(startingNode.getData());
            startingNode = startingNode.getNext();
        }
        return subList;
    }

    // this method is O(1)
    // gets size of list
    public int size() {
        return size;
    }

    // this method is O(N)
    // uses getIndexOfNode to get the index of first occurrence of item
    public int indexOf(E item) {
        doNullCheck(item);
        return getIndexOfNode(item, 0).index;
    }

    // this method is O(N)
    // creates a NodeIndexPair to get the index and node of item requested
    // by getting the index and the node, remove(E obj) is O(N) because then 
    // the node can be referenced and removed
    // runs through the first elements in the list up to starting index and then
    // finds the first node with data equal to item
    private NodeIndexPair getIndexOfNode(E item, int startingIndex) {
        if(startingIndex > size) {
            return new NodeIndexPair(-1, null);
        }
        DoubleListNode<E> nodeAtCurrentIndex = front;
        int index = startingIndex;
        for(int i = 0; i < startingIndex; i++) {
            nodeAtCurrentIndex = nodeAtCurrentIndex.getNext();
        }
        while(nodeAtCurrentIndex != HEADER) {
            if(nodeAtCurrentIndex.getData().equals(item)) {
                return new NodeIndexPair(index, nodeAtCurrentIndex);
            }
            nodeAtCurrentIndex = nodeAtCurrentIndex.getNext();
            index++;
        }
        return new NodeIndexPair(-1, null);
    }

    // this method is O(N)
    // similar to the other indexOf method, this method gets first occurrence of
    // item after the index pos given
    public int indexOf(E item, int pos) {
        doNullCheck(item);
        return getIndexOfNode(item, pos).index;
    }

    // this method is O(1)
    // "deletes" the items in the list by setting the front and the end to the HEADER
    // and setting the HEADER to circularly referencing itself in sequence
    // also sets size to zero
    public void makeEmpty() {
        HEADER.setNext(HEADER);
        HEADER.setPrev(HEADER);
        front = end = HEADER;
        size = 0;
    }

    // gets a new iterator
    // this method is O(1)
    public Iterator<E> iterator() {
        return new LLIterator();
    }

    // this method is O(N)
    // this method deletes the elements between start inclusive and stop exclusive
    // by finding the nodes at indices start - 1 and stop and setting their sequence to make
    // them next to each other in sequence
    // changes size by stop - start
    public void removeRange(int start, int stop) {
        doPositionCheck(start);
        if(start > stop || stop > size) {
            throw new IllegalArgumentException("Start cannot be greater than stop.");
        }
        DoubleListNode<E> firstNode = start == 0 ? HEADER : getNodeAtPosition(start - 1);
        DoubleListNode<E> lastNode = stop == size ? HEADER : getNodeAtPosition(stop);
        firstNode.setNext(lastNode);
        lastNode.setPrev(firstNode);
        size = size - (stop - start);
    }
    
    // this method is O(N)
    // this method returns a string of the list in the format: 
    // [ E, E, ... , E ]
    // by going through the nodes until it reaches the HEADER
    public String toString() {
        if(size == 0) {
            return "[]";
        }
        StringBuilder theList = new StringBuilder();
        theList.append("[");
        theList.append(front.getData());
        DoubleListNode<E> currentNode = front.getNext();
        while(currentNode != HEADER) {
            theList.append(", ");
            theList.append(currentNode.getData());
            currentNode = currentNode.getNext();
        }
        theList.append("]");
        return theList.toString();
    }
    
    // This method is O(N)
    // returns true if the elements in each list is the same in the same order and false otherwise
    // if their sizes differ, returns false
    // if they're empty, returns true no matter what
    public boolean equals(LinkedList<E> otherList) {
        if(size != otherList.size()) {
            return false;
        }
        DoubleListNode<E> thisNode = front;
        DoubleListNode<E> otherNode = otherList.front;
        while(thisNode != HEADER) {
            if(!(thisNode.getData().equals(otherNode.getData()))) {
                return false;
            }
            thisNode = thisNode.getNext();
            otherNode = otherNode.getNext();
        }
        return true;
    }
    
    // iterator class to make the class iterable with a for loop
    private class LLIterator implements Iterator<E>{
        
        private int position;
        private DoubleListNode<E> currentNode;
        private boolean canRemove;
        
        // makes currentNode the front
        public LLIterator() {
            currentNode = front;
        }
        
        // checks if position is less than size to show hasNext() is true
        public boolean hasNext() {
            return position < size;
        }

        // pre: hasNext()
        // returns currentNode's data
        // sets canRemove to true and moves currentNode over by one
        // increments position
        public E next() {
            if(!hasNext()) {
                throw new NoSuchElementException("There are no more elements in the iterator.");
            }
            canRemove = true;
            E currentNodeData = currentNode.getData();
            currentNode = currentNode.getNext();
            position++;
            return currentNodeData;
        }
        
        // removes currentNode
        public void remove() {
            if(!canRemove) {
                throw new IllegalStateException("Called remove() without calling next().");
            }
            DoubleListNode<E> prev = currentNode.getPrev();
            DoubleListNode<E> next = currentNode.getNext();
            prev.setNext(currentNode.getNext());
            next.setPrev(currentNode.getPrev());
            currentNode = prev;
            canRemove = false;
        }
    }
    
    // class to store Node and Index together
    private class NodeIndexPair {
        private int index;
        private DoubleListNode<E> node;
        
        public NodeIndexPair(int i, DoubleListNode<E> n) {
            index = i;
            node = n;
        }
    }
}
