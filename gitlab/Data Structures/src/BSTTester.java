
/*  Student information for assignment:
 *
 *  On <MY|OUR> honor, <Mark Grubbs> and <NAME2), this programming assignment is <MY|OUR> own work
 *  and <I|WE> have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose Canvas account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *
 *  Student 2
 *  UTEID:
 *  email address:
 *
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

/**
 * Experiment data: 
 * Experiment Random Insertion:
 * BINARY SEARCH TREE
 * 1000 -
 * time to add elements: 3.947E-4 seconds
 * height = 20
 * size = 1000
 * average of ten add elements: 7.1376E-4 seconds
 * average of ten heights = 21.4
 * average of ten sizes = 1000
 * 2000 - 
 * time to add elements: 4.449E-4
 * height = 26
 * size = 2000
 * average of ten add elements: 4.5795E-4
 * average of ten heights = 24.5
 * average of ten sizes = 2000
 * 4000 -
 * time to add elements: 9.952E-4
 * height = 26
 * size = 4000
 * average of ten add elements: 9.9084E-4
 * average of ten heights = 25.4
 * average of ten sizes = 4000
 * 8000 - 
 * time to add elements: 0.0021075
 * height = 30
 * size = 8000
 * average of ten add elements: 0.00196225
 * average of ten heights = 29.8
 * average of ten sizes = 8000
 * 16000 - 
 * time to add elements: 0.0045786
 * height = 31
 * size = 16000
 * average of ten add elements: 0.00428837
 * average of ten heights = 30.6
 * average of ten sizes = 16000
 * 32000 -
 * time to add elements: 0.0110325
 * height = 34
 * size = 32000
 * average of ten add elements: 0.01035471
 * average of ten heights = 34.4
 * average of ten sizes = 32000
 * 64000 -
 * time to add elements: 0.0236133
 * height = 40
 * size = 64000
 * average of ten add elements: 0.02444911
 * average of ten heights = 37.3
 * average of ten sizes = 63999.7
 * 128000 -
 * time to add elements: 0.057965
 * height = 40
 * size = 128000
 * average of ten add elements: 0.05477956
 * average of ten heights = 40.7
 * average of ten sizes = 127999
 * 256000 -
 * time to add elements: 0.1374188
 * height = 44
 * size = 255991
 * average of ten add elements: 0.13069431
 * average of ten heights = 43.9
 * average of ten sizes = 255993.4
 * 512000 -
 * time to add elements: 0.3349534
 * height = 44
 * size = 511974
 * average of ten add elements: 0.33195438
 * average of ten heights = 47.0
 * average of ten sizes = 511969.6
 * 1024000 -
 * time to add elements: 0.8620155
 * height = 49
 * size = 1023894
 * average of ten add elements: 0.78685142
 * average of ten heights = 49.4
 * average of ten sizes = 1023884.7
 * The minimum possible height of the tree is logN.
 * 
 * JAVA TREE SET
 * 1000 -
 * time to add elements: 9.873E-4 seconds
 * size = 1000
 * average of ten add elements: 8.8968E-4 seconds
 * average of ten sizes = 1000
 * 2000 - 
 * time to add elements: 4.449E-4
 * size = 2000
 * average of ten add elements: 9.2513E-4
 * average of ten sizes = 2000
 * 4000 -
 * time to add elements: 0.0012622
 * size = 4000
 * average of ten add elements: 0.00113239
 * average of ten sizes = 4000
 * 8000 - 
 * time to add elements: 0.002885
 * size = 8000
 * average of ten add elements: 0.00220452
 * average of ten sizes = 8000
 * 16000 - 
 * time to add elements: 0.0050464
 * size = 16000
 * average of ten add elements: 0.00460619
 * average of ten sizes = 16000
 * 32000 -
 * time to add elements: 0.0116199
 * size = 32000
 * average of ten add elements: 0.01123878
 * average of ten sizes = 31999.9
 * 64000 -
 * time to add elements: 0.0291786
 * size = 63999
 * average of ten add elements: 0.02575024
 * average of ten sizes = 63998.9
 * 128000 -
 * time to add elements: 0.0552875
 * size = 127997
 * average of ten add elements: 0.05443576
 * average of ten sizes = 127998.2
 * 256000 -
 * time to add elements: 0.1361372
 * size = 255987
 * average of ten add elements: 0.13642467
 * average of ten sizes = 255993.0
 * 512000 -
 * time to add elements: 0.3529739
 * size = 511971
 * average of ten add elements: 0.35970464
 * average of ten sizes = 511972.0
 * 1024000 -
 * time to add elements: 0.8584355
 * size = 1023878
 * average of ten add elements: 0.84719573
 * average of ten sizes = 1023876.0
 * Compared to the BinarySearchTree class, the TreeSet has about the same times.
 * 
 * Experiment - Sorted Order Insertion
 * BINARY SEARCH TREE
 * 1000 -
 * time to add elements: 0.0059757 seconds
 * height = 999
 * size = 1000
 * average of ten add elements: 0.0031369 seconds
 * average of ten heights = 999.0
 * average of ten sizes = 1000
 * 2000 - 
 * time to add elements: 0.0067301
 * height = 1999
 * size = 2000
 * average of ten add elements: 0.00682044
 * average of ten heights = 1999
 * average of ten sizes = 2000
 * 4000 -
 * time to add elements: 0.0270925
 * height = 3999
 * size = 4000
 * average of ten add elements: 0.02648784
 * average of ten heights = 3999.0
 * average of ten sizes = 4000
 * 8000 - 
 * time to add elements: 0.1071205
 * height = 7999
 * size = 8000
 * average of ten add elements: 0.11494717
 * average of ten heights = 7999
 * average of ten sizes = 8000
 * 16000 - 
 * time to add elements: 0.4993103
 * height = 15999
 * size = 16000
 * average of ten add elements: 0.50031872
 * average of ten heights = 15999.0
 * average of ten sizes = 16000
 * 32000 -
 * time to add elements: 2.0207467
 * height = 31999
 * size = 32000
 * average of ten add elements: 2.01968398
 * average of ten heights = 31999.0
 * average of ten sizes = 32000
 * 64000 -
 * time to add elements: 8.0342661
 * height = 63999
 * size = 64000
 * average of ten add elements: 8.04066926
 * average of ten heights = 63999.0
 * average of ten sizes = 64000.0
 * For 128,000, 256,000, and 512,000 sorted integers, it would likely take 4 times the last amount of integers' time.
 * e.g. 128,000 would take 4 times the time of 64,000 insertions, etc.
 * 
 * JAVA TREE SET
 * 1000 -
 * time to add elements: 9.307E-4 seconds
 * average of ten add elements: 8.9436E-4 seconds
 * 2000 - 
 * time to add elements: 0.0010152
 * average of ten add elements: 6.1678E-4
 * 4000 -
 * time to add elements: 6.573E-4
 * average of ten add elements: 6.5879E-4
 * 8000 - 
 * time to add elements: 0.0012972
 * average of ten add elements: 0.00113631
 * 16000 - 
 * time to add elements: 0.0023625
 * average of ten add elements: 0.002515
 * 32000 -
 * time to add elements: 0.0064741
 * average of ten add elements: 0.00577635
 * 64000 -
 * time to add elements: 0.0131731
 * average of ten add elements: 0.01261633
 * The times for adding is significantly faster than my BinarySearchTree class. This is likely due to the naive insertion method
 * my class uses while the TreeSet uses a more efficient insertion method that reduces its tree's height.
 */

/**
 * Some test cases for CS314 Binary Search Tree assignment.
 *
 */
public class BSTTester {

    /**
     * The main method runs the tests.
     * 
     * @param args Not used
     */
    public static void main(String[] args) {
        studentTests();
        //experiment();
    }

    // tests
    private static void studentTests() {
        BinarySearchTree<String> tree = new BinarySearchTree<>();
        // add and isPresent
        tree.add("Banana");
        tree.add("banana");
        tree.add("squid" + "quid");
        tree.add("A");
        System.out.println("Testing add method and isPresent.");
        System.out.println("banana and Banana should be present.");
        showTestResults(tree.isPresent("banana"), 1);
        showTestResults(tree.isPresent("Banana"), 2);
        // getAll
        System.out.println("Testing getAll method.");
        List<String> expected = new ArrayList<>();
        expected.add("A");
        expected.add("Banana");
        expected.add("banana");
        expected.add("squidquid");
        showTestResults(expected.equals(tree.getAll()), 3);
        tree.add("ahab");
        expected.add(2, "ahab");
        showTestResults(expected.equals(tree.getAll()), 4);
        // remove
        System.out.println("Testing remove.");
        System.out.println("removing banana and Banana");
        showTestResults(tree.remove("banana"), 5);
        showTestResults(tree.remove("Banana"), 6);
        System.out.println("removing aha should return false");
        showTestResults(!tree.remove("aha"), 7);
        // getAllGreaterThan
        System.out.println("Testing getAllGreaterThan.");
        System.out.println("everything should be bigger than \" \"");
        expected = new ArrayList<>();
        expected.add("A");
        expected.add("ahab");
        expected.add("squidquid");
        showTestResults(expected.equals(tree.getAllGreaterThan(" ")), 8);
        System.out.println("nothing should be greater than s other than squidquid");
        showTestResults("squidquid".equals(tree.getAllGreaterThan("s").get(0)), 9);
        // getAllLessThan
        tree.add("Barotene");
        System.out.println("added barotene for getAllLessThan test");
        System.out.println("getAllLessThan(\"aha\") should return \"A\" and \"Barotene\" in list");
        expected.clear();
        expected.add("A");
        expected.add("Barotene");
        showTestResults(expected.equals(tree.getAllLessThan("aha")), 10);
        System.out.println("nothing should be smaller than \" \"");
        showTestResults(tree.getAllLessThan(" ").isEmpty(), 11);
        // height
        System.out.println("Testing height");
        System.out.println("current height should be 2");
        showTestResults(tree.height() == 2, 12);
        tree.remove("A");
        tree.remove("ahab");
        System.out.println("removing two elements should result in tree being height 1");
        showTestResults(tree.height() == 1, 13);
        // iterativeAdd and more isPresent
        tree = new BinarySearchTree<>();
        tree.iterativeAdd("sevene");
        tree.iterativeAdd("SSSSS");
        tree.iterativeAdd("ZZZZ");
        tree.iterativeAdd("zeta");
        tree.iterativeAdd("theta");
        expected.clear();
        expected.add("sevene");
        expected.add("SSSSS");
        expected.add("ZZZZ");
        expected.add("zeta");
        expected.add("theta");
        System.out.println("Testing iterativeAdd and isPresent");
        System.out.println("added : " + expected.toString() + " in that order.");
        System.out.println("theta should be present");
        showTestResults(tree.isPresent("theta"), 14);
        System.out.println("Should not be able to add \"zeta\"");
        showTestResults(!tree.iterativeAdd("zeta") && !tree.add("zeta"), 15);
        // max, min, and size
        System.out.println("Testing max, min and size");
        System.out.println("current max should be zeta");
        showTestResults(tree.max().equals("zeta"), 16);
        System.out.println("current min should be SSSSS");
        showTestResults(tree.min().equals("SSSSS"), 17);
        System.out.println("current size should be 5");
        showTestResults(tree.size() == 5, 18);
        System.out.println("removing zeta and SSSSS");
        tree.remove("zeta");
        tree.remove("SSSSS");
        System.out.println("new max should be theta; new min should be ZZZZ; new size should be 3");
        showTestResults(tree.max().equals("theta"), 19);
        showTestResults(tree.min().equals("ZZZZ"), 20);
        showTestResults(tree.size() == 3, 21);
        // numNodesAtDepth
        tree.add("A");
        tree.add("a");
        tree.add("t");
        tree.add("usoza");
        System.out.println("added four nodes at the same level for numNodesAtDepth test");
        System.out.println("nodes at depth 1 should be 2 and depth 2 should have 4");
        showTestResults(tree.numNodesAtDepth(1) == 2, 22);
        showTestResults(tree.numNodesAtDepth(2) == 4, 23);
        // get
        System.out.println("Testing get");
        System.out.println("4th element should be sevene and 2nd element should be ZZZZ");
        showTestResults(tree.get(3).equals("sevene"), 24);
        showTestResults(tree.get(1).equals("ZZZZ"), 25);
    }

    // bare bones experiment code
    private static void experiment() {
        for (int i = 0; i <= 10; i++) {
            // int heights = 0;
            int sizes = 0;
            double totalTime = 0.0;
            for (int m = 0; m < 10; m++) {
                Stopwatch s = new Stopwatch();
                s.start();
                int n = (int) (1000 * Math.pow(2, i));
                TreeSet<Integer> b = new TreeSet<>();
                for (int j = 0; j < n; j++) {
                    b.add(new Integer(j));
                }
                s.stop();
                System.out.println(s.time() + " " + b.size());
                totalTime = totalTime + s.time();
                //heights += b.height();
                sizes += b.size();
            }
            System.out.println(totalTime / 10 + " " + sizes / 10.0);
        }

    }

    // provided by the professor; I used this in my test cases.
    private static void showTestResults(boolean passed, int testNum) {
        if (passed) {
            System.out.println("Test " + testNum + " passed.");
        } else {
            System.out.println("TEST " + testNum + " FAILED.");
        }
    }
}