
/*  Student information for assignment:
 *
 *  On <MY|OUR> honor, <Mark Grubbs> and <NAME2), this programming assignment is <MY|OUR> own work
 *  and <I|WE> have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose Canvas account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *
 *  Student 2
 *  UTEID:
 *  email address:
 *
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Shell for a binary search tree class.
 * 
 * @author scottm
 * @param <E> The data type of the elements of this BinarySearchTree. Must
 *            implement Comparable or inherit from a class that implements
 *            Comparable.
 *
 */
public class BinarySearchTree<E extends Comparable<? super E>> {

    private BSTNode<E> root;
    // CS314 students. Add any other instance variables you want here
    private int size;

    // CS314 students. Add a default constructor here if you feel it is necessary.
    public BinarySearchTree() {
        root = null;
        size = 0;
    }

    /**
     * Add the specified item to this Binary Search Tree if it is not already
     * present. <br>
     * pre: <tt>value</tt> != null<br>
     * post: Add value to this tree if not already present. Return true if this tree
     * changed as a result of this method call, false otherwise.
     * 
     * @param value the value to add to the tree
     * @return false if an item equivalent to value is already present in the tree,
     *         return true if value is added to the tree and size() = old size() + 1
     */
    public boolean add(E value) {
        if (value == null) {
            throw new IllegalArgumentException("value to be added to the tree cannot be null");
        }
        int oldSize = size;
        root = addHelp(root, value);
        return oldSize != size;
    }

    // implemented from the notes in class
    private BSTNode<E> addHelp(BSTNode<E> n, E value) {
        if (n == null) { // this is where to insert
            size++;
            return new BSTNode<>(value);
        } else {
            int direction = value.compareTo(n.data);
            if (direction < 0) { // go left; value smaller
                n.left = addHelp(n.left, value);
            } else if (direction > 0) { // go right; value bigger
                n.right = addHelp(n.right, value);
            } // else do nothing; value is already present
            return n;
        }
    }

    /**
     * Remove a specified item from this Binary Search Tree if it is present. <br>
     * pre: <tt>value</tt> != null<br>
     * post: Remove value from the tree if present, return true if this tree changed
     * as a result of this method call, false otherwise.
     * 
     * @param value the value to remove from the tree if present
     * @return false if value was not present returns true if value was present and
     *         size() = old size() - 1
     */
    public boolean remove(E value) {
        if (value == null) {
            throw new IllegalArgumentException("cannot remove null values");
        }
        int oldSize = size;
        root = removeHelp(root, value);
        return oldSize != size;
    }

    // from the slides given in class
    private BSTNode<E> removeHelp(BSTNode<E> node, E value) {
        if (node == null) { // if the bottom of the tree is reached, stop looking there
            return null;
        }
        int direction = value.compareTo(node.data);
        if (direction < 0) { // go left if smaller
            node.left = removeHelp(node.left, value);
        } else if (direction > 0) { // go right if bigger
            node.right = removeHelp(node.right, value);
        } else {
            size--;
            // node is a leaf
            if (node.left == null && node.right == null) {
                return null;
            }
            // left is placed because right is null
            if (node.right == null) {
                return node.left;
            }
            // right is placed because left is null
            if (node.left == null) {
                return node.right;
            }
            // both left and right have data so choose left subtree's biggest child to
            // replaced
            node.data = max(node.left);
            node.left = removeHelp(node.left, node.data);
            size++;
        }
        // make sure not to destroy entire tree
        return node;
    }

    // gets the max value of the tree starting at a certain node
    // does this by constantly going right until right is null
    private E max(BSTNode<E> start) {
        if (start.right == null) {
            return start.data;
        }
        return max(start.right);
    }

    /**
     * Check to see if the specified element is in this Binary Search Tree. <br>
     * pre: <tt>value</tt> != null<br>
     * post: return true if value is present in tree, false otherwise
     * 
     * @param value the value to look for in the tree
     * @return true if value is present in this tree, false otherwise
     */
    public boolean isPresent(E value) {
        if (value == null) {
            throw new IllegalArgumentException("value to find cannot be null");
        }
        BSTNode<E> node = root;
        // iteratively finds if something is present
        // easier than iterativeAdd because if you reach the value you stop and do not
        // add
        while (node != null) {
            int direction = value.compareTo(node.data);
            if (direction < 0) { // value smaller go left
                node = node.left;
            } else if (direction > 0) { // value bigger go right
                node = node.right;
            } else { // value just right return true
                return true;
            }
        }
        return false;
    }

    /**
     * Return how many elements are in this Binary Search Tree. <br>
     * pre: none<br>
     * post: return the number of items in this tree
     * 
     * @return the number of items in this Binary Search Tree
     */
    public int size() {
        return size;
    }

    /**
     * return the height of this Binary Search Tree. <br>
     * pre: none<br>
     * post: return the height of this tree. If the tree is empty return -1,
     * otherwise return the height of the tree
     * 
     * @return the height of this tree or -1 if the tree is empty
     */
    public int height() {
        return htHelp(root);
    }

    // from the notes over binary search tree
    private int htHelp(BSTNode<E> node) {
        if (node == null) { // helps if the tree is empty; empty tree returns -1
            return -1; // also removes the null count
        }
        return 1 + Math.max(htHelp(node.left), htHelp(node.right));
    }

    /**
     * Return a list of all the elements in this Binary Search Tree. <br>
     * pre: none<br>
     * post: return a List object with all data from the tree in ascending order. If
     * the tree is empty return an empty List
     * 
     * @return a List object with all data from the tree in sorted order if the tree
     *         is empty return an empty List
     */
    public List<E> getAll() {
        List<E> allElements = new ArrayList<>();
        getAllInOrder(allElements, root);
        return allElements;
    }

    // inorder traversal of tree; prints elements in ascending order
    private void getAllInOrder(List<E> allElements, BSTNode<E> node) {
        if (node != null) {
            getAllInOrder(allElements, node.left);
            allElements.add(node.data);
            getAllInOrder(allElements, node.right);
        }
    }

    /**
     * return the maximum value in this binary search tree. <br>
     * pre: <tt>size()</tt> > 0<br>
     * post: return the largest value in this Binary Search Tree
     * 
     * @return the maximum value in this tree
     */
    public E max() {
        if (size == 0) {
            throw new IllegalArgumentException("cannot find max of empty tree");
        }
        return max(root);
    }

    /**
     * return the minimum value in this binary search tree. <br>
     * pre: <tt>size()</tt> > 0<br>
     * post: return the smallest value in this Binary Search Tree
     * 
     * @return the minimum value in this tree
     */
    public E min() {
        if (size == 0) {
            throw new IllegalArgumentException("cannot find min of empty tree");
        }
        BSTNode<E> nodeAt = root;
        while (nodeAt.left != null) {
            nodeAt = nodeAt.left;
        }
        return nodeAt.data;
    }

    /**
     * An add method that implements the add algorithm iteratively instead of
     * recursively. <br>
     * pre: data != null <br>
     * post: if data is not present add it to the tree, otherwise do nothing.
     * 
     * @param data the item to be added to this tree
     * @return true if data was not present before this call to add, false
     *         otherwise.
     */
    public boolean iterativeAdd(E data) {
        if (data == null) {
            throw new IllegalArgumentException("data to be added cannot be null");
        }
        // start at root
        BSTNode<E> placeAt = root;
        while (placeAt != null) {
            int direction = data.compareTo(placeAt.data);
            if (direction < 0) { // go left value smaller
                if (placeAt.left == null) { // if left is empty, can place there
                    placeAt.left = new BSTNode<E>(data);
                    size++;
                    return true;
                }
                placeAt = placeAt.left;
            } else if (direction > 0) { // go right value bigger
                if (placeAt.right == null) { // same as left
                    placeAt.right = new BSTNode<E>(data);
                    size++;
                    return true;
                }
                placeAt = placeAt.right;
            } else { // already contains the value
                return false;
            }
        }
        // if this is reached, the tree was empty
        // create new BSTNode for root
        root = new BSTNode<>(data);
        size++;
        return true;
    }

    /**
     * Return the "kth" element in this Binary Search Tree. If kth = 0 the smallest
     * value (minimum) is returned. If kth = 1 the second smallest value is
     * returned, and so forth. <br>
     * pre: 0 <= kth < size()
     * 
     * @param kth indicates the rank of the element to get
     * @return the kth value in this Binary Search Tree
     */
    public E get(int kth) {
        if (kth < 0 || kth >= size()) {
            throw new IllegalArgumentException("incapable of reaching a value at that rank");
        }
        List<BSTNode<E>> nodes = new ArrayList<>();
        // if the end or the beginning is wanted finds them
        // reduces space usage
        if (kth == 0) {
            return min();
        }
        if (kth == size() - 1) {
            return max();
        }
        return getKth(kth, nodes, root);
    }

    // goes through the tree adding items to a list and if the list size - 1 equals
    // kth,
    // the node wanted is the end
    private E getKth(int kth, List<BSTNode<E>> nodesSeen, BSTNode<E> start) {
        if (start != null) {
            int lastNode = nodesSeen.size() - 1;
            if (lastNode == kth) {
                return nodesSeen.get(lastNode).data;
            } else {
                // go through left
                getKth(kth, nodesSeen, start.left);
                lastNode = nodesSeen.size() - 1;
                // if the size - 1 is kth return node data
                // node was in the left
                if (lastNode == kth) {
                    return nodesSeen.get(lastNode).data;
                }
                // at node passed
                nodesSeen.add(start);
                lastNode = nodesSeen.size() - 1;
                // node wanted is this one
                if (lastNode == kth) {
                    return nodesSeen.get(lastNode).data;
                }
                // go through right
                getKth(kth, nodesSeen, start.right);
                lastNode = nodesSeen.size() - 1;
                // node was in the right
                if (lastNode == kth) {
                    return nodesSeen.get(lastNode).data;
                }
            }
        }
        return null;
    }

    /**
     * Return a List with all values in this Binary Search Tree that are less than
     * the parameter <tt>value</tt>. <tt>value</tt> != null<br>
     * 
     * @param value the cutoff value
     * @return a List with all values in this tree that are less than the parameter
     *         value. If there are no values in this tree less than value return an
     *         empty list. The elements of the list are in ascending order.
     */
    public List<E> getAllLessThan(E value) {
        if (value == null) {
            throw new IllegalArgumentException("value to compare against cannot be null");
        }
        List<E> lessThan = new ArrayList<>();
        BSTNode<E> startAdding = root;
        // changes start to be where there would be values less than value
        while (startAdding != null && value.compareTo(startAdding.data) <= 0) {
            startAdding = startAdding.left;
        }
        getLessThan(lessThan, startAdding, value);
        return lessThan;
    }

    // places all the elements less than the value in order into the list
    // if the value at the right of the node at the start is less than the value,
    // place all of those elements left of that element into the list and so on
    private void getLessThan(List<E> less, BSTNode<E> startNode, E value) {
        if (startNode != null) {
            getAllInOrder(less, startNode.left);
            less.add(startNode.data);
            if (startNode.right != null) {
                // this checks if the right node is smaller than value
                int check = value.compareTo(startNode.right.data);
                if (check > 0) { // yes it was
                    getLessThan(less, startNode.right, value);
                } else if (startNode.right.left != null && check == 0) {
                    // not smaller, but equal so its left subtree is smaller
                    getLessThan(less, startNode.right.left, value);
                } else if (startNode.right.left != null && check < 0) {
                    // bigger, but the left subtree could still have values to insert
                    BSTNode<E> potentiallyLess = startNode.right.left;
                    // jump to smaller value
                    while (potentiallyLess.left != null && value.compareTo(potentiallyLess.data) <= 0) {
                        potentiallyLess = potentiallyLess.left;
                    }
                    if (value.compareTo(potentiallyLess.data) > 0)
                        getLessThan(less, potentiallyLess, value);
                }
            }
        }

    }

    /**
     * Return a List with all values in this Binary Search Tree that are greater
     * than the parameter <tt>value</tt>. <tt>value</tt> != null<br>
     * 
     * @param value the cutoff value
     * @return a List with all values in this tree that are greater than the
     *         parameter value. If there are no values in this tree greater than
     *         value return an empty list. The elements of the list are in ascending
     *         order.
     */
    public List<E> getAllGreaterThan(E value) {
        if (value == null) {
            throw new IllegalArgumentException("value to compare against cannot be null");
        }
        List<E> greaterThan = new ArrayList<E>();
        BSTNode<E> startAdding = root;
        // jump to values that are greater than value
        while (startAdding != null && value.compareTo(startAdding.data) >= 0) {
            startAdding = startAdding.right;
        }
        getGreaterThan(greaterThan, startAdding, value);
        return greaterThan;
    }

    // similar to getLessThan, but in this case the checking of the side that does
    // not fit the value given occurs first to keep values in order
    // places values greater than value into a list of data from the tree
    private void getGreaterThan(List<E> greater, BSTNode<E> startNode, E value) {
        if (startNode != null) {
            if (startNode.left != null) {
                // check if left is bigger
                int check = value.compareTo(startNode.left.data);
                if (check < 0) { // was bigger call that side
                    getGreaterThan(greater, startNode.left, value);
                } else if (startNode.left.right != null && check == 0) {
                    // was not bigger, but value was equal so call right subtree
                    getGreaterThan(greater, startNode.left.right, value);
                } else if (startNode.left.right != null && check < 0) {
                    // value was not bigger, but right subtree potentially bigger
                    BSTNode<E> potentiallyGreater = startNode.left.right;
                    // jump to bigger value
                    while (potentiallyGreater.right != null && value.compareTo(potentiallyGreater.data) >= 0) {
                        potentiallyGreater = potentiallyGreater.right;
                    }
                    if (value.compareTo(potentiallyGreater.data) < 0)
                        getGreaterThan(greater, potentiallyGreater, value);
                }
            }
            // add parent first
            greater.add(startNode.data);
            // add right subtree
            getAllInOrder(greater, startNode.right);
        }
    }

    /**
     * Find the number of nodes in this tree at the specified depth. <br>
     * pre: none
     * 
     * @param d The target depth.
     * @return The number of nodes in this tree at a depth equal to the parameter d.
     */
    public int numNodesAtDepth(int d) {
        return nodesAtDepth(d, root, 0);
    }

    // finds number of nodes at depth given
    private int nodesAtDepth(int depth, BSTNode<E> node, int currentDepth) {
        if (node == null || currentDepth > depth) {
            // if you fall off the tree or go too deep, do not add to count
            return 0;
        }
        if (depth == currentDepth) { 
            // at the current depth and value not null, add one to count
            return 1;
        }
        // call on left and right nodes
        return nodesAtDepth(depth, node.left, currentDepth + 1) + nodesAtDepth(depth, node.right, currentDepth + 1);
    }

    /**
     * Prints a vertical representation of this tree. The tree has been rotated
     * counter clockwise 90 degrees. The root is on the left. Each node is printed
     * out on its own row. A node's children will not necessarily be at the rows
     * directly above and below a row. They will be indented three spaces from the
     * parent. Nodes indented the same amount are at the same depth. <br>
     * pre: none
     */
    public void printTree() {
        printTree(root, "");
    }

    // provided by professor
    private void printTree(BSTNode<E> n, String spaces) {
        if (n != null) {
            printTree(n.getRight(), spaces + "  ");
            System.out.println(spaces + n.getData());
            printTree(n.getLeft(), spaces + "  ");
        }
    }

    private static class BSTNode<E extends Comparable<? super E>> {
        private E data;
        private BSTNode<E> left;
        private BSTNode<E> right;

        public BSTNode() {
            this(null);
        }

        public BSTNode(E initValue) {
            this(null, initValue, null);
        }

        public BSTNode(BSTNode<E> initLeft, E initValue, BSTNode<E> initRight) {
            data = initValue;
            left = initLeft;
            right = initRight;
        }

        public E getData() {
            return data;
        }

        public BSTNode<E> getLeft() {
            return left;
        }

        public BSTNode<E> getRight() {
            return right;
        }

        public void setData(E theNewValue) {
            data = theNewValue;
        }

        public void setLeft(BSTNode<E> theNewLeft) {
            left = theNewLeft;
        }

        public void setRight(BSTNode<E> theNewRight) {
            right = theNewRight;
        }
    }
}
