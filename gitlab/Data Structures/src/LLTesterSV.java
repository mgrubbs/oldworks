import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

public class LLTesterSV {

    public static void main(String[] args) {
        // As a preface, toString() is not tested as every test uses toString()
        LinkedList<Object> l = new LinkedList<>();
        Object[] expected = new Object[0];
        String actual = l.toString();
        // constructor
        test("Testing constructor", 1, expected, actual);
        // addFirst 3 tests
        l.addFirst("banana");
        expected = new Object[] { "banana" };
        actual = l.toString();
        test("Testing addFirst", 2, expected, actual);
        expected = new Object[] { 64, 66, "banana" };
        l.addFirst(66);
        l.addFirst(64);
        actual = l.toString();
        test("Testing addFirst", 3, expected, actual);
        expected = new Object[] { 5.1, 3.2, 64, 66, "banana" };
        l.addFirst(3.2);
        l.addFirst(5.1);
        actual = l.toString();
        test("Testing addFirst", 4, expected, actual);
        // addLast
        l.addLast("Oh no");
        expected = new Object[] { 5.1, 3.2, 64, 66, "banana", "Oh no" };
        actual = l.toString();
        test("Testing addLast", 5, expected, actual);
        l.addLast(123.123);
        expected = new Object[] { 5.1, 3.2, 64, 66, "banana", "Oh no", 123.123 };
        actual = l.toString();
        test("Testing addLast", 6, expected, actual);
        l.addLast(8);
        expected = new Object[] { 5.1, 3.2, 64, 66, "banana", "Oh no", 123.123, 8 };
        actual = l.toString();
        test("Testing addLast", 7, expected, actual);
        // removeFirst
        l.removeFirst();
        expected = new Object[] { 3.2, 64, 66, "banana", "Oh no", 123.123, 8 };
        actual = l.toString();
        test("Testing removeFirst", 8, expected, actual);
        l.removeFirst();
        l.removeFirst();
        expected = new Object[] { 66, "banana", "Oh no", 123.123, 8 };
        actual = l.toString();
        test("Testing removeFirst", 9, expected, actual);
        l.removeFirst();
        expected = new Object[] { "banana", "Oh no", 123.123, 8 };
        actual = l.toString();
        test("Testing removeFirst", 10, expected, actual);
        // removeLast
        l.removeLast();
        expected = new Object[] { "banana", "Oh no", 123.123 };
        actual = l.toString();
        test("Testing removeLast", 11, expected, actual);
        l.removeLast();
        expected = new Object[] { "banana", "Oh no" };
        actual = l.toString();
        test("Testing removeLast", 12, expected, actual);
        l.removeLast();
        l.removeLast();
        expected = new Object[0];
        actual = l.toString();
        test("Testing removeLast", 13, expected, actual);
        // add
        l.add("bananas");
        l.add("apples");
        double bread = 3.99;
        l.add(bread);
        expected = new Object[] { "bananas", "apples", 3.99 };
        actual = l.toString();
        test("Testing add", 14, expected, actual);
        l.add("chips");
        l.add(Integer.parseInt("143123"));
        expected = new Object[] { "bananas", "apples", 3.99, "chips", 143123 };
        actual = l.toString();
        test("Testing add", 15, expected, actual);
        l.add(l.toString());
        expected = new Object[] { "bananas", "apples", 3.99, "chips", 143123,
                "[bananas, apples, 3.99, chips, 143123]" };
        actual = l.toString();
        test("Testing add", 16, expected, actual);
        // set, size and get
        l.set(l.size() - 1, l.get(l.size() - 2));
        expected = new Object[] { "bananas", "apples", 3.99, "chips", 143123, 143123 };
        actual = l.toString();
        test("Testing set, size and get", 17, expected, actual);
        l.set(0, l.get(l.size() / 2));
        expected = new Object[] { "chips", "apples", 3.99, "chips", 143123, 143123 };
        actual = l.toString();
        test("Testing set, size and get", 18, expected, actual);
        l.set(l.size() - 1, l.set(0, l.get(2)));
        expected = new Object[] { 3.99, "apples", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing set, size and get", 19, expected, actual);
        // insert
        l.insert(1, 6);
        expected = new Object[] { 3.99, 6, "apples", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing insert", 20, expected, actual);
        l.insert(0, 6);
        expected = new Object[] { 6, 3.99, 6, "apples", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing insert", 21, expected, actual);
        l.insert(l.size() / 2, "Kudamono");
        expected = new Object[] { 6, 3.99, 6, "apples", "Kudamono", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing insert", 22, expected, actual);
        // remove(pos) & remove(obj)
        l.remove(l.remove(0));
        expected = new Object[] { 3.99, "apples", "Kudamono", 3.99, "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing remove", 22, expected, actual);
        l.remove(l.remove(0));
        expected = new Object[] { "apples", "Kudamono", "chips", 143123, "chips" };
        actual = l.toString();
        test("Testing remove", 23, expected, actual);
        l.remove(l.remove(2));
        expected = new Object[] { "apples", "Kudamono", 143123 };
        actual = l.toString();
        test("Testing remove", 24, expected, actual);
        // equals test
        equalsTest();
        // subList and removeRange
        IList<Object> subList = l.getSubList(0, 2);
        expected = new Object[] { "apples", "Kudamono" };
        actual = subList.toString();
        test("Testing subList", 28, expected, actual);
        for (Object o : subList) {
            l.add(o);
        }
        l = (LinkedList<Object>) l.getSubList(0, 3);
        expected = new Object[] { "apples", "Kudamono", 143123 };
        actual = l.toString();
        test("Testing subList", 29, expected, actual);
        l.add(((LinkedList<Object>) l.getSubList(0, 2)).toString());
        expected = new Object[] { "apples", "Kudamono", 143123, "[apples, Kudamono]" };
        actual = l.toString();
        test("Testing subList", 30, expected, actual);
        l.removeRange(1, l.size());
        expected = new Object[] { "apples" };
        actual = l.toString();
        test("Testing removeRange", 31, expected, actual);
        l.removeRange(0, 1);
        expected = new Object[] {};
        actual = l.toString();
        test("Testing removeRange", 32, expected, actual);
        l = new LinkedList<>();
        l.add("hahaha");
        l.add("hihihi");
        l.add("hohoho");
        l.add("hehehe");
        l.add("huhuhu");
        l.removeRange(1, 4);
        actual = l.toString();
        expected = new Object[] { "hahaha", "huhuhu" };
        test("Testing removeRange", 33, expected, actual);
        // makeEmpty()
        l.makeEmpty();
        actual = l.toString();
        expected = new Object[] {};
        test("Testing removeRange", 34, expected, actual);
        l.add(l.toString());
        l.makeEmpty();
        actual = l.toString();
        expected = new Object[] {};
        test("Testing removeRange", 35, expected, actual);
        for(int i = 0; i < 1000000; i++) {
            l.add(i);
        }
        l.makeEmpty();
        actual = l.toString();
        expected = new Object[] {};
        test("Testing removeRange", 36, expected, actual);
        // indexOf(pos) and indexOf(pos, start)
        indexTest();
        // iterator
        iteratorTests(9, 43);
        iteratorTests(4, 44);
        iteratorTests(8, 45);
        
    }

    private static void iteratorTests(int n, int testNum) {
        LinkedList<Object> l = new LinkedList<>();
        Random r = new Random();
        StringBuilder e = new StringBuilder();
        int currentInt = 0;
        for(int i = 0; i < n; i++) {
            currentInt = r.nextInt(26);
            e.append((char)(currentInt + 97));
            l.add((char)(currentInt + 97));
        }
        Iterator<Object> i = l.iterator();
        StringBuilder a = new StringBuilder();
        while(i.hasNext()) {
            a.append(i.next());
            i.remove();
        }
        System.out.println("\nTesting iterator next/hasNext Test number: " + testNum);
        System.out.println("Expected: " + e.toString());
        System.out.println("Actual: " + a.toString());
        if(e.toString().equals(a.toString())) {
            System.out.println("Test number: " + testNum + " passed");
        } else {
            System.out.println("Test number: " + testNum + " failed");
        }
        System.out.println("\nTesting iterator remove Test number: " + (testNum + 1));
        System.out.println("Expected: " + "[]");
        System.out.println("Actual: " + l.toString());
        if(l.toString().equals("[]")) {
            System.out.println("Test number: " + (testNum + 1) + " passed");
        } else {
            System.out.println("Test number: " + (testNum + 1) + " failed");
        }
        
    }

    private static void indexTest() {
        LinkedList<Object> l = new LinkedList<>();
        for(int i = 0; i < 10; i++) {
            l.add(i);
            l.add(i * 2);
            l.add(i * 4);
        }
        System.out.println("\nTesting indexOf Test number: " + 37);
        System.out.println("Expected: " + 0);
        System.out.println("Actual: " + l.indexOf(0));
        if (l.indexOf(0) == 0) {
            System.out.println("Test number: " + 37 + " passed");
        } else {
            System.out.println("Test number: " + 37 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 38);
        System.out.println("Expected: " + 2);
        System.out.println("Actual: " + l.indexOf(0, 2));
        if (l.indexOf(0, 2) == 2) {
            System.out.println("Test number: " + 38 + " passed");
        } else {
            System.out.println("Test number: " + 38 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 39);
        System.out.println("Expected: " + 5);
        System.out.println("Actual: " + l.indexOf(4));
        if (l.indexOf(4) == 5) {
            System.out.println("Test number: " + 39 + " passed");
        } else {
            System.out.println("Test number: " + 39 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 40);
        System.out.println("Expected: " + 7);
        System.out.println("Actual: " + l.indexOf(4, 6));
        if (l.indexOf(4, 6) == 7) {
            System.out.println("Test number: " + 40 + " passed");
        } else {
            System.out.println("Test number: " + 40 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 41);
        System.out.println("Expected: " + 8);
        System.out.println("Actual: " + l.indexOf(8));
        if (l.indexOf(8) == 8) {
            System.out.println("Test number: " + 41 + " passed");
        } else {
            System.out.println("Test number: " + 41 + " failed");
        }
        System.out.println("\nTesting indexOf Test number: " + 42);
        System.out.println("Expected: " + 13);
        System.out.println("Actual: " + l.indexOf(8, 9));
        if (l.indexOf(8, 9) == 13) {
            System.out.println("Test number: " + 42 + " passed");
        } else {
            System.out.println("Test number: " + 42 + " failed");
        }
    }

    private static void test(String testCase, int testNumber, Object[] expect, String LLString) {
        System.out.println("\n" + testCase + " Test number: " + testNumber);
        System.out.println("Expected: " + Arrays.toString(expect));
        System.out.println("Actual: " + LLString);
        if (Arrays.toString(expect).equals(LLString)) {
            System.out.println("Test number: " + testNumber + " passed");
        } else {
            System.out.println("Test number: " + testNumber + " failed");
        }
    }

    private static void equalsTest() {
        LinkedList<Object> unga = new LinkedList<>();
        LinkedList<Object> bunga = new LinkedList<>();
        System.out.println("\nTesting Equals Test number: " + 25);
        System.out.println("Expected: " + true);
        System.out.println("Actual: " + unga.equals(bunga));
        if (unga.equals(bunga)) {
            System.out.println("Test number: " + 25 + " passed");
        } else {
            System.out.println("Test number: " + 25 + " failed");
        }
        unga.add("banana");
        bunga.add("apple");
        System.out.println("\nTesting Equals Test number: " + 26);
        System.out.println("Expected: " + false);
        System.out.println("Actual: " + unga.equals(bunga));
        if (!unga.equals(bunga)) {
            System.out.println("Test number: " + 26 + " passed");
        } else {
            System.out.println("Test number: " + 26 + " failed");
        }
        unga.insert(0, "apple");
        bunga.add("banana");
        System.out.println("\nTesting Equals Test number: " + 27);
        System.out.println("Expected: " + true);
        System.out.println("Actual: " + unga.equals(bunga));
        if (unga.equals(bunga)) {
            System.out.println("Test number: " + 27 + " passed");
        } else {
            System.out.println("Test number: " + 27 + " failed");
        }
    }
}
