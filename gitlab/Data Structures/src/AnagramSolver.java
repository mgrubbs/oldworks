
/* CS 314 STUDENTS: FILL IN THIS HEADER.
 *
 * Student information for assignment:
 *
 *  On my honor, Mark Grubbs, this programming assignment is my own work
 *  and I have not provided this code to any other student.
 *
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *  Number of slip days I am using: 1
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class AnagramSolver {

    private HashMap<String, LetterInventory> dictionaryLetterInventories;

    /*
     * pre: list != null
     * 
     * @param list Contains the words to form anagrams from. creates
     * letterInventories for each of the words in the dictionary
     */
    public AnagramSolver(Set<String> dictionary) {
        if (dictionary == null) {
            throw new IllegalArgumentException("dictionary cannot be null");
        }
        dictionaryLetterInventories = new HashMap<>();
        for (String word : dictionary) {
            if (!dictionaryLetterInventories.containsKey(word)) {
                dictionaryLetterInventories.put(word, new LetterInventory(word));
            }
        }
    }

    /*
     * pre: maxWords >= 0, s != null, s contains at least one English letter. Return
     * a list of anagrams that can be formed from s with no more than maxWords,
     * unless maxWords is 0 in which case there is no limit on the number of words
     * in the anagram
     */
    public List<List<String>> getAnagrams(String s, int maxWords) {
        // CS314 Students, add your code here.
        if (!checkPreconditions(s, maxWords)) {
            throw new IllegalArgumentException("Word target must contain at least one english letter.");
        }
        LetterInventory wordLetterInventory = new LetterInventory(s);
        List<String> initialDictionary = new ArrayList<>();
        for (String word : dictionaryLetterInventories.keySet()) {
            initialDictionary.add(word);
        }
        initialDictionary = removeImpossibleWords(wordLetterInventory, initialDictionary);
        List<List<String>> anagrams = new ArrayList<>();
        anagramFinder(initialDictionary, anagrams, maxWords, wordLetterInventory, new ArrayList<>());
        sortAnagrams(anagrams);
        return anagrams;
    }

    // simply uses Collections.sort to sort the anagrams both in each grouping and
    // for the entire list
    private void sortAnagrams(List<List<String>> anagrams) {
        for (List<String> anagramGroup : anagrams) {
            Collections.sort(anagramGroup);
        }
        Comparator<List<String>> sorter = new AnagramComparator();
        Collections.sort(anagrams, sorter);
    }

    // finds all the anagrams/groups of words that make an anagram for a target word
    // from a dictionary
    private void anagramFinder(List<String> dictionary, List<List<String>> anagrams, int maxWords,
            LetterInventory lettersLeft, List<String> currentAnagrams) {
        if (currentAnagrams.size() == maxWords && maxWords != 0 && lettersLeft.size() > 0) {
            currentAnagrams.remove(currentAnagrams.size() - 1); // base case that you are out of words
        } else if (lettersLeft.size() == 0) {
            anagrams.add(currentAnagrams); // base case that there are no more letters
            currentAnagrams = new ArrayList<>(); // successful anagram
        } else if (!dictionary.isEmpty()) {
            // if the dictionary is not empty, then there are still options available
            // loop through options and passes a deep copy of the currentAnagrams
            // to a recursive call
            // removes words already used from the dictionary to prevent duplicates
            for (int wordInDict = dictionary.size() - 1; wordInDict >= 0; wordInDict--) {
                String word = dictionary.get(wordInDict);
                List<String> wordsThatFit = new ArrayList<>();
                wordsThatFit.addAll(currentAnagrams);
                LetterInventory wordInventory = dictionaryLetterInventories.get(word);
                LetterInventory tryAWord = lettersLeft.subtract(wordInventory);
                wordsThatFit.add(word);
                List<String> smallerDictionary = removeImpossibleWords(tryAWord, dictionary);
                anagramFinder(smallerDictionary, anagrams, maxWords, tryAWord, wordsThatFit);
                dictionary.remove(wordInDict);
            }
        } else {
            // if there were no options available, backtrack
            currentAnagrams.remove(currentAnagrams.size() - 1);
        }
    }

    // checks the preconditions of the getAnagrams method
    private boolean checkPreconditions(String s, int maxWords) {
        if (s == null || maxWords < 0) {
            throw new IllegalArgumentException("String cannot be null and the max words cannot be less than zero.");
        }
        return (new LetterInventory(s)).size() != 0;
    }

    // removes the words from the dictionary that cannot possibly be an anagram with
    // what is left of the target word
    // does not alter the original dictionary
    private List<String> removeImpossibleWords(LetterInventory lettersLeft, List<String> currentDictionary) {
        List<String> smallerDictionary = new ArrayList<>();
        for (String word : currentDictionary) {
            LetterInventory currentWord = dictionaryLetterInventories.get(word);
            if (lettersLeft.subtract(currentWord) != null) {
                smallerDictionary.add(word);
            }
        }
        return smallerDictionary;
    }

    // Comparator to enable usage of sorting the lists of lists of strings
    private static class AnagramComparator implements Comparator<List<String>> {

        // simple comparison of anagram group sizes
        // returns first group - second group for ascending order
        public int compare(List<String> firstAnagrams, List<String> secondAnagrams) {
            int comparison = firstAnagrams.size() - secondAnagrams.size();
            if (comparison == 0) {
                comparison = furtherComparison(firstAnagrams, secondAnagrams);
            }
            return comparison;
        }

        // compares internal data of lists of lists
        // goes through letters of words to find point at which one list should go
        // before the other
        private int furtherComparison(List<String> firstGroup, List<String> secondGroup) {
            int comparison = 0;
            int indexInLists = 0;
            while (comparison == 0 && indexInLists < firstGroup.size()) {
                String firstWord = firstGroup.get(indexInLists);
                String secondWord = secondGroup.get(indexInLists);
                final int LIMIT = Math.min(firstWord.length(), secondWord.length());
                int indexInWord = 0;
                while (comparison == 0 && indexInWord < LIMIT) {
                    comparison = firstWord.charAt(indexInWord) - secondWord.charAt(indexInWord);
                    indexInWord++;
                }
                if (firstWord.length() < secondWord.length() && comparison == 0) {
                    comparison = -1;
                } else if (firstWord.length() > secondWord.length() && comparison == 0) {
                    comparison = 1;
                }
                indexInLists++;
            }
            return comparison;
        }

    }

}
