/*  Student information for assignment:  
 *
 *  On <MY|OUR> honor, <Mark Grubbs> and <NAME2), this programming assignment is <MY|OUR> own work
 *  and <I|WE> have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose turnin account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *
 *  Student 2
 *  UTEID:
 *  email address:
 *
 */
import java.util.ArrayList;

public class HuffPriorityQueue<E extends Comparable<? super E>> {

    // uses list to hold nodes
    private ArrayList<E> nodes;
    
    // creates a priority queue by setting list to new list
    public HuffPriorityQueue(){
        nodes = new ArrayList<>();
    }
    
    // puts an item into the list based on frequency and value
    // fairly places
    public void enqueue(E item) {
        if(item == null) {
            throw new IllegalArgumentException("Cannot use null values.");
        }
        int index = 0;
        while(index < nodes.size() && nodes.get(index).compareTo(item) <= 0) {
            index++;
        }
        nodes.add(index, item);
    }
    
    // removes the front node and returns it
    public E dequeue() {
        if(nodes.isEmpty()) {
            throw new IllegalArgumentException("Cannot dequeue empty queue.");
        }
        E front = nodes.get(0);
        nodes.remove(0);
        return front;
    }

    // returns size of queue
    public int size() {
        return nodes.size();
    }
    
}
