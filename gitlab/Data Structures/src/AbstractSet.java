
/*  Student information for assignment: 
 *
 *  On <MY|OUR> honor, Mark Grubbs and REDACTED, this programming assignment is <MY|OUR> own work
 *  and <I|WE> have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose turnin account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *  Section number:
 *  
 *  Student 2 
 *  UTEID:
 *  email address:
 *  Grader name:
 *  Section number:
 *  
 */

import java.util.Iterator;

/**
 * Students are to complete this class. Students should implement as many
 * methods as they can using the Iterator from the iterator method and the other
 * methods.
 *
 */
public abstract class AbstractSet<E> implements ISet<E> {

    /*
     * NO INSTANCE VARIABLES ALLOWED. NO DIRECT REFERENCE TO UnsortedSet OR
     * SortedSet ALLOWED. (In other words the data types UnsortedSet and SortedSet
     * will not appear any where in this class.) Also no direct references to
     * ArrayList or other Java Collections.
     */

    /**
     * Make this set empty.
     * <br>pre: none
     * <br>post: size() = 0
     * this is O(N)
     */
    public void clear() {
        Iterator<E> it = this.iterator();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    // checks precon that an object is null
    private void preconditionObjNull(E item) {
        if (item == null) {
            throw new IllegalArgumentException("Set does not deal with null objects.");
        }
    }

    /**
     * Remove the specified item from this set if it is present.
     * pre: item != null
     * @param item the item to remove from the set. item may not equal null.
     * @return true if this set changed as a result of this operation, false otherwise
     * this is O(N)
     */
    public boolean remove(E item) {
        preconditionObjNull(item);
        Iterator<E> it = this.iterator();
        while (it.hasNext()) {
            if (it.next().equals(item)) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if item is in this set. 
     * <br>pre: item != null
     * @param item element whose presence is being tested. Item may not equal null.
     * @return true if this set contains the specified item, false otherwise.
     * this is O(N)
     */
    // taken from the assignment webpage
    public boolean contains(E obj) {
        preconditionObjNull(obj);
        for (E item : this) {
            if (item.equals(obj)) {
                return true;
            }
        }
        return false;
    }

    // checks precon of set passed is null
    private void preconditionSetGivenNull(ISet<E> otherSet) {
        if (otherSet == null) {
            throw new IllegalArgumentException("Sets passed cannot be null.");
        }
    }

    /**
     * Determine if all of the elements of otherSet are in this set.
     * <br> pre: otherSet != null
     * @param otherSet != null
     * @return true if this set contains all of the elements in otherSet, 
     * false otherwise.
     * this is O(N^2)
     */
    public boolean containsAll(ISet<E> otherSet) {
        preconditionSetGivenNull(otherSet);
        for (E item : otherSet) {
            if (!contains(item)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Determine if this set is equal to other.
     * Two sets are equal if they have exactly the same elements.
     * The order of the elements does not matter.
     * <br>pre: none
     * @param other the object to compare to this set 
     * @return true if other is a Set and has the same elements as this set
     * this is O(N^2)
     */
    public boolean equals(Object otherSet) {
        if (otherSet == null) {
            throw new IllegalArgumentException("set to compare against cannot be null");
        }
        boolean isEqual = false;
        if (otherSet instanceof ISet<?>) {
            isEqual = ((ISet<E>) (otherSet)).size() == size() && containsAll(((ISet<E>) (otherSet)));
        }
        return isEqual;
    }

    /**
     * Return the number of elements of this set.
     * pre: none
     * @return the number of items in this set
     * this is O(N)
     */
    public int size() {
        int size = 0;
        Iterator<E> it = this.iterator();
        while (it.hasNext()) {
            size++;
            it.next();
        }
        return size;
    }

    /**
     * create a new set that is the intersection of this set and otherSet.
     * <br>pre: otherSet != null<br>
     * <br>post: returns a set that is the intersection of this set and otherSet.
     * Neither this set or otherSet are altered as a result of this operation.
     * <br> pre: otherSet != null
     * @param otherSet != null
     * @return a set that is the intersection of this set and otherSet
     * this has order dependent on sorted or unsorted
     */
    public ISet<E> intersection(ISet<E> otherSet) {
        preconditionSetGivenNull(otherSet);
        ISet<E> start = difference(otherSet);
        return difference(start);
    }

    /**
     * Create a new set that is the union of this set and otherSet.
     * <br>pre: otherSet != null
     * <br>post: returns a set that is the union of this set and otherSet.
     * Neither this set or otherSet are altered as a result of this operation.
     * <br> pre: otherSet != null
     * @param otherSet != null
     * @return a set that is the union of this set and otherSet
     */
    public abstract ISet<E> union(ISet<E> otherSet);

    /**
     * Create a new set that is the difference of this set and otherSet. Return an ISet of 
     * elements that are in this Set but not in otherSet. Also called
     * the relative complement. 
     * <br>Example: If ISet A contains [X, Y, Z] and ISet B contains [W, Z] then
     * A.difference(B) would return an ISet with elements [X, Y] while
     * B.difference(A) would return an ISet with elements [W]. 
     * <br>pre: otherSet != null
     * <br>post: returns a set that is the difference of this set and otherSet.
     * Neither this set or otherSet are altered as a result of this operation.
     * <br> pre: otherSet != null
     * @param otherSet != null
     * @return a set that is the difference of this set and otherSet
     */
    public abstract ISet<E> difference(ISet<E> otherSet);

    // provided by professor
    public String toString() {
        StringBuilder result = new StringBuilder();
        String seperator = ", ";
        result.append("(");

        Iterator<E> it = this.iterator();
        while (it.hasNext()) {
            result.append(it.next());
            result.append(seperator);
        }
        // get rid of extra separator
        if (this.size() > 0)
            result.setLength(result.length() - seperator.length());

        result.append(")");
        return result.toString();
    }
}
