
/*  Student information for assignment:  
 *
 *  On <MY|OUR> honor, <Mark Grubbs> and <NAME2), this programming assignment is <MY|OUR> own work
 *  and <I|WE> have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose turnin account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *
 *  Student 2
 *  UTEID:
 *  email address:
 *
 */
import java.io.IOException;

public class Decompressor implements IHuffConstants {

    // data in tree
    private HuffTree dataFromCompress;
    // size of file
    private int size;
    // frequencies of chars
    private int[] freq;

    public Decompressor(BitInputStream bitIn) throws IOException {
        int headerFormat = bitIn.readBits(BITS_PER_INT);
        if (headerFormat == STORE_COUNTS) {
            getCountsData(bitIn);
        } else if (headerFormat == STORE_TREE) {
            getTreeData(bitIn);
        }
    }

    // creates a tree from STORE_TREE header
    private void getTreeData(BitInputStream bitIn) throws IOException {
        HuffPriorityQueue<TreeNode> preorderList= new HuffPriorityQueue<>();
        int frequency = 0;
        int bitsLeft = bitIn.readBits(BITS_PER_INT);
        while(bitsLeft > 0) {
            int input = bitIn.readBits(1);
            bitsLeft--;
            if(input == 1) {
                bitsLeft -= BITS_PER_WORD + 1;
                input = bitIn.readBits(BITS_PER_WORD + 1);
                TreeNode node = new TreeNode(input, frequency);
                preorderList.enqueue(node);
                frequency++;
            }
        }
    }

    // gets the uncompressedSize
    private void setUncompressedSize(int[] freqs) {
        size = 0;
        for (int i = 0; i < ALPH_SIZE; i++) {
            size += BITS_PER_WORD * freqs[i];
        }
    }

    // returns size of file printed
    public int getSize() {
        return size;
    }

    // gets STORE_COUNTS version of data from header
    // recreates tree normal way
    private void getCountsData(BitInputStream bitIn) throws IOException {
        int[] frequencies = new int[ALPH_SIZE + 1];
        for (int bits = 0; bits < ALPH_SIZE; bits++) {
            frequencies[bits] = bitIn.readBits(BITS_PER_INT);
        }
        freq = frequencies;
        dataFromCompress = new HuffTree(frequencies);
        setUncompressedSize(frequencies);
    }

    // displays frequencies
    public String displayInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Frequencies from header\n");
        for (int f = 0; f < ALPH_SIZE; f++) {
            if (freq[f] > 0)
                sb.append(f + " " + freq[f] + "\n");
        }
        return sb.toString();
    }

    // decompresses the file
    public void writeDecompressed(BitInputStream bitIn, BitOutputStream bitOut) throws IOException {
        dataFromCompress.decompress(bitIn, bitOut);
    }
}
