
/*  Student information for assignment:
 *
 *  On MY honor, Mark Grubbs, this programming assignment is MY own work
 *  and I have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose Canvas account is being used) Mark Grubbs
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name: 
 *  Section number:
 *
 *  Student 2
 *  UTEID: doesn't exist
 *  email address:
 *
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Tester class for the methods in Recursive.java
 * 
 * @author scottm
 *
 */
public class RecursiveTester {

    // run the tests
    public static void main(String[] args) {

       studentTests();
    }

    private static void flowOffMapTests() {
        int[][] flowData = new int[10][10];
        int start = 3;
        for (int x = flowData.length - 1; x >= 0; x--) {
            for (int y = 0; y < flowData[0].length; y++) {
                int elevation = (int) (start + x * y / 5.0);
                if (elevation % 3 == 0) {
                    elevation = elevation / 3;
                }
                flowData[x][y] = elevation;
            }
        }
        for (int[] i : flowData) {
            System.out.println(Arrays.toString(i));
        }
        boolean actual = Recursive.canFlowOffMap(flowData, 4, 7);
        boolean expected = true;
        System.out.println("Test 1 : start at 4,7");
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual) {
            System.out.println("passed test 1\n");
        } else {
            System.out.println("failed test 1\n");
        }
        actual = Recursive.canFlowOffMap(flowData, 6, 6);
        expected = false;
        System.out.println("Test 2 : start at 6,6");
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (!actual) {
            System.out.println("passed test 1\n");
        } else {
            System.out.println("failed test 1\n");
        }
    }

    private static void listMnemonicsTests() {
        String digitData = "45";
        ArrayList<String> actual = Recursive.listMnemonics(digitData);
        ArrayList<String> expected = new ArrayList<>();
        expected.add("GJ");
        expected.add("GK");
        expected.add("GL");
        expected.add("HJ");
        expected.add("HK");
        expected.add("HL");
        expected.add("IJ");
        expected.add("IK");
        expected.add("IL");
        System.out.println("Test 1 : " + digitData);
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual.equals(expected)) {
            System.out.println("passed test 1\n");
        } else {
            System.out.println("failed test 1\n");
        }
        digitData = "102";
        expected.clear();
        actual = Recursive.listMnemonics(digitData);
        expected.add("10A");
        expected.add("10B");
        expected.add("10C");
        System.out.println("Test 2 : " + digitData);
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual.equals(expected)) {
            System.out.println("passed test 2\n");
        } else {
            System.out.println("failed test 2\n");
        }

    }

    private static final String[] letters = { "0", "1", "ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ" };

    private static void nextIsDoubleTests() {
        int[] testData = new int[7];
        testData[0] = 4;
        testData[1] = 2 * testData[0];
        testData[2] = 4 * testData[0];
        testData[3] = 4 * testData[1];
        testData[4] = 0;
        testData[5] = 0;
        testData[6] = 4 * testData[5];
        int expected = 5;
        int actual = Recursive.nextIsDouble(testData);
        System.out.println("Test 1: ");
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual == expected) {
            System.out.println("passed test 1\n");
        } else {
            System.out.println("failed test 1\n");
        }
        testData[1] = testData[1] * 2;
        testData[5] = 1;
        expected = 1;
        actual = Recursive.nextIsDouble(testData);
        System.out.println("Test 2: ");
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual == expected) {
            System.out.println("passed test 2\n");
        } else {
            System.out.println("failed test 2\n");
        }

    }

    private static void binaryTests() {
        int firstTest = -123123;
        System.out.println("Test 1: " + firstTest);
        String actual = Recursive.getBinary(firstTest);
        String expected = "-11110000011110011";
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual.equals(expected)) {
            System.out.println("passed test 1\n");
        } else {
            System.out.println("failed test 1\n");
        }
        int secondTest = 2001001001;
        System.out.println("Test 2: " + secondTest);
        actual = Recursive.getBinary(secondTest);
        expected = "1110111010001001101101000101001";
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual.equals(expected)) {
            System.out.println("passed test 2\n");
        } else {
            System.out.println("failed test 2\n");
        }
    }

    private static void reverseTests() {
        String firstTest = "girafarig";
        System.out.println("Test 1: " + firstTest);
        String actual = Recursive.revString(firstTest);
        String expected = firstTest;
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual.equals(expected)) {
            System.out.println("passed test 1\n");
        } else {
            System.out.println("failed test 1\n");
        }
        String secondTest = Recursive.getBinary(127) + " " + Recursive.getBinary(63) + " " + Recursive.getBinary(31)
                + " " + Recursive.getBinary(63) + " " + Recursive.getBinary(127);
        System.out.println("Test 2: " + secondTest);
        actual = Recursive.revString(secondTest);
        expected = secondTest;
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual.equals(expected)) {
            System.out.println("passed test 2\n");
        } else {
            System.out.println("failed test 2\n");
        }
    }

//    private static void doMazeTests() {
//        int mazeTestNum = 1;
//        runMazeTest("$GSGE", 1, 2, mazeTestNum++);
//        runMazeTest("$Y$SGE", 1, 1, mazeTestNum++);
//        String maze = "GGSGG**GGGGG";
//        runMazeTest(maze, 3, 0, mazeTestNum++);
//        maze = "GSGGG**GGEGG";
//        runMazeTest(maze, 3, 2, mazeTestNum++);
//        maze = "*EY****YYYYYY**YYYSY";
//        runMazeTest(maze, 5, 0, mazeTestNum++);
//        maze = "**$****G**ESGG$**G****$**";
//        runMazeTest(maze, 5, 1, mazeTestNum++);
//        maze = "**$****G**ESGG$*GGG***$G*";
//        runMazeTest(maze, 5, 2, mazeTestNum++);
//        maze = "**$****G**ESGG$**G*$**$**";
//        runMazeTest(maze, 5, 1, mazeTestNum++);
//        maze = "EYSG$";
//        runMazeTest(maze, 1, 2, mazeTestNum++);
//        maze = "EYYGGGYGSGGG$GG";
//        runMazeTest(maze, 3, 2, mazeTestNum++);
//        maze = "EGG$$$GGSGG$$$$";
//        runMazeTest(maze, 3, 2, mazeTestNum++);
//        maze = "EYY$$$YYSG***YYG$$$$";
//        runMazeTest(maze, 4, 2, mazeTestNum++);
//        maze = "EYY$$$YYSG****YG$$$$";
//        runMazeTest(maze, 4, 1, mazeTestNum++);
//        maze = "EYY$$$**SGY***YG$$$$";
//        runMazeTest(maze, 4, 2, mazeTestNum++);
//    }
//
//    private static void runMazeTest(String rawMaze, int rows, int expected, int num) {
//        char[][] maze = makeMaze(rawMaze, rows);
//        System.out.println("Can escape maze test number " + num);
//        printMaze(maze);
//        int actual = Recursive.canEscapeMaze(maze);
//        System.out.println("Expected result: " + expected);
//        System.out.println("Actual result:   " + actual);
//        if (expected == actual) {
//            System.out.println("passed test " + num);
//        } else {
//            System.out.println("FAILED TEST " + num);
//        }
//        System.out.println();
//    }
//
//    // print the given maze
//    // pre: none
//    private static void printMaze(char[][] maze) {
//        if (maze == null) {
//            System.out.println("NO MAZE GIVEN");
//        } else {
//            for (char[] row : maze) {
//                for (char c : row) {
//                    System.out.print(c);
//                    System.out.print(" ");
//                }
//                System.out.println();
//            }
//        }
//    }
//
//    // generate a char[][] given the raw string
//    // pre: rawMaze != null, rawMaze.length() % rows == 0
//    private static char[][] makeMaze(String rawMaze, int rows) {
//        if (rawMaze == null || rawMaze.length() % rows != 0) {
//            throw new IllegalArgumentException(
//                    "Violation of precondition in makeMaze." + "Either raw data is null or left over values: ");
//        }
//        int cols = rawMaze.length() / rows;
//        char[][] result = new char[rows][cols];
//        int rawIndex = 0;
//        for (int r = 0; r < result.length; r++) {
//            for (int c = 0; c < result[0].length; c++) {
//                result[r][c] = rawMaze.charAt(rawIndex);
//                rawIndex++;
//            }
//        }
//        return result;
//    }
//
//    private static void doCarpetTest() {
//        Recursive.drawCarpet(729, 4);
//        Recursive.drawCarpet(729, 1);
//
//    }

    private static void doFairTeamsTests() {
        System.out.println("Stress test for minDifference - may take up to a minute");
        int[] testerArr = new int[] {5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 100000};
        Stopwatch s = new Stopwatch();
        s.start();
        int actualInt = Recursive.minDifference(4, testerArr);
        s.stop();
        System.out.println("Time to solve for 16 people on 4 teams: " + s.time() + "\n");
        System.out.println(actualInt);

        int[] abilities = { 1, 2, 3, 4, 5, 6, 7 };
        if (Recursive.minDifference(3, abilities) == 1)
            System.out.println("Test 1 passed. min difference.");
        else
            System.out.println("Test 1 failed. min difference.");

        if (Recursive.minDifference(5, abilities) == 2)
            System.out.println("Test 2 passed. min difference.");
        else
            System.out.println("Test 2 failed. min difference.");
        if (Recursive.minDifference(6, abilities) == 4)
            System.out.println("Test 3 passed. min difference.");
        else
            System.out.println("Test 3 failed. min difference.");

        abilities = new int[] { 1, 12, 46, 60, 53, 86, 72, 79, 44, 7 };
        if (Recursive.minDifference(3, abilities) == 3)
            System.out.println("Test 4 passed. min difference.");
        else
            System.out.println("Test 4 failed. min difference.");

        if (Recursive.minDifference(5, abilities) == 19)
            System.out.println("Test 5 passed. min difference.");
        else
            System.out.println("Test 5 failed. min difference.");

        abilities = new int[] { 10, 10, 6, 6, 6 };
        if (Recursive.minDifference(2, abilities) == 2)
            System.out.println("Test 6 passed. min difference.");
        else
            System.out.println("Test 6 failed. min difference.");

        abilities = new int[] { -10, -10, -8, -8, -8 };
        if (Recursive.minDifference(2, abilities) == 4)
            System.out.println("Test 6 passed. min difference.");
        else
            System.out.println("Test 6 failed. min difference.");

        abilities = new int[] { -5, 5, 10, 5, 10, -15 };
        if (Recursive.minDifference(2, abilities) == 0)
            System.out.println("Test 7 passed. min difference.");
        else
            System.out.println("Test 7 failed. min difference.");
    }

//    private static void doFlowOffMapTests() {
//        int testNum = 1;
//        int[][] world = { { 5, 5, 5, 5, 5, 5 }, { 5, 5, 5, 5, 5, 5 }, { 5, 5, 5, 5, 5, 5 }, { 5, 5, 4, 4, 5, 5 },
//                { 5, 5, 3, 3, 5, 5 }, { 5, 5, 2, 2, 5, 5 }, { 5, 5, 5, 1, 5, 5 }, { 5, 5, 5, -2, 5, 5 } };
//
//        doOneFlowTest(world, 0, 0, true, testNum++);
//        doOneFlowTest(world, 1, 1, false, testNum++);
//        doOneFlowTest(world, 3, 3, true, testNum++);
//        doOneFlowTest(world, 1, 5, true, testNum++);
//
//        world = new int[][] { { 10, 10, 10, 10, 10, 10, 10 }, { 10, 10, 10, 5, 10, 10, 10 },
//                { 10, 10, 10, 6, 10, 10, 10 }, { 10, 10, 10, 7, 10, 10, 10 }, { 5, 6, 7, 8, 7, 6, 10 },
//                { 10, 10, 10, 7, 10, 10, 10 }, { 10, 10, 10, 6, 10, 10, 10 }, { 10, 10, 10, 5, 10, 10, 10 },
//                { 10, 10, 10, 10, 10, 10, 10 } };
//
//        doOneFlowTest(world, 3, 3, false, testNum++);
//        doOneFlowTest(world, 4, 3, true, testNum++);
//        System.out.println();
//    }
//
//    private static void doOneFlowTest(int[][] world, int r, int c, boolean expected, int testNum) {
//        System.out.println("Can Flow Off Map Test Number " + testNum);
//        boolean actual = Recursive.canFlowOffMap(world, r, c);
//        System.out.println("Start location = " + r + ", " + c);
//        System.out.println("Expected result = " + expected + " actual result = " + actual);
//        if (expected == actual) {
//            System.out.println("passed test " + testNum + " can flow off map.");
//        } else {
//            System.out.println("FAILED TEST " + testNum + " can flow off map.");
//        }
//        System.out.println();
//    }
//
//    private static void doListMnemonicsTests() {
//        ArrayList<String> mnemonics = Recursive.listMnemonics("1");
//        ArrayList<String> expected = new ArrayList<>();
//        expected.add("1");
//        if (mnemonics.equals(expected))
//            System.out.println("Test 1 passed. Phone mnemonics.");
//        else
//            System.out.println("Test1 failed. Phone mnemonics.");
//
//        mnemonics = Recursive.listMnemonics("22");
//        Collections.sort(mnemonics);
//        expected.clear();
//        expected.add("AA");
//        expected.add("AB");
//        expected.add("AC");
//        expected.add("BA");
//        expected.add("BB");
//        expected.add("BC");
//        expected.add("CA");
//        expected.add("CB");
//        expected.add("CC");
//        Collections.sort(expected);
//        if (mnemonics.equals(expected))
//            System.out.println("Test 2 passed. Phone mnemonics.");
//        else
//            System.out.println("Test 2 failed. Phone mnemonics.");
//
//        mnemonics = Recursive.listMnemonics("110010");
//        expected.clear();
//        expected.add("110010");
//        if (mnemonics.equals(expected))
//            System.out.println("Test 3 passed. Phone mnemonics.");
//        else
//            System.out.println("Test 3 failed. Phone mnemonics.");
//        System.out.println();
//
//    }
//
//    private static void doNextIsDoubleTests() {
//        int[] numsForDouble = { 1, 2, 4, 8, 16, 32, 64, 128, 256 };
//        int actualDouble = Recursive.nextIsDouble(numsForDouble);
//        int expectedDouble = 8;
//        if (actualDouble == expectedDouble)
//            System.out.println("Test 1 passed. next is double.");
//        else
//            System.out.println(
//                    "Test 1 failed. next is double. expected: " + expectedDouble + ", actual: " + actualDouble);
//
//        numsForDouble = new int[] { 1, 3, 4, 2, 32, 8, 128, -5, 6 };
//        actualDouble = Recursive.nextIsDouble(numsForDouble);
//        expectedDouble = 0;
//        if (actualDouble == expectedDouble)
//            System.out.println("Test 2 passed. next is double.");
//        else
//            System.out.println(
//                    "Test 2 failed. next is double. expected: " + expectedDouble + ", actual: " + actualDouble);
//
//        numsForDouble = new int[] { 1, 0, 0, -5, -10, 32, 64, 128, 2, 9, 18 };
//        actualDouble = Recursive.nextIsDouble(numsForDouble);
//        expectedDouble = 5;
//        if (actualDouble == expectedDouble)
//            System.out.println("Test 3 passed. next is double.");
//        else
//            System.out.println(
//                    "Test 3 failed. next is double. expected: " + expectedDouble + ", actual: " + actualDouble);
//
//        numsForDouble = new int[] { 37 };
//        actualDouble = Recursive.nextIsDouble(numsForDouble);
//        expectedDouble = 0;
//        if (actualDouble == expectedDouble)
//            System.out.println("Test 4 passed. next is double.");
//        else
//            System.out.println(
//                    "Test 4 failed. next is double. expected: " + expectedDouble + ", actual: " + actualDouble);
//
//        numsForDouble = new int[] { 37, 74 };
//        actualDouble = Recursive.nextIsDouble(numsForDouble);
//        expectedDouble = 1;
//        if (actualDouble == expectedDouble)
//            System.out.println("Test 5 passed. next is double.");
//        else
//            System.out.println(
//                    "Test 5 failed. next is double. expected: " + expectedDouble + ", actual: " + actualDouble);
//        System.out.println();
//    }
//
//    private static void doReverseTests() {
//        String actualRev = Recursive.revString("target");
//        String expectedRev = "tegrat";
//        if (actualRev.equals(expectedRev))
//            System.out.println("Test 1 passed. reverse string.");
//        else
//            System.out.println("Test 1 failed. reverse string. expected: " + expectedRev + ", actual: " + actualRev);
//
//        actualRev = Recursive.revString("Calvin and Hobbes");
//        expectedRev = "sebboH dna nivlaC";
//        if (actualRev.equals(expectedRev))
//            System.out.println("Test 2 passed. reverse string.");
//        else
//            System.out.println("Test 2 failed. reverse string. expected: " + expectedRev + ", actual: " + actualRev);
//
//        actualRev = Recursive.revString("U");
//        expectedRev = "U";
//        if (actualRev.equals(expectedRev))
//            System.out.println("Test 3 passed. reverse string.");
//        else
//            System.out.println("Test 3 failed. reverse string. expected: " + expectedRev + ", actual: " + actualRev);
//        System.out.println();
//    }

//    private static void doBinaryTests() {
//        String actualBinary = Recursive.getBinary(13);
//        String expectedBinary = "1101";
//        if (actualBinary.equals(expectedBinary))
//            System.out.println( "Test 1 passed. get binary.");
//        else
//            System.out.println( "Test 1 failed. get binary. expected: " + expectedBinary + ", actual: " + actualBinary);
//
//
//        actualBinary = Recursive.getBinary(0);
//        expectedBinary = "0";
//        if (actualBinary.equals(expectedBinary))
//            System.out.println( "Test 2 passed. get binary.");
//        else
//            System.out.println( "Test 2 failed. get binary. expected: " + expectedBinary + ", actual: " + actualBinary);
//
//        actualBinary = Recursive.getBinary(-35);
//        expectedBinary = "-100011";
//        if (actualBinary.equals(expectedBinary))
//            System.out.println( "Test 3 passed. get binary.");
//        else
//            System.out.println( "Test 3failed. get binary. expected: " + expectedBinary + ", actual: " + actualBinary);
//
//        actualBinary = Recursive.getBinary(1);
//        expectedBinary = "1";
//        if (actualBinary.equals(expectedBinary))
//            System.out.println( "Test 4 passed. get binary.");
//        else
//            System.out.println( "Test 4 failed. get binary. expected: " + expectedBinary + ", actual: " + actualBinary);
//
//
//        actualBinary = Recursive.getBinary(64);
//        expectedBinary = "1000000";
//        if (actualBinary.equals(expectedBinary))
//            System.out.println( "Test 5 passed. get binary");
//        else
//            System.out.println( "Test 5 failed. get binary. expected: " + expectedBinary + ", actual: " + actualBinary);
//        System.out.println();
//    }

    // pre: r != null
    // post: run student test
    private static void studentTests() {
        // CS314 students put your tests here
        System.out.println("Binary Tests :");
        binaryTests();
        System.out.println("Reverse Tests :");
        reverseTests();
        System.out.println("NextIsDouble Tests :");
        nextIsDoubleTests();
        System.out.println("Mnemonics Tests :");
        listMnemonicsTests();
        System.out.println("CanFlow Tests :");
        flowOffMapTests();
        System.out.println("MinDifference Tests :");
        fairTeamsTests();
        System.out.println("Maze Tests :");
        mazeTests();
    }

    private static void mazeTests() {
        char[][] maze = new char[6][6];
        for (int r = 0; r < maze.length; r++) {
            for (int c = 0; c < maze[r].length; c++) {
                maze[r][c] = 'G';
            }
        }
        maze[3][3] = 'S';
        int expected = 0;
        int actual = Recursive.canEscapeMaze(maze);
        System.out.println("Test 1: ");
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual == expected) {
            System.out.println("passed test 1\n");
        } else {
            System.out.println("failed test 1\n");
        }
        maze[5][4] = 'E';
        expected = 2;
        actual = Recursive.canEscapeMaze(maze);
        System.out.println("Test 2: ");
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual == expected) {
            System.out.println("passed test 2\n");
        } else {
            System.out.println("failed test 2\n");
        }

    }

    private static void fairTeamsTests() {
        int[] abilities = { 3, 6, 1, -4, 5, 8 };
        int actual = Recursive.minDifference(3, abilities);
        int expected = 2;
        System.out.println("Test 1: ");
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual == expected) {
            System.out.println("passed test 1\n");
        } else {
            System.out.println("failed test 1\n");
        }
        actual = Recursive.minDifference(5, abilities);
        expected = 5;
        System.out.println("Test 2: ");
        System.out.println("expected = " + expected);
        System.out.println("actual = " + actual);
        if (actual == expected) {
            System.out.println("passed test 2\n");
        } else {
            System.out.println("failed test 2\n");
        }
    }
}