import java.util.Arrays;
//MathMatrix.java - CS314 Assignment 2

/*  Student information for assignment:
*
*  On my honor, Mark Grubbs, this programming assignment is my own work
*  and I have not provided this code to any other student.
*
*  UTEID: msg2772
*  email address: siegbalicula@gmail.com
*  Unique section number:
*  Number of slip days I am using:
*/

/**
 * A class that models systems of linear equations (Math Matrices) as used in
 * linear algebra.
 */
public class MathMatrix {

    // instance variable
    private int[][] mathMat;

    /**
     * create a MathMatrix with cells equal to the values in mat. A "deep" copy of
     * mat is made. Changes to mat after this constructor do not affect this Matrix
     * and changes to this MathMatrix do not affect mat
     * 
     * @param mat mat !=null, mat.length > 0, mat[0].length > 0, mat is a
     *            rectangular matrix
     */
    public MathMatrix(int[][] mat) {
        if (!MathMatrix.rectangularMatrix(mat)) {
            throw new IllegalArgumentException("Given matrix must be rectangular");
        }
        mathMat = new int[mat.length][mat[0].length];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                mathMat[i][j] = mat[i][j];
            }
        }
    }

    /**
     * create a MathMatrix of the specified size with all cells set to the
     * intialValue. <br>
     * pre: numRows > 0, numCols > 0 <br>
     * post: create a matrix with numRows rows and numCols columns. All elements of
     * this matrix equal initialVal. In other words after this method has been
     * called getVal(r,c) = initialVal for all valid r and c.
     * 
     * @param numRows    numRows > 0
     * @param numCols    numCols > 0
     * @param initialVal all cells of this Matrix are set to initialVal
     */
    public MathMatrix(int numRows, int numCols, int initialVal) {
        if (numRows <= 0 || numCols <= 0) {
            throw new IllegalArgumentException("Cannot give non positive parameters for number of rows and columns");
        }
        mathMat = new int[numRows][numCols];
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                mathMat[i][j] = initialVal;
            }
        }
    }

    /**
     * Get the number of rows.
     * 
     * @return the number of rows in this MathMatrix
     */
    public int getNumRows() {
        return mathMat.length; // returns rows
    }

    /**
     * Get the number of columns.
     * 
     * @return the number of columns in this MathMatrix
     */
    public int getNumColumns() {
        return mathMat[0].length; // returns columns
    }

    /**
     * get the value of a cell in this MathMatrix. <br>
     * pre: row 0 <= row < getNumRows(), col 0 <= col < getNumColumns()
     * 
     * @param row 0 <= row < getNumRows()
     * @param col 0 <= col < getNumColumns()
     * @return the value at the specified position
     */
    public int getVal(int row, int col) {
        // checks if outOfBounds
        if (row >= getNumRows() || row < 0 || col >= getNumColumns() || col < 0) {
            throw new IllegalArgumentException("A given parameter is out of bounds.");
        }
        return mathMat[row][col]; // returns cell value
    }

    /**
     * implements MathMatrix addition, (this MathMatrix) + rightHandSide. <br>
     * pre: rightHandSide.getNumRows() = getNumRows(), rightHandSide.numCols() =
     * getNumColumns() <br>
     * post: This method does not alter the calling object or rightHandSide
     * 
     * @param rightHandSide rightHandSide.getNumRows() = getNumRows(),
     *                      rightHandSide.numCols() = getNumColumns()
     * @return a new MathMatrix that is the result of adding this Matrix to
     *         rightHandSide. The number of rows in the returned Matrix is equal to
     *         the number of rows in this MathMatrix. The number of columns in the
     *         returned Matrix is equal to the number of columns in this MathMatrix.
     */
    public MathMatrix add(MathMatrix rightHandSide) {
        return addOrSubtract(rightHandSide, true); // calls addOrSubtract method which does one depending on whatever
                                                   // the boolean is. true = add; false = subtract
    }

    /**
     * implements MathMatrix subtraction, (this MathMatrix) - rightHandSide. <br>
     * pre: rightHandSide.getNumRows() = getNumRows(), rightHandSide.numCols() =
     * getNumColumns() <br>
     * post: This method does not alter the calling object or rightHandSide
     * 
     * @param rightHandSide rightHandSide.getNumRows() = getNumRows(),
     *                      rightHandSide.numCols() = getNumColumns()
     * @return a new MathMatrix that is the result of subtracting rightHandSide from
     *         this MathMatrix. The number of rows in the returned MathMatrix is
     *         equal to the number of rows in this MathMatrix. The number of columns
     *         in the returned MathMatrix is equal to the number of columns in this
     *         MathMatrix.
     */
    public MathMatrix subtract(MathMatrix rightHandSide) {
        return addOrSubtract(rightHandSide, false); // same thing as add
    }

    private MathMatrix addOrSubtract(MathMatrix rightHandSide, boolean isAddition) {
        // checks similar precondition
        if (rightHandSide.getNumRows() != getNumRows() || rightHandSide.getNumColumns() != getNumColumns()) {
            throw new IllegalArgumentException(
                    "The given Math Matrix to add to the called Math Matrix does not have the same dimensions.");
        }
        int[][] addedMat = new int[getNumRows()][getNumColumns()];
        if (isAddition) {
            for (int i = 0; i < getNumRows(); i++) {
                for (int j = 0; j < getNumColumns(); j++) {
                    addedMat[i][j] = rightHandSide.getVal(i, j) + getVal(i, j); // addition loop
                }
            }
            return new MathMatrix(addedMat);
        }
        for (int i = 0; i < getNumRows(); i++) {
            for (int j = 0; j < getNumColumns(); j++) {
                addedMat[i][j] = getVal(i, j) - rightHandSide.getVal(i, j); // subtraction loop
            }
        }
        return new MathMatrix(addedMat);
    }

    /**
     * implements matrix multiplication, (this MathMatrix) * rightHandSide. <br>
     * pre: rightHandSide.getNumRows() = getNumColumns() <br>
     * post: This method should not alter the calling object or rightHandSide
     * 
     * @param rightHandSide rightHandSide.getNumRows() = getNumColumns()
     * @return a new MathMatrix that is the result of multiplying this MathMatrix
     *         and rightHandSide. The number of rows in the returned MathMatrix is
     *         equal to the number of rows in this MathMatrix. The number of columns
     *         in the returned MathMatrix is equal to the number of columns in
     *         rightHandSide.
     */
    public MathMatrix multiply(MathMatrix rightHandSide) {
        if (rightHandSide.getNumRows() != getNumColumns()) {
            throw new IllegalArgumentException(
                    "The amount of the rows on the right hand side is not equal to the left hand side's number of columns.");
        }
        int[][] productMat = new int[getNumRows()][rightHandSide.getNumColumns()];
        if (checkIfZeroMatrix(this) || checkIfZeroMatrix(rightHandSide)) {
            return new MathMatrix(productMat); // if either matrix is a zero matrix, the new matrix will be a zero
                                               // matrix
        }
        int currIForProduct = 0; // store i and j for the new matrix
        int currJForProduct = 0;
        // goes through right matrix's columns and multiplies appropriately across rows
        // of left matrix
        for (int rightJ = 0; rightJ < rightHandSide.getNumColumns(); rightJ++) {
            for (int leftI = 0; leftI < getNumRows(); leftI++) {
                int total = 0; // to be set for the new matrix
                for (int leftJ = 0; leftJ < getNumColumns(); leftJ++) { // one right column multiplies with multiple
                                                                        // left rows
                    total += getVal(leftI, leftJ) * rightHandSide.getVal(leftJ, rightJ);
                }
                productMat[currIForProduct][currJForProduct] = total;
                currIForProduct++; // after product is found, place in cell and then go down the column
                if (currIForProduct == getNumRows()) { // if out of rows, go to next column
                    currIForProduct = 0;
                    currJForProduct++;
                }
            }
        }
        return new MathMatrix(productMat);
    }

    // checks if the matrix is a zero matrix
    private boolean checkIfZeroMatrix(MathMatrix data) {
        for (int i = 0; i < data.getNumRows(); i++) {
            for (int j = 0; j < data.getNumRows(); j++) {
                if (data.getVal(i, j) != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Create and return a new Matrix that is a copy of this matrix, but with all
     * values multiplied by a scale value. <br>
     * pre: none <br>
     * post: returns a new Matrix with all elements in this matrix multiplied by
     * factor. In other words after this method has been called
     * returned_matrix.getVal(r,c) = original_matrix.getVal(r, c) * factor for all
     * valid r and c.
     * 
     * @param factor the value to multiply every cell in this Matrix by.
     * @return a MathMatrix that is a copy of this MathMatrix, but with all values
     *         in the result multiplied by factor.
     */
    public MathMatrix getScaledMatrix(int factor) {
        // makes a new matrix and multiplies each cell by the scaling factor
        int[][] scaledMat = new int[getNumRows()][getNumColumns()];
        for (int i = 0; i < getNumRows(); i++) {
            for (int j = 0; j < getNumColumns(); j++) {
                scaledMat[i][j] = getVal(i, j) * factor;
            }
        }
        return new MathMatrix(scaledMat);
    }

    /**
     * accessor: get a transpose of this MathMatrix. This Matrix is not changed.
     * <br>
     * pre: none
     * 
     * @return a transpose of this MathMatrix
     */
    public MathMatrix getTranspose() {
        // flips the i, j for each cell
        int[][] transposedMat = new int[getNumColumns()][getNumRows()];
        for (int i = 0; i < getNumRows(); i++) {
            for (int j = 0; j < getNumColumns(); j++) {
                transposedMat[j][i] = getVal(i, j);
            }
        }
        return new MathMatrix(transposedMat);
    }

    /**
     * override equals.
     * 
     * @return true if rightHandSide is the same size as this MathMatrix and all
     *         values in the two MathMatrix objects are the same, false otherwise
     */
    public boolean equals(Object rightHandSide) {
        /*
         * CS314 Students. The following is standard equals method code. We will learn
         * about in the coming weeks.
         */
        boolean result = false;
        if (rightHandSide != null && this.getClass() == rightHandSide.getClass()) {
            // rightHandSide is a non null MathMatrix
            MathMatrix otherMatrix = (MathMatrix) rightHandSide;
            if (otherMatrix.getNumRows() == getNumRows() && otherMatrix.getNumColumns() == getNumColumns()) {
                result = true;
                for (int i = 0; i < getNumRows(); i++) {
                    for (int j = 0; j < getNumColumns(); j++) {
                        result = getVal(i, j) == otherMatrix.getVal(i, j);
                        if (!result) {
                            return result; // if result == false, stop
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * override toString.
     * 
     * @return a String with all elements of this MathMatrix. Each row is on a
     *         separate line. Spacing based on longest element in this Matrix.
     */
    public String toString() {
        final int MAX_STRING_LENGTH = getLongestNumber() + 1; // use this to determine spaces
        StringBuilder sb = new StringBuilder(); // less operations
        final char PIPE = '|'; // these constants go into the toString()
        final String NEW_LINE = "\n";
        for (int i = 0; i < getNumRows(); i++) {
            sb.append(PIPE); // start line with vertical bar
            for (int j = 0; j < getNumColumns(); j++) {
                final int CURRENT_VALUE = getVal(i, j); // append values
                final int NUM_SPACES = MAX_STRING_LENGTH - ("" + CURRENT_VALUE).length(); // number of spaces dependent
                                                                                          // on length of number's
                                                                                          // string
                for (int currentSpace = 0; currentSpace < NUM_SPACES; currentSpace++) {
                    sb.append(' '); // add spaces
                }
                sb.append(CURRENT_VALUE); // then add the value
            }
            sb.append(PIPE + NEW_LINE); // end line with vertical bar
        }
        return sb.toString();
    }

    // for use in getting the amount of spaces needed
    private int getLongestNumber() {
        int maxStringLength = 0;
        for (int i = 0; i < getNumRows(); i++) {
            for (int j = 0; j < getNumColumns(); j++) {
                maxStringLength = Math.max(maxStringLength, ("" + getVal(i, j)).length());
            }
        }
        return maxStringLength;
    }

    /**
     * Return true if this MathMatrix is upper triangular. To be upper triangular
     * all elements below the main diagonal must be 0.<br>
     * pre: this is a square matrix. getNumRows() == getNumColumns()
     * 
     * @return <tt>true</tt> if this MathMatrix is upper triangular, <tt>false</tt>
     *         otherwise.
     */
    public boolean isUpperTriangular() {
        if (getNumRows() != getNumColumns()) {
            throw new IllegalArgumentException("This is not a square matrix.");
        }
        for (int i = 0; i < getNumRows(); i++) {
            for (int j = i + 1; j < getNumRows(); j++) {
                if (getVal(j, i) != 0) { // if values below diagonal are not zero, stop looking
                    return false;
                }
            }
        }
        return true;
    }

    // method to ensure mat is rectangular
    // pre: mat != null, mat has at least one row
    // return true if all rows in mat have the same
    // number of columns false otherwise.
    public static boolean rectangularMatrix(int[][] mat) {
        if (mat == null || mat.length == 0) {
            throw new IllegalArgumentException(
                    "argument mat may not be null and must " + " have at least one row. mat = " + Arrays.toString(mat));
        }
        boolean isRectangular = true;
        int row = 1;
        final int COLUMNS = mat[0].length;
        while (isRectangular && row < mat.length) {
            isRectangular = (mat[row].length == COLUMNS);
            row++;
        }
        return isRectangular;
    }

}