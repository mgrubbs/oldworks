
/*  Student information for assignment: 
 *
 *  On <MY|OUR> honor, Mark Grubbs and REDACTED, this programming assignment is <MY|OUR> own work
 *  and <I|WE> have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose turnin account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *  Section number:
 *  
 *  Student 2 
 *  UTEID:
 *  email address:
 *  Grader name:
 *  Section number:
 *  
 */

import java.util.Iterator;
import java.util.ArrayList;

/**
 * In this implementation of the ISet interface the elements in the Set are
 * maintained in ascending order.
 * 
 * The data type for E must be a type that implements Comparable.
 * 
 * Students are to implement methods that were not implemented in AbstractSet
 * and override methods that can be done more efficiently. An ArrayList must be
 * used as the internal storage container. For methods involving two set, if
 * that method can be done more efficiently if the other set is also a SortedSet
 * do so.
 */
public class SortedSet<E extends Comparable<? super E>> extends AbstractSet<E> {

    private ArrayList<E> myCon;

    /**
     * create an empty SortedSet
     */
    public SortedSet() {
        myCon = new ArrayList<>();
    }

    /**
     * create a SortedSet out of an unsorted set. <br>
     * 
     * @param other != null
     * this is O(NlogN)
     */
    public SortedSet(ISet<E> other) {
        if (other == null) {
            throw new IllegalArgumentException("Set to be made into sorted set may not be null.");
        }
        UnsortedSet<E> otherSet = new UnsortedSet<E>();
        if (other instanceof UnsortedSet<?>) {
            otherSet = (UnsortedSet<E>) (other);
        }
        for (E item : otherSet) {
            myCon.add(item);
        }
        quickSort(0, myCon.size());
    }

    // adapted from the slides
    // performs a quickSort on the unsorted set
    // has O(NlogN)
    private void quickSort(int start, int stop) {
        if (start < stop) {
            int pivotIndex = getMedian(start, stop, (start - stop) / 2);
            swap(pivotIndex, start);
            E pivot = myCon.get(pivotIndex);
            int j = start;
            for (int i = start + 1; i <= stop; i++) {
                if (myCon.get(i).compareTo(pivot) <= 0) {
                    j++;
                    swap(i, j);
                }
            }
            swap(start, j);
            quickSort(start, j - 1);
            quickSort(j + 1, stop);
        }
    }

    // gets the median between the front, end and middle
    // may reduce T(N) ? ? ? ?
    private int getMedian(int start, int stop, int middle) {
        if (middle >= start && middle < stop) {
            return middle;
        }
        if (middle < start && start >= stop) {
            return start;
        }
        return stop;
    }

    // swaps the elements between two indices in myCon
    private void swap(int firstIndex, int secondIndex) {
        E temp = myCon.get(firstIndex);
        myCon.set(firstIndex, myCon.get(secondIndex));
        myCon.set(secondIndex, temp);
    }

    // checks precon for if the set is empty
    private void checkSizePrecon() {
        if (myCon.size() == 0) {
            throw new IllegalStateException("cannot find min or max of empty set");
        }
    }

    /**
     * Return the smallest element in this SortedSet. <br>
     * pre: size() != 0
     * 
     * @return the smallest element in this SortedSet.
     * O(1)
     */
    public E min() {
        checkSizePrecon();
        return myCon.get(0);
    }

    /**
     * Return the largest element in this SortedSet. <br>
     * pre: size() != 0
     * 
     * @return the largest element in this SortedSet.
     * O(1)
     */
    public E max() {
        checkSizePrecon();
        return myCon.get(size() - 1);
    }

    /**
     * Add an item to this set. <br>
     * item != null
     * 
     * @param item the item to be added to this set. item may not equal null.
     * @return true if this set changed as a result of this operation, false
     *         otherwise.
     * this is O(N)
     */
    public boolean add(E item) {
        preconditionNull(item);
        if (myCon.isEmpty()) {
            myCon.add(item);
            return true;
        }
        int index = findWhereToPut(item);
        if(index == -1) {
            return false;
        }
        myCon.add(index, item);
        return true;
    }

    // does a linear search of where to put the item
    // has O(N)
    private int findWhereToPut(E item) {
        if(myCon.get(size() - 1).compareTo(item) < 0) {
            return size();
        }
        for(int i = 0; i < myCon.size(); i++) {
            int comparison = myCon.get(i).compareTo(item);
            if( comparison > 0) {
                return i;
            } else if (comparison == 0){
                return -1;
            }
        }
        return size();
    }

    // adapted binary search from the slides
    // has O(logN)
    public boolean contains(E item) {
        preconditionNull(item);
        int low = 0;
        int high = myCon.size() - 1;
        while (low <= high) {
            int mid = low + ((high - low) / 2);
            if (myCon.get(mid).equals(item)) {
                return true;
            } else if (myCon.get(mid).compareTo(item) > 0) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return false;
    }

    /**
     * Determine if this set is equal to other.
     * Two sets are equal if they have exactly the same elements.
     * The order of the elements does not matter.
     * <br>pre: none
     * @param other the object to compare to this set 
     * @return true if other is a Set and has the same elements as this set
     * this is O(N)
     */
    public boolean equals(Object otherSet) {
        if (otherSet == null) {
            throw new IllegalArgumentException("otherSet to compare against cannot be null.");
        }
        boolean isEqual = false;
        SortedSet<E> other = new SortedSet<E>();
        if (otherSet instanceof ISet<?>)
            other = castToSortedSet((ISet<E>) (otherSet));
        else
            return false;
        if (otherSet instanceof SortedSet<?>) {
            isEqual = other.size() == size();
            int index = 0;
            while (isEqual && index < size()) {
                isEqual = myCon.get(index).equals(other.myCon.get(index));
                index++;
            }
        }
        return isEqual;
    }

    // checks precon item is not null
    private void preconditionNull(E item) {
        if (item == null) {
            throw new IllegalArgumentException("Set does not deal with null objects.");
        }
    }

    /**
     * A union operation. Add all items of otherSet that are not already present in
     * this set to this set.
     * 
     * @param otherSet != null
     * @return true if this set changed as a result of this operation, false
     *         otherwise.
     * O(N) or O(NlogN) if not sortedSet
     */
    public boolean addAll(ISet<E> otherSet) {
        int oldSize = size();
        merge(otherSet);
        return oldSize != size();
    }

    // performs a merge algorithm
    // creates a new arrayList that the myCon will become
    // adds objects into the mergedSet arrayList in order of whichever one is smaller than the other
    // goes through each set adding elements only once from each set
    // if one set runs out, adds all the remaining elements from the other set 
    // O(N) where N is total elements
    private void merge(ISet<E> otherSet) {
        ArrayList<E> mergedSet = new ArrayList<E>();
        int thisIndex = 0;
        int otherIndex = 0;
        SortedSet<E> other = castToSortedSet(otherSet);
        while (thisIndex < size() || otherIndex < otherSet.size()) {
            if (thisIndex != size() && otherIndex != otherSet.size()) {
                int comparison = myCon.get(thisIndex).compareTo(other.myCon.get(otherIndex));
                if (comparison < 0) {
                    mergedSet.add(myCon.get(thisIndex));
                    thisIndex++;
                } else if (comparison > 0) {
                    mergedSet.add(other.myCon.get(otherIndex));
                    otherIndex++;
                } else {
                    mergedSet.add(myCon.get(thisIndex));
                    thisIndex++;
                    otherIndex++;
                }
            } else if (thisIndex == size()) {
                for (int i = otherIndex; i < other.size(); i++) {
                    mergedSet.add(other.myCon.get(i));
                }
                otherIndex = other.size();
            } else {
                for (int i = thisIndex; i < size(); i++) {
                    mergedSet.add(myCon.get(i));
                }
                thisIndex = size();
            }
        }
        myCon = mergedSet;
    }

    // casts ISet to sortedSet
    // potentially O(NlogN)
    private SortedSet<E> castToSortedSet(ISet<E> otherSet) {
        if (!(otherSet instanceof SortedSet<?>))
            return new SortedSet<E>(otherSet);
        return (SortedSet<E>) (otherSet);
    }

    /**
     * Return an Iterator object for the elements of this set.
     * pre: none
     * @return an Iterator object for the elements of this set
     * this is O(1)
     */
    public Iterator<E> iterator() {
        return myCon.iterator();
    }

    /**
     * Create a new set that is the union of this set and otherSet.
     * <br>pre: otherSet != null
     * <br>post: returns a set that is the union of this set and otherSet.
     * Neither this set or otherSet are altered as a result of this operation.
     * <br> pre: otherSet != null
     * @param otherSet != null
     * @return a set that is the union of this set and otherSet
     * this is O(N)
     */
    public ISet<E> union(ISet<E> otherSet) {
        SortedSet<E> unionSet = new SortedSet<>();
        unionSet.myCon.addAll(myCon);
        unionSet.merge(otherSet);
        return unionSet;
    }
    
    /**
     * Create a new set that is the difference of this set and otherSet. Return an ISet of 
     * elements that are in this Set but not in otherSet. Also called
     * the relative complement. 
     * <br>Example: If ISet A contains [X, Y, Z] and ISet B contains [W, Z] then
     * A.difference(B) would return an ISet with elements [X, Y] while
     * B.difference(A) would return an ISet with elements [W]. 
     * <br>pre: otherSet != null
     * <br>post: returns a set that is the difference of this set and otherSet.
     * Neither this set or otherSet are altered as a result of this operation.
     * <br> pre: otherSet != null
     * @param otherSet != null
     * @return a set that is the difference of this set and otherSet
     * this is O(N)
     */
    public ISet<E> difference(ISet<E> otherSet) {
        SortedSet<E> differenceSet = new SortedSet<>();
        SortedSet<E> other = castToSortedSet(otherSet);
        int thisIndex = 0;
        int otherIndex = 0;
        while(thisIndex < size()) {
            if(otherIndex == other.size()) {
                for(int i = thisIndex; i < size(); i++) {
                    differenceSet.myCon.add(myCon.get(i));
                }
                return differenceSet;
            }
            int comparison = myCon.get(thisIndex).compareTo(other.myCon.get(otherIndex));
            if(comparison < 0) {
                differenceSet.myCon.add(myCon.get(thisIndex));
                thisIndex++;
            } else if(comparison == 0) {
                thisIndex++;
                otherIndex++;
            } else {
                otherIndex++;
            }
        }
        return differenceSet;
    }

    /**
     * Return the number of elements of this set.
     * pre: none
     * @return the number of items in this set
     * this is O(1)
     */
    public int size() {
        return myCon.size();
    }

    /**
     * Determine if all of the elements of otherSet are in this set.
     * <br> pre: otherSet != null
     * @param otherSet != null
     * @return true if this set contains all of the elements in otherSet, 
     * false otherwise.
     * this is O(N)
     */
    public boolean containsAll(ISet<E> otherSet) {
        SortedSet<E> other = castToSortedSet(otherSet);
        if (other.size() > size()) {
            return false;
        }
        boolean containsAll = true;
        int index = 0;
        int otherIndex = 0;
        while (containsAll && index < size() && otherIndex < other.size()) {
            int comparison = myCon.get(index).compareTo(other.myCon.get(otherIndex));
            if (comparison > 0) {
                containsAll = false;
            } else if (comparison < 0) {
                index++;
            } else if (comparison < 0 && index == size() - 1) {
                containsAll = false;
            } else {
                index++;
                otherIndex++;
            }
        }
        return containsAll;
    }

}
