/*  Student information for assignment:  
 *
 *  On <MY|OUR> honor, <Mark Grubbs> and <NAME2), this programming assignment is <MY|OUR> own work
 *  and <I|WE> have not provided this code to any other student.
 *
 *  Number of slip days used:
 *
 *  Student 1 (Student whose turnin account is being used)
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *
 *  Student 2
 *  UTEID:
 *  email address:
 *
 */
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class Compressor implements IHuffConstants {

    // map of codes
    private Map<Integer, String> compressedNodes;
    // data of tree
    private HuffTree data;
    // the frequencies of characters
    private int[] frequencies;
    // original size of file
    private int sizeOriginal;

    
    public Compressor(InputStream in) throws IOException {
        BitInputStream bitIn = new BitInputStream(new BufferedInputStream(in));
        frequencies = new int[IHuffConstants.ALPH_SIZE + 1];
        frequencies[IHuffConstants.ALPH_SIZE] = 1;
        readInBits(frequencies, bitIn);
        data = new HuffTree(frequencies);
        compressedNodes = data.getMapping();
        setOriginalSize();
        bitIn.close();
    }

    // gets the original file size by multiplying each letter by 8
    private void setOriginalSize() {
        for (int i = 0; i < ALPH_SIZE; i++) {
            sizeOriginal += BITS_PER_WORD * frequencies[i];
        }
    }

    // returns the original size of the file
    public int getOriginalSize() {
        return sizeOriginal;
    }

    // gets frequencies of each character in file
    private void readInBits(int[] frequencies, BitInputStream in) throws IOException {
        int indexToIncrement = 0;
        while ((indexToIncrement = in.readBits(IHuffConstants.BITS_PER_WORD)) != -1) {
            frequencies[indexToIncrement]++;
        }
    }

    // returns encoding of a particular input
    public String getBitString(int readInput) {
        return compressedNodes.get(readInput);
    }

    // puts compressed output onto a new file
    // reads file and outputs the codes for each char
    public void outputCompression(BitOutputStream bitOut, BitInputStream bitInput, int header) throws IOException {
        int input = 0;
        writeHeaderFormat(bitOut, header);
        while ((input = bitInput.readBits(IHuffConstants.BITS_PER_WORD)) != -1) {
            outputKey(bitOut, getBitString(input));
        }
        bitOut.writeBits(BITS_PER_WORD, PSEUDO_EOF);
    }

    // writes the header format based on what header is given
    private void writeHeaderFormat(BitOutputStream bitOut, int header) {
        if (header == IHuffConstants.STORE_COUNTS) { // goes through counts and writes them in order
            for (int k = 0; k < IHuffConstants.ALPH_SIZE; k++) {
                bitOut.writeBits(IHuffConstants.BITS_PER_INT, frequencies[k]);
            }
        } else if (header == IHuffConstants.STORE_TREE) { // performs a preorder traversal to write
            int treeSize = data.treeBitSize();
            bitOut.writeBits(IHuffConstants.BITS_PER_INT, treeSize);
            data.headerIsTree(bitOut);
        }
    }

    // outputs individual bits of a bitString
    private void outputKey(BitOutputStream bitOut, String bitString) {
        for (int bit = 0; bit < bitString.length(); bit++) {
            if (bitString.charAt(bit) == '1') {
                bitOut.writeBits(1, 1);
            } else {
                bitOut.writeBits(1, 0);
            }
        }
    }

    // displays the frequencies and their codes along with what they were
    public String displayInformation() {
        StringBuilder frequencyInfo = new StringBuilder();
        frequencyInfo.append("Frequencies and their codes: \n");
        for (int i = 0; i < ALPH_SIZE; i++) {
            if (frequencies[i] > 0)
                frequencyInfo.append(i + " " + getBitString(i) + " " + (char) (i) + " " + frequencies[i] + "\n");
        }
        frequencyInfo
                .append(ALPH_SIZE + " " + getBitString(ALPH_SIZE) + " " + "Pseudo-eof" + " " + frequencies[ALPH_SIZE]);
        return frequencyInfo.toString();
    }

    // calculates the compressedSize
    public int calculateCompressedSize(int header) {
        int totalSize = 0;
        totalSize += BITS_PER_INT + BITS_PER_INT; // MAGIC_NUMBER and headerformat
        if (header == STORE_COUNTS) { // if header was counts format - add total letters possible times bits per int
            totalSize += ALPH_SIZE * BITS_PER_INT;
        } else if (header == STORE_TREE) { // if header was tree format adds the tree size
            totalSize += data.treeBitSize();
        }
        for (int freq = 0; freq < ALPH_SIZE; freq++) { // goes through frequencies greater than one and adds to total
            if (frequencies[freq] > 0)
                totalSize += frequencies[freq] * getBitString(freq).length(); // adds amount of times written
        }
        String pseudoEof = getBitString(PSEUDO_EOF); // adds pseudo eof
        totalSize += pseudoEof.length();
        return totalSize;
    }

}
