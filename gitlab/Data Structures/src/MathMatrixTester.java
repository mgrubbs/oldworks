import java.util.Random;

/*  Student information for assignment:
 *
 *  UTEID: msg2772
 *  email address: siegbalicula@gmail.com
 *  Grader name:
 *  Number of slip days I am using:
 */

/* CS314 Students. Put your experiment results and
 * answers to questions here.
 * experiment1: 
 * The time that it takes to add two n by n matrices where n = 1000 is 3.08 seconds.
 * The time it takes to add two when n = 2000 is 9.41 seconds. The time it takes to add two when n = 4000 is 37.80 seconds
 * When n = 561, the time it takes to perform the experiment is 1.01 seconds. When this number is doubled, the time becomes 3.47 seconds. 
 * When doubled again, the time becomes 11.79 seconds.
 * experiment2:
 * When n = 1000, the time is 517.88 seconds. Using the T(N) of my multiply method, at n = 2000, the time would be 4135.47 seconds. 
 * Using this formula again with n = 4000, this would be 33053.48 seconds. 
 * When n = 200, the time is 1.10 seconds. Doubling this n results in a time of 14.40 seconds. Doubling again results in a time of 235.32 seconds.
 * Questions:
 * 1. If the dimensions of the MathMatrices are doubled again, I expect the time to be 4 times the length of n = 4000.
 * 2. The order is O(N^2). This is based on the doubling of the times from n = 2000 to n = 4000 and the time changes when starting with n = 561.
 * 3. If the dimensions of the MathMatrices are doubled again, I expect the time to be 8 times the amount based on starting with n = 1000.
 * 4. The order is O(N^3) based on the T(N). My times do not support this.
 * 5. The largest possible n x n matrix that can be made before running out of the default memory for my system is a 30019 by 30019 matrix.
 */

/**
 * A class to run tests on the MathMatrix class
 */
public class MathMatrixTester {

    /**
     * main method that runs simple test on the MathMatrix class
     *
     * @param args not used
     */
    public static void main(String[] args) {
        // I used the printTestResult and get2DArray methods by Professor Scott in my tests.
        // CS314 Students. When ready delete the above tests
        // and add your 22 tests here.
        int[][] data1 = new int[10][10];
        int[][] data2 = new int[11][20];
        int[][] e1;
        MathMatrix m1 = new MathMatrix(data1);
        MathMatrix m2 = new MathMatrix(data2);
        e1 = new int[][] { { 13, 13, 13, 13 }, { 13, 13, 13, 13 }, { 13, 13, 13, 13 }, { 13, 13, 13, 13 },
                { 13, 13, 13, 13 }, { 13, 13, 13, 13 }, { 13, 13, 13, 13 }, { 13, 13, 13, 13 }, { 13, 13, 13, 13 },
                { 13, 13, 13, 13 } };
        // tests for getNumRows() and getNumColumns()
        System.out.print("Test number 1 tests getNumRows. ");
        passedTest(getEqualityTest(m1.getNumRows(), e1.length));
        System.out.print("Test number 2 tests getNumRows. ");
        passedTest(!getEqualityTest(m2.getNumColumns(), e1.length));
        System.out.print("Test number 3 tests getNumColumns. ");
        passedTest(getEqualityTest(m1.getNumColumns(), e1.length));
        System.out.print("Test number 4 tests getNumColumns. ");
        passedTest(!getEqualityTest(m1.getNumColumns(), m2.getNumColumns()));
        // tests for getVal()
        m1 = new MathMatrix(10, 10, 13);
        System.out.print("Test number 5 tests getVal. ");
        passedTest(getEqualityTest(m1.getVal(3, 8), e1[1][1]));
        m2 = new MathMatrix(4, 51, 39 / 3);
        System.out.print("Test number 6 tests getVal. ");
        passedTest(getEqualityTest(m1.getVal(2, 5), e1[1][1]));
        // tests for addition and subtraction
        m2 = new MathMatrix(10, 10, 4);
        MathMatrix m3 = m1.add(m2);
        MathMatrix m4 = new MathMatrix(10, 10, 17);
        printTestResult(get2DArray(m3), get2DArray(m4), 7, "add method");
        m2 = new MathMatrix(10, 10, -4);
        m3 = m1.add(m3.add(m2));
        m4 = new MathMatrix(10, 10, 26);
        printTestResult(get2DArray(m3), get2DArray(m4), 8, "add method");
        m2 = new MathMatrix(10, 10, 4);
        m3 = m1.subtract(m2);
        m4 = new MathMatrix(10, 10, 9);
        printTestResult(get2DArray(m3), get2DArray(m4), 9, "subtract method");
        m3 = m3.subtract(m2.subtract(m1.subtract(m2.subtract(m1))));
        m4 = new MathMatrix(10, 10, 27);
        printTestResult(get2DArray(m3), get2DArray(m4), 10, "subtract method");
        // tests for multiplication
        m1 = new MathMatrix(2, 4, 1);
        m2 = new MathMatrix(4, 2, 1);
        m3 = m1.multiply(m2);
        m4 = new MathMatrix(2, 2, 4);
        printTestResult(get2DArray(m3), get2DArray(m4), 11, "multiply method");
        m1 = new MathMatrix(new int[][] { { 5, 2, 2 }, { 2, 5, 2 }, { 2, 2, 5 } });
        m2 = new MathMatrix(3, 1, 1);
        m3 = m1.multiply(m2); // eigenvector
        m4 = m2.getScaledMatrix(9); // eigenvalue = 9
        printTestResult(get2DArray(m3), get2DArray(m4), 12, "multiply method");
        // tests for getScaledMatrix
        m1 = m1.getScaledMatrix(3);
        m3 = m1.multiply(m2);
        m4 = m4.getScaledMatrix(3);
        printTestResult(get2DArray(m3), get2DArray(m4), 13, "getScaledMatrix method");
        m3 = m1.getScaledMatrix(1 / 9);
        m4 = new MathMatrix(3, 3, 0);
        printTestResult(get2DArray(m3), get2DArray(m4), 14, "getScaledMatrix method");
        // tests for getTranspose
        m3 = m1.getTranspose();
        printTestResult(get2DArray(m3), get2DArray(m1), 15, "getTranspose method");
        m3 = m2.getTranspose();
        m4 = new MathMatrix(1, 3, 1);
        printTestResult(get2DArray(m3), get2DArray(m4), 16, "getTranspose method");
        // tests for equals
        data1 = new int[100][100];
        m3 = new MathMatrix(100, 100, 0);
        m4 = new MathMatrix(data1);
        System.out.print("Test number 17 tests equals. ");
        passedTest(m3.equals(m4));
        data1[4][5] = 10;
        m4 = new MathMatrix(data1);
        System.out.print("Test number 18 tests equals. ");
        passedTest(!m3.equals(m4));
        // tests for toString
        m3 = new MathMatrix(1, 3, 4);
        String expected = "| 4 4 4|\n";
        System.out.print("Test number 19 tests toString. ");
        passedTest(m3.toString().equals(expected));
        m3 = new MathMatrix(new int[][] { { 4, 10 }, { 1, 4 }, { 100, 14 } });
        expected = "|   4  10|\n|   1   4|\n| 100  14|\n";
        System.out.print("Test number 20 tests toString. ");
        passedTest(m3.toString().equals(expected));
        // tests for isUpperTriangle
        System.out.print("Test number 21 tests isUpperTriangle. ");
        passedTest(!m1.isUpperTriangular());
        m1 = m1.subtract(new MathMatrix(3, 3, 6));
        System.out.print("Test number 22 tests isUpperTriangle. ");
        passedTest(m1.isUpperTriangular());
        // experiments
//        Stopwatch time = new Stopwatch();
//        int n = 200;
//        MathMatrix mat1 = new MathMatrix(1, 1, 1);
//        MathMatrix mat2 = new MathMatrix(1, 1, 1);
//        for (int i = 0; i < 3; i++) {
//            time.start();
//            Random numGen = new Random();
//            mat1 = new MathMatrix(n, n, numGen.nextInt());
//            mat2 = new MathMatrix(n, n, numGen.nextInt());
//            experiment2(mat1, mat2);
//            time.stop();
//            System.out.println(time.time());
//            n = n * 2;
//        }
//        MathMatrix m = new MathMatrix(30019, 30019, 4);
    }

    
    // tests equality of certain values
    private static boolean getEqualityTest(int data1, int testAgainst) {
        return data1 == testAgainst;
    }

    private static void passedTest(boolean res) {
        if (res) {
            System.out.print("Test passed.\n");
        } else {
            System.out.print("Test failed.\n");
        }
    }

//    private static void experiment1(MathMatrix mat1, MathMatrix mat2) {
//        for (int i = 0; i < 1000; i++) {
//            MathMatrix mat3 = mat1.add(mat2);
//        }
//    }
//
//    private static void experiment2(MathMatrix mat1, MathMatrix mat2) {
//        for (int i = 0; i < 100; i++) {
//            MathMatrix mat3 = mat1.multiply(mat2);
//        }
//    }

    // method that sums elements of mat, may overflow int!
    // pre: mat != null
    private static int sumVals(MathMatrix mat) {
        if (mat == null) {
            throw new IllegalArgumentException("mat may not be null");
        }
        int result = 0;
        final int ROWS = mat.getNumRows();
        final int COLS = mat.getNumColumns();
        for (int r = 0; r < ROWS; r++) {
            for (int c = 0; c < COLS; c++) {
                result += mat.getVal(r, c); // likely to overflow, but can still do simple check
            }
        }
        return result;
    }

    // create a matrix with random values
    // pre: rows > 0, cols > 0, randNumGen != null
    public static MathMatrix createMat(Random randNumGen, int rows, int cols, final int LIMIT) {

        if (randNumGen == null) {
            throw new IllegalArgumentException("randomNumGen variable may no be null");
        } else if (rows <= 0 || cols <= 0) {
            throw new IllegalArgumentException(
                    "rows and columns must be greater than 0. " + "rows: " + rows + ", cols: " + cols);
        }

        int[][] temp = new int[rows][cols];
        final int SUB = LIMIT / 4;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                temp[r][c] = randNumGen.nextInt(LIMIT) - SUB;
            }
        }

        return new MathMatrix(temp);
    }

    private static void printTestResult(int[][] data1, int[][] data2, int testNum, String testingWhat) {
        System.out.print("Test number " + testNum + " tests the " + testingWhat + ". The test ");
        String result = equals(data1, data2) ? "passed" : "failed";
        System.out.println(result);
    }

    // pre: m != null, m is at least 1 by 1 in size
    // return a 2d array of ints the same size as m and with
    // the same elements
    private static int[][] get2DArray(MathMatrix m) {
        // check precondition
        if ((m == null) || (m.getNumRows() == 0) || (m.getNumColumns() == 0)) {
            throw new IllegalArgumentException("Violation of precondition: get2DArray");
        }

        int[][] result = new int[m.getNumRows()][m.getNumColumns()];
        for (int r = 0; r < result.length; r++) {
            for (int c = 0; c < result[0].length; c++) {
                result[r][c] = m.getVal(r, c);
            }
        }
        return result;
    }

    // pre: data1 != null, data2 != null, data1 and data2 are at least 1 by 1
    // matrices
    // data1 and data2 are rectangular matrices
    // post: return true if data1 and data2 are the same size and all elements are
    // the same
    private static boolean equals(int[][] data1, int[][] data2) {
        // check precondition
        if ((data1 == null) || (data1.length == 0) || (data1[0].length == 0) || !rectangularMatrix(data1)
                || (data2 == null) || (data2.length == 0) || (data2[0].length == 0) || !rectangularMatrix(data2)) {
            throw new IllegalArgumentException("Violation of precondition: equals check on 2d arrays of ints");
        }
        boolean result = (data1.length == data2.length) && (data1[0].length == data2[0].length);
        int row = 0;
        while (result && row < data1.length) {
            int col = 0;
            while (result && col < data1[0].length) {
                result = (data1[row][col] == data2[row][col]);
                col++;
            }
            row++;
        }

        return result;
    }

    // method to ensure mat is rectangular
    // pre: mat != null, mat is at least 1 by 1
    private static boolean rectangularMatrix(int[][] mat) {
        if (mat == null || mat.length == 0 || mat[0].length == 0) {
            throw new IllegalArgumentException(
                    "Violation of precondition: " + " Parameter mat may not be null" + " and must be at least 1 by 1");
        }
        return MathMatrix.rectangularMatrix(mat);
    }
}
