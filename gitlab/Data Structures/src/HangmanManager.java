/*  Student information for assignment:
 *
 *  On my honor, Mark Grubbs, this programming assignment is my own work
 *  and I have not provided this code to any other student.
 *
 *  Name: Mark Grubbs
 *  email address: siegbalicula@gmail.com
 *  UTEID: msg2772
 *  Section 5 digit ID: 
 *  Grader name:
 *  Number of slip days used on this assignment:
 */

// add imports as necessary

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

/**
 * Manages the details of EvilHangman. This class keeps tracks of the possible
 * words from a dictionary during rounds of hangman, based on guesses so far.
 *
 */
public class HangmanManager {

    // constant used in creating the keys
    private static final char NOT_GUESSED = '-';
    // instance vars
    private boolean debugOn; // debugging on or off
    private Set<String> dictionary; // the original list of words
    private int guessesLeft; // amount of guesses for user
    private ArrayList<Character> guessedLetters; // guessed letters added over time
    private int currentWordLength; // word length of all active words
    private ArrayList<String> activeWords; // words that follow the hardest or second hardest word family
    private String activePattern; // the current pattern that gives the word family
    private String secretWord; // word randomly chosen among the active words if there are more than one
    private HangmanDifficulty difficulty; // difficulty that changes how the game chooses word families
    private int difficultyChoice; // int that determines when the second hardest word family is chosen
    private int round; // used in conjunction with difficultyChoice

    /**
     * Create a new HangmanManager from the provided set of words and phrases. pre:
     * words != null, words.size() > 0
     * 
     * @param words   A set with the words for this instance of Hangman.
     * @param debugOn true if we should print out debugging to System.out.
     */
    public HangmanManager(Set<String> words, boolean debugOn) {
        if (words == null || words.size() <= 0) {
            throw new IllegalArgumentException("Given set of words cannot be null or empty.");
        }
        this.debugOn = debugOn;
        dictionary = new HashSet<>();
        dictionary.addAll(words);
        activeWords = new ArrayList<>();
    }

    /**
     * Create a new HangmanManager from the provided set of words and phrases.
     * Debugging is off. pre: words != null, words.size() > 0
     * 
     * @param words A set with the words for this instance of Hangman.
     */
    public HangmanManager(Set<String> words) {
        this(words, false);
    }

    /**
     * Get the number of words in this HangmanManager of the given length. pre: none
     * 
     * @param length The given length to check.
     * @return the number of words in the original Dictionary with the given length
     */
    public int numWords(int length) {
        Iterator<String> dictionaryIterator = dictionary.iterator();
        int numWords = 0;
        while (dictionaryIterator.hasNext()) {
            if (dictionaryIterator.next().length() == length) {
                numWords++;
            }
        }
        return numWords;
    }

    /**
     * Get for a new round of Ha ngman. Think of a round as a complete game of
     * Hangman.
     * 
     * @param wordLen    the length of the word to pick this time. numWords(wordLen)
     *                   > 0
     * @param numGuesses the number of wrong guesses before the player loses the
     *                   round. numGuesses >= 1
     * @param diff       The difficulty for this round.
     */
    public void prepForRound(int wordLen, int numGuesses, HangmanDifficulty diff) {
        guessesLeft = numGuesses;
        currentWordLength = wordLen;
        setActiveWords(wordLen);
        guessedLetters = new ArrayList<>();
        activePattern = setInitialPattern();
        difficulty = diff;
        setDifficulty();
        round = 0;
    }

    // sets the amount of rounds until the difficulty affects the list change
    private void setDifficulty() {
        final int ROUNDS_EASY = 2;
        final int ROUNDS_MEDIUM = 4;
        if (difficulty == HangmanDifficulty.EASY) {
            difficultyChoice = ROUNDS_EASY;
        } else if (difficulty == HangmanDifficulty.MEDIUM) {
            difficultyChoice = ROUNDS_MEDIUM;
        } else {
            difficultyChoice = activeWords.size() + 1;
        }
    }

    // initializes activePattern with dashes
    // e.g. "------"
    private String setInitialPattern() {
        StringBuilder pattern = new StringBuilder();
        for (int i = 0; i < currentWordLength; i++) {
            pattern.append(NOT_GUESSED);
        }
        return pattern.toString();
    }

    // used in prepForRound to set all the activeWords to words of length given
    private void setActiveWords(int wordLength) {
        activeWords = new ArrayList<>();
        for (String s : dictionary) {
            if (s.length() == wordLength) {
                activeWords.add(s);
            }
        }
    }

    /**
     * The number of words still possible (live) based on the guesses so far.
     * Guesses will eliminate possible words.
     * 
     * @return the number of words that are still possibilities based on the
     *         original dictionary and the guesses so far.
     */
    public int numWordsCurrent() {
        return activeWords.size();
    }

    /**
     * Get the number of wrong guesses the user has left in this round (game) of
     * Hangman.
     * 
     * @return the number of wrong guesses the user has left in this round (game) of
     *         Hangman.
     */
    public int getGuessesLeft() {
        return guessesLeft;
    }

    /**
     * Return a String that contains the letters the user has guessed so far during
     * this round. The String is in alphabetical order. The String is in the form
     * [let1, let2, let3, ... letN]. For example [a, c, e, s, t, z]
     * 
     * @return a String that contains the letters the user has guessed so far during
     *         this round.
     */
    public String getGuessesMade() {
        if (guessedLetters.size() == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(guessedLetters.get(0));
        for (int i = 1; i < guessedLetters.size(); i++) {
            sb.append(", ");
            sb.append(guessedLetters.get(i));
        }
        sb.append(']');
        return sb.toString();
    }

    /**
     * Check the status of a character.
     * 
     * @param guess The character to check.
     * @return true if guess has been used or guessed this round of Hangman, false
     *         otherwise.
     */
    public boolean alreadyGuessed(char guess) {
        return guessedLetters.contains(guess);
    }

    /**
     * Get the current pattern. The pattern contains '-''s for unrevealed (or
     * guessed) characters and the actual character for "correctly guessed"
     * characters.
     * 
     * @return the current pattern.
     */
    public String getPattern() {
        return activePattern;
    }

    // pre: !alreadyGuessed(ch)
    // post: return a tree map with the resulting patterns and the number of
    // words in each of the new patterns.
    // the return value is for testing and debugging purposes
    /**
     * Update the game status (pattern, wrong guesses, word list), based on the give
     * guess.
     * 
     * @param guess pre: !alreadyGuessed(ch), the current guessed character
     * @return return a tree map with the resulting patterns and the number of words
     *         in each of the new patterns. The return value is for testing and
     *         debugging purposes.
     */
    public TreeMap<String, Integer> makeGuess(char guess) {
        final char ACTUAL_GUESS = (guess + "").toLowerCase().charAt(0);
        if (alreadyGuessed(ACTUAL_GUESS)) {
            throw new IllegalStateException("That letter was already guessed.");
        }
        setGuessed(ACTUAL_GUESS);
        TreeMap<String, ArrayList<String>> patterns = getPatterns(ACTUAL_GUESS);
        String temp = activePattern;
        setActivePattern(patterns);
        if (temp.equals(activePattern)) {
            guessesLeft--;
        }
        ArrayList<DescendingWordPatterns> sortedPatterns = createSortablePatternList(patterns);
        cullActiveWords(patterns, sortedPatterns);
        changeSecretWord();
        return changeTreeMap(patterns);
    }

    // creates a list of the patterns using the DescendingWordPatterns object; this
    // list can be sorted and
    // the compareTo puts the list in descending order of size
    private ArrayList<DescendingWordPatterns> createSortablePatternList(TreeMap<String, ArrayList<String>> patterns) {
        ArrayList<DescendingWordPatterns> sortablePatterns = new ArrayList<>();
        for (String s : patterns.keySet()) {
            sortablePatterns.add(new DescendingWordPatterns(s, patterns.get(s).size()));
        }
        Collections.sort(sortablePatterns);
        return sortablePatterns;
    }

    private TreeMap<String, Integer> changeTreeMap(TreeMap<String, ArrayList<String>> patterns) {
        TreeMap<String, Integer> patternFreq = new TreeMap<String, Integer>();
        for (String s : patterns.keySet()) {
            patternFreq.put(s, patterns.get(s).size());
        }
        return patternFreq;
    }

    // randomly changes the secretWord after each guess
    private void changeSecretWord() {
        if (numWordsCurrent() <= 0) {
            throw new IllegalStateException("No words to choose from.");
        }
        Random chooseWord = new Random();
        int wordIndex = chooseWord.nextInt(activeWords.size());
        secretWord = activeWords.get(wordIndex);
    }

    // sets a letter to true when it is guessed
    private void setGuessed(char guess) {
        guessedLetters.add(guess);
    }

    // uses checks given in the assignment page to determine the activePattern
    private void setActivePattern(TreeMap<String, ArrayList<String>> patterns) {
        activePattern = patterns.firstKey();
        for (String key : patterns.keySet()) {
            if (patterns.get(key).size() > patterns.get(activePattern).size()) {
                activePattern = key;
            } else if (patterns.get(key).size() == patterns.get(activePattern).size()) {
                int patternDifference = comparePatterns(key, activePattern);
                if (patternDifference < 0) {
                    activePattern = key;
                } else if (patternDifference == 0) {
                    if (key.compareTo(activePattern) < 0) {
                        activePattern = key;
                    }
                }
            }
        }
    }

    // used to compare the amount of revealed chars in a pattern
    // returns an integer:
    // > 0 implies that the key has more revealed chars
    // < 0 implies that the activePattern has more
    // = 0 implies that they have an equal amount
    private int comparePatterns(String key, String pattern) {
        int lettersRevealedKey = 0;
        int lettersRevealedPattern = 0;
        for (int i = 0; i < key.length(); i++) {
            if (key.charAt(i) != NOT_GUESSED) {
                lettersRevealedKey++;
            }
            if (key.charAt(i) != NOT_GUESSED) {
                lettersRevealedPattern++;
            }
        }
        return lettersRevealedKey - lettersRevealedPattern;
    }

    // "culls" active words by choosing the ArrayList with the most amount of words
    // if difficulty is not HARD, then the round number changes the ArrayList choice
    // to the second hardest
    private void cullActiveWords(TreeMap<String, ArrayList<String>> patterns,
            ArrayList<DescendingWordPatterns> sortablePatterns) {
        round++;
        if (round % difficultyChoice == 0) {
            if (patterns.size() > 1) {
                activePattern = sortablePatterns.get(1).pattern;
            }
        }
        activeWords = patterns.get(activePattern);
    }

    // generates the patterns of the dictionary based on the activePattern
    // standardly adds pattern to TreeMap if it is not in there and increments
    // the key's value if it is
    private TreeMap<String, ArrayList<String>> getPatterns(char guess) {
        TreeMap<String, ArrayList<String>> patterns = new TreeMap<>();
        String currentPattern = "";
        for (int i = 0; i < activeWords.size(); i++) {
            currentPattern = makePattern(activeWords.get(i), guess);
            if (!patterns.containsKey(currentPattern)) {
                patterns.put(currentPattern, new ArrayList<>());
                patterns.get(currentPattern).add(activeWords.get(i));
            } else {
                patterns.get(currentPattern).add(activeWords.get(i));
            }
        }
        return patterns;
    }

    // generates a pattern that is based on the activePattern
    // pattern is to be added to local TreeMap
    private String makePattern(String word, char guess) {
        StringBuilder pattern = new StringBuilder(activePattern);
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == guess) {
                pattern.setCharAt(i, guess);
            }
        }
        return pattern.toString();
    }

    /**
     * Return the secret word this HangmanManager finally ended up picking for this
     * round. If there are multiple possible words left one is selected at random.
     * <br>
     * pre: numWordsCurrent() > 0
     * 
     * @return return the secret word the manager picked.
     */
    public String getSecretWord() {
        if (numWordsCurrent() <= 0) {
            throw new IllegalStateException("No words to choose from.");
        }
        return secretWord;
    }

    // based from piazza post by Claire @350
    // prints out a debug string if debug is true
    private void debug(String... strings) {
        if (debugOn && strings.length > 0) {
            System.out.print("DEBUGGING: ");
            for (String string : strings) {
                System.out.println(string);
            }
            if (strings.length != 1)
                System.out.println("END DEBUGGING");
        }
    }

    // class for difficulty
    private static class DescendingWordPatterns implements Comparable<DescendingWordPatterns> {

        // each DescendingWordPatterns object has a size and a pattern
        // compareTo is based on the size
        private int size;
        private String pattern;

        // creates a new DWP with patt and sz
        public DescendingWordPatterns(String patt, int sz) {
            pattern = patt;
            size = sz;
        }

        // compareTo compares size of the other DWP to see if the other is bigger than this.DWP
        // causes the compareTo to rank in descending order
        public int compareTo(DescendingWordPatterns other) {
            if (other == null) {
                throw new IllegalArgumentException("other cannot be null.");
            }
            return other.size - size;
        }
    }
}
