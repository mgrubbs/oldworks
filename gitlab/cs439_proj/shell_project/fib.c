#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

const int MAX = 13;

static void doFib(int n, int doPrint);


/*
 * unix_error - unix-style error routine.
 */
inline static void unix_error(char *msg)
{
    fprintf(stdout, "%s: %s\n", msg, strerror(errno));
    exit(1);
}


int main(int argc, char **argv)
{
    int arg;
    int print=1;

    if(argc != 2){
        fprintf(stderr, "Usage: fib <num>\n");
        exit(-1);
    }

    arg = atoi(argv[1]);
    if(arg < 0 || arg > MAX){
        fprintf(stderr, "number must be between 0 and %d\n", MAX);
        exit(-1);
    }

    doFib(arg, print);

    return 0;
}

/* 
 * Recursively compute the specified number. If print is
 * true, print it. Otherwise, provide it to my parent process.
 *
 * NOTE: The solution must be recursive and it must fork
 * a new child for each call. Each process should call
 * doFib() exactly once.
 */
static void doFib(int n, int doPrint)
{
    int wstatus1;
    int wstatus2;
    if (n == 0){
        if(doPrint){
            printf("%d\n", 0);
        }
        exit(0);
    }else if(n <= 2){
        if(doPrint){
            printf("%d\n", 1);
        }
        exit(1);
    }
    else{
        pid_t child1 = fork();
        pid_t child2 = fork();
        if(child1 == 0){
            doFib(n-1, 0); 
        } 
        if(child2 == 0){
            doFib(n-2, 0);
        } 
        else {
            waitpid(child1, &wstatus1, 0);
            wstatus1 = WEXITSTATUS(wstatus1);

            waitpid(child2, &wstatus2, 0);
            wstatus2 = WEXITSTATUS(wstatus2);

            int result = wstatus1 + wstatus2;

            if(doPrint){
                printf("%d\n", result);
            } else {
                exit(result);
            }
        }
    }
    
}


