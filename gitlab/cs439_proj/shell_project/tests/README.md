In any given program specification directory, there exists a specific `tests/`
directory which holds the expected return code, standard output, and standard
error in files called `n/rc`, `n/out`, and `n/err` (respectively) for each
test `n`. The testing framework just starts at `1` and keeps incrementing
tests until it can't find any more or encounters a failure. Thus, adding new
tests is easy; just add the relevant files to the tests directory at the
lowest available number.

The files needed to describe a test number `n` are:
- `info`: A JSON file containing 4 fields:
  - `id`: The ID # of the test (redundant, but useful for catching bugs)
  - `name`: A short name for the test
  - `description`: A short description for the test
  - `rc`: The return code expected from UTCSH
- `out`: The standard output expected from the test
- `err`: The standard error expected from the test
- `run`: How to run the test (which arguments it needs, etc.)
- `pre` (optional): Code to run before the test, to set something up
- `post` (optional): Code to run after the test, to clean something up

Note: the test framework expects you to be in the project root to run tests, 
so the correct command to run the script is `python3 tests/run-tests.py <args>`.

Finally, note that the provided test files do not actually work until running
the test script. This is to solve a problem where running tests on shared
machines could cause tests to fail because another user used the same hardcoded
`/tmp/outputX` path as you. In a real shell, we would solve this by using
variable substitution, but since that's hard to implement, the python script does
it for you. This does mean that you cannot use a directory that is literally
called `$TMPDIR` in the tests, since the scripts will sub it out for you. I think
that's a very reasonable tradeoff to make.
