/*
      utcsh - The UTCS Shell

   <Put your name and CS login ID here>
   Mark Grubbs - mgrubbs@cs.utexas.edu

*/

/* Read the additional functions from util.h. They may be beneficial to you
in the future */
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdbool.h>

/* Global variables */
/* The array for holding shell paths. Can be edited by the functions in util.c*/
char shell_paths[MAX_ENTRIES_IN_SHELLPATH][MAX_CHARS_PER_CMDLINE];
static char prompt[] = "utcsh> "; /* Command line prompt */
static char *default_shell_path[2] = {"/bin", NULL};
/* End Global Variables */

/* Convenience struct for describing a command. Modify this struct as you see
 * fit--add extra members to help you write your code. */
struct Command {
   char **args;         /* Argument array for the command */
   char *outputFile; /* Redirect target for file (NULL means no redirect) */
   int args_total;
};

/* Here are the functions we recommend you implement */

char **tokenize_command_line(char *cmdline);
struct Command *parse_command(char **tokens);
void eval(struct Command **cmd);
int try_exec_builtin(struct Command **cmd);
void exec_external_cmd(struct Command **cmd);
char **get_multiple_commands(char *cmdline);

void print_error(){
   char error_message[30] = "An error has occurred\n";
   int nbytes_written = write(STDERR_FILENO, error_message, strlen(error_message));
   if(nbytes_written != strlen(error_message)){
      exit(2);   // Should almost never happen -- if it does, error is unrecoverable
   }
}

int current_amount_of_commands;

/* Main REPL: read, evaluate, and print. This function should remain relatively
    short: if it grows beyond 60 lines, you're doing too much in main() and
    should try to move some of that work into other functions. */
int main(int argc, char **argv) {
   set_shell_path(default_shell_path);

   /* These two lines are just here to suppress certain warnings. You should
    * delete them when you implement Part 1.4 */   
   if(argc == 2){
      FILE *fileInput;
      fileInput = fopen(argv[1], "r");
      if(fileInput == NULL){
         print_error();
         exit(1);
      }
      stdin = fileInput;
   }else if(argc > 2){
      print_error();
      exit(1);
   }
   while (1) {
      if(argc == 1){
         printf("%s", prompt);
      }
      /* Read */ 
      char *line = malloc(MAX_CHARS_PER_CMD); 
      size_t sizeofline = 0;
      int sizeofargs = getline(&line, &sizeofline, stdin);
      /* Evaluate */
      if(sizeofargs == -1){
         print_error();
         exit(1);
      }
      if(sizeofline > MAX_CHARS_PER_CMDLINE){
         print_error();
         exit(0);
      }
      if(sizeofargs >= 1){
         char **cmds = get_multiple_commands(line);
         int i = 0;
         struct Command **new_command = malloc(MAX_WORDS_PER_CMDLINE);
         while(cmds[i]){
            char **tokens = tokenize_command_line(cmds[i]);
            new_command[i] = parse_command(tokens);
            i++;
         }
         eval(new_command);
         /* Print (optional) */
         free(new_command);
         free(cmds);
      }
      free(line); 
   }
   return 0;
}

/*
This function 
*/
char **get_multiple_commands(char *cmdline) {
	const char delimiter[1] = "&";
   char **separate_cmds = malloc(MAX_WORDS_PER_CMDLINE);
	char *curr_cmd = strtok(cmdline, delimiter);
   int index_cmd = 0;
   while(curr_cmd){
      separate_cmds[index_cmd] = curr_cmd;
      index_cmd++;
      curr_cmd = strtok(NULL, delimiter);
   }
	current_amount_of_commands = index_cmd;
   return separate_cmds;
}

/* NOTE: In the skeleton code, all function bodies below this line are dummy
implementations made to avoid warnings. You should delete them and replace them
with your own implementation. */

/** Turn a command line into tokens with strtok
 *
 * This function turns a command line into an array of arguments, making it
 * much easier to process. First, you should figure out how many arguments you
 * have, then allocate a char** of sufficient size and fill it using strtok()
 */
char **tokenize_command_line(char *cmdline) {
      int num_args = 0;
      if(cmdline[0] && cmdline[0] != ' '){
            num_args++;
      }
      int i = 0;
      bool on_space = false;
      while(cmdline[i]){
            if(cmdline[i] == ' ' && !on_space){
                  num_args++;
                  on_space = true;
            }
            if(cmdline[i] != ' '){
                  on_space = false;
            }
            i++;
      }
      int counter = 0;
      const char delimiter[11] = " \n\t\r\v\f";
      char *curr_arg = strtok(cmdline, delimiter);
      char **tokens = malloc(num_args * MAX_CHARS_PER_CMD);
      while(curr_arg){
      tokens[counter] = curr_arg;
      curr_arg = strtok(NULL, delimiter);
      counter++;
   }
   return tokens;
}

/** Turn tokens into a command.
 *
 * The `struct Command` represents a command to execute. This is the preferred
 * format for storing information about a command, though you are free to change
 * it. This function takes a sequence of tokens and turns them into a struct
 * Command.
 */
struct Command *parse_command(char **tokens) {
   struct Command *commands = malloc(sizeof(struct Command));
   commands->args = tokens;
   int total_args = 0;
   int num_redirect = 0;
   int index = 0;
   while(tokens[total_args]){
      if(strcmp(">", tokens[total_args]) == 0){
         index = total_args;
         num_redirect++;
      }
      total_args++;
   }
   int num_args_after_redirect = total_args - index - 1;
   if(num_redirect == 1 && total_args >= 3 && index == total_args - 2){
      commands->outputFile = tokens[index + 1];
      tokens[index] = NULL;
      total_args -= 2;
   }else if(num_redirect != 0 || (num_redirect > 0 && (num_args_after_redirect > 1 || num_args_after_redirect == 0))){
      print_error();
      exit(0);
   }
   commands->args_total = total_args;
   return commands;
}

/** Evaluate a single command
 *
 * Both built-ins and external commands can be passed to this function--it
 * should work out what the correct type is and take the appropriate action.
 */
void eval(struct Command **cmd) {
   if(!try_exec_builtin(cmd)){
      exec_external_cmd(cmd);
   }
   return;
}



/** Execute built-in commands
 *
 * If the command is a built-in command, immediately execute it and return 1
 * If the command is not a built-in command, do nothing and return 0
 */
int try_exec_builtin(struct Command **cmd) {
   char *exitCommand = "exit";
   char *cdCommand = "cd";
   char *pathCommand = "path";
   int index = 0;
   while(cmd[index]){
      if(cmd[index]->args_total == 0){
      } else if(strcmp(cmd[index]->args[0], exitCommand) == 0){
            if(cmd[index]->args_total == 1){
               exit(0);
            }else{
               print_error();
            }   
      } else if(strcmp(cmd[index]->args[0], cdCommand) == 0){
            if(cmd[index]->args_total == 2){
               if(chdir(cmd[index]->args[1]) == -1){
                  print_error();
               }
            }else{
               print_error();
            }   
      } else if(strcmp(cmd[index]->args[0], pathCommand) == 0){
            set_shell_path(cmd[index]->args);
      } else {
         return 0;
      }
		index++;
   }
   return 1;
}

/** Execute an external command
 *
 * Execute an external command by fork-and-exec. Should also take care of
 * output redirection, if any is requested
 */ 
void exec_external_cmd(struct Command **cmd) {
   int cmd_index = 0;
   while(cmd[cmd_index]){
		if(cmd[cmd_index]->args_total != 0){
      	int child = fork();
      	if(child == 0){
         	char* path = cmd[cmd_index]->args[0];
         	if(path != NULL && !is_absolute_path(path)){
            	char* absolutePath = NULL;
            	int index = 0;
            	while(index <= MAX_ENTRIES_IN_SHELLPATH && absolutePath == NULL && shell_paths[index] != NULL){
               	absolutePath = exe_exists_in_dir(shell_paths[index], path);
               	index++;
            	}
            	path = absolutePath;
         	}
         	if(cmd[cmd_index]->outputFile){
            	int redirect = open(cmd[cmd_index]->outputFile, O_CREAT|O_WRONLY|O_TRUNC, 0666);
            	// from Kevin's Blog post
            	int stdOut = dup2(redirect, STDOUT_FILENO);
            	int stdErr = dup2(redirect, STDERR_FILENO);
            	if(redirect == -1 || stdOut == -1 || stdErr == -1){
               	print_error();
               	exit(1);
            	}
         	}
         	int err = execv(path, cmd[cmd_index]->args);
         	if(err == -1){
            	print_error();
            	exit(1);
         	}
         	exit(0);
      	}
   	}
		cmd_index++;
	}
	wait(NULL);
   return;
}
