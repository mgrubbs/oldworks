import java.awt.Button;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.TextComponent;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import javax.swing.JFrame;

public class PasswordGen {
    public static void main(String[] args) throws FileNotFoundException {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        GraphicsConfiguration gc = gd.getDefaultConfiguration();
        JFrame window = new JFrame("Password Generator", gc);
        window.setSize(new Dimension((int)(screenSize.width * 1.0 / 3), (int)(screenSize.height * 1.0 / 3)));
        window.setLocation((int)(screenSize.width * 1.0 / 3), (int)(screenSize.height * 1.0 / 3));
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Component text = new TextComponent("");
        Component button = new Button("Make Password");
        button.setMaximumSize(new Dimension(10, 10));
        window.add(button);
        window.setVisible(true);
 
        
    }
}