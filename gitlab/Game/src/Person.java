
public class Person {

    private int health, money, damage;
    private String name;
    private boolean isEnemy;

    public Person() {
        health = 100;
        money = 100;
        damage = 10;
        name = (new RandomNameGenerator(7)).toString();
        isEnemy = Math.random() < 0.5;
    }

    public Person(String n) {
        health = 100;
        money = 100;
        damage = 10;
        name = n;
        isEnemy = Math.random() < 0.5;
    }

    public Person(int h, int m, int d, String n, boolean isE) {
        health = h;
        money = m;
        damage = d;
        name = n;
        isEnemy = isE;
    }
    
    public boolean isThisHostile() {
        return isEnemy;
    }

    public String getStats() {
        if (isEnemy)
            return name + " has " + health + " hitpoints, " + money + " money, and deals " + damage
                    + " damage with each attack. They are hostile.";
        return name + " has " + health + " hitpoints, " + money + " money, and deals " + damage
                + " damage with each attack. They are friendly.";
    }

    public String toString() {
        return name;
    }
}
