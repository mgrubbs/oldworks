import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameRunner {

    public static void main(String[] args) throws IOException {
        final int SIZE_OF_WORLD = 20;
        Landscape Palegar = new Landscape(SIZE_OF_WORLD);
        for(int i = 0; i < SIZE_OF_WORLD; i++) {
            for(int j = 0; j < SIZE_OF_WORLD; j++) {
                final int NAME_LENGTH = (int)(Math.random() * 7) + 2;
                final String NAME = (new RandomNameGenerator(NAME_LENGTH)).toString();
                Palegar.addPerson(i, j, new Person(NAME));
            }
        }
        DrawingPanel drawingPanel = new DrawingPanel(1600, 900);
        Graphics g = drawingPanel.getGraphics();
        BufferedImage img = ImageIO.read(new File("GenericSnake.png"));
        //g.drawImage(img, 40, 40, drawingPanel);
        int currX = 0;
        int currY = 0;
        Person[][] world = Palegar.expressWorld();
        Font font = new Font("Racism Destroyed", 41, 20);
        g.setFont(font);
        Color Enemy = new Color(190, 70, 20);
        Color Friendly = new Color(10, 190, 100);
        for(Person[] row : world) {
            for(Person column : row) {
                if(column.isThisHostile()) {
                    g.setColor(Enemy);
                } else {
                    g.setColor(Friendly);
                }
                g.drawString("" + column.toString().charAt(0), currX, currY);
                currX += 20;
            }
            currY += 20;
            currX = 0;
        }
    }
}
