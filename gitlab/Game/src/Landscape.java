
public class Landscape {
    
    private Person[][] twoDWorld;
    
    public Landscape() {
        twoDWorld = new Person[9][9];
    }
    
    public Landscape(int i) {
        twoDWorld = new Person[i][i];
    }
    
    public Landscape(int x, int y) {
        twoDWorld = new Person[x][y];
    }
    
    public void addPerson(int x, int y, Person p) {
        twoDWorld[x][y] = p;
    }
    
    public Person[][] expressWorld(){
        return twoDWorld;
    }
    
    public String toString() {
        String res = "";
        for(int row = 0; row < twoDWorld.length; row++) {
            for(int col = 0; col < twoDWorld[0].length; col++) {
                res = res + twoDWorld[row][col].toString().substring(0,1) + " ";
            }
            res = res + "\n";
        }
        return res;
    }
}
