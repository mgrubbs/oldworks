import java.util.Random;

public class RandomNameGenerator {

    private String rngName;
    private boolean isPass;
    private final char[] ALPHABET = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    private final char[] VOWELS = { 'A', 'E', 'I', 'O', 'U' };
    private final int[] NUMBERS = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    private final char[] SPECIAL = { '*', '%', '$', '!', '#', '&' };

    public RandomNameGenerator() {
        String genName = "";
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                int charChoice = (int) (Math.random() * 26);
                genName = genName + ALPHABET[charChoice];
            } else {
                int charChoice = (int) (Math.random() * 5);
                genName = genName + VOWELS[charChoice];
            }
        }
        rngName = genName;
    }

    public RandomNameGenerator(int n) {
        String genName = "";
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                int charChoice = (int) (Math.random() * 26);
                genName = genName + ALPHABET[charChoice];
            } else {
                int charChoice = (int) (Math.random() * 5);
                genName = genName + VOWELS[charChoice];
            }
        }
        rngName = genName;
    }

    public RandomNameGenerator(boolean isPassword, int length) {
        String genName = "";
        isPass = isPassword;
        Random numGen = new Random();
        for (int i = 0; i < length; i++) {
            int choice = numGen.nextInt(42);
            boolean isLower = numGen.nextInt(2) == 1;
            if (choice < 26 && isLower) {
                genName += ("" + ALPHABET[choice]).toLowerCase();
            } else if (choice < 26 && !isLower) {
                genName += ALPHABET[choice];
            } else if (choice >= 26 && choice < 36) {
                genName += "" + NUMBERS[choice - 26];
            } else {
                genName += "" + SPECIAL[choice - 36];
            }
        }
        rngName = genName;
    }

    public int getLength() {
        return rngName.length();
    }

    public char getMiddleChar() {
        return rngName.charAt(rngName.length() / 2);
    }

    public String toString() {
        if (isPass) {
            return rngName;
        }
        return rngName.substring(0, 1) + rngName.substring(1).toLowerCase();
    }
}
