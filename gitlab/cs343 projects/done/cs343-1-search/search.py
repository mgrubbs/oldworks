# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()

class StateWithPath:
    def __init__(self, position, cost):
        self.position = position
        self.list = []
        self.cost = cost

    def addToPathway(self, action):
        self.list.append(action)

def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

"""
Problems 1, 2, 3, 4 are all similarly implemented by using the pseudocode provided. They all 
utilize a piece of code that prevents the addition of the first state to the fringe due to 
having no action or cost. Other than problem 1, the way that the actions are stored is in 
each state's second argument as a list. I didn't realize this in problem 1, so instead I
have a complicated backtracking method that keeps one copy of the actions already taken 
and going back when a dead end is found.
"""

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    actions = []
    node = problem.getStartState()
    if(problem.isGoalState(node)):
        return actions
    explored = set()
    explored.add(node)
    fringe = util.Stack() # puts deeper ones as next to be explored
    numBranches = []
    startBranches = 0
    for state in problem.getSuccessors(node):
        startBranches = startBranches + 1
        fringe.push(state)
    numBranches.append(startBranches)
    while not fringe.isEmpty():
        node = fringe.pop()
        actions.append(node[1])
        if not node in explored:
            explored.add(node[0])
            countAdded = 0
            for state in problem.getSuccessors(node[0]):
                if not state[0] in explored:
                    if problem.isGoalState(state[0]):
                        actions.append(state[1])
                        return actions
                    fringe.push(state)
                    countAdded = countAdded + 1
            numBranches.append(countAdded)
            if countAdded == 0:
                while numBranches[-1] < 2 and actions:
                    numBranches.pop()
                    actions.pop()
                numBranches[-1] = numBranches[-1] - 1
    return None

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    actions = []
    node = problem.getStartState()
    if problem.isGoalState(node):
        return actions
    fringe = util.Queue() # level by level exploration
    explored = set()
    explored.add(node)
    for state in problem.getSuccessors(node):
        firstChild = ((state[0], [], state[2]))
        firstChild[1].append(state[1])
        fringe.push(firstChild)
    while not fringe.isEmpty():
        node = fringe.pop()
        explored.add(node[0])
        if problem.isGoalState(node[0]):
            return node[1]
        for state in problem.getSuccessors(node[0]):
            li_copy = node[1][:]
            child = ((state[0], li_copy, state[2]))
            child[1].append(state[1])
            if not child[0] in explored and not isInList(fringe.list, child):
                fringe.push(child)
    return None

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    actions = []
    node = problem.getStartState()
    if problem.isGoalState(node):
        return actions
    fringe = util.PriorityQueue() # priority based on cost to go from node to child
    explored = set()
    explored.add(node)
    for state in problem.getSuccessors(node):
        firstChild = ((state[0], [], state[2]))
        firstChild[1].append(state[1])
        fringe.push(firstChild, firstChild[2])
    while not fringe.isEmpty():
        node = fringe.pop()
        explored.add(node[0])
        if problem.isGoalState(node[0]):
            return node[1]
        for state in problem.getSuccessors(node[0]):
            li_copy = node[1][:]
            child = ((state[0], li_copy, node[2] + state[2]))
            child[1].append(state[1])
            if not child[0] in explored:
                fringe.update(child, child[2])
    return None
    

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    actions = []
    node = problem.getStartState()
    if problem.isGoalState(node):
        return actions
    fringe = util.PriorityQueue() # use heuristic and cost to choose who goes first
    explored = set()
    explored.add(node)
    for state in problem.getSuccessors(node):
        firstChild = ((state[0], [], state[2] + heuristic(state[0], problem))) # don't forget heuristic
        firstChild[1].append(state[1])
        fringe.push(firstChild, firstChild[2])
    while not fringe.isEmpty():
        node = fringe.pop()
        explored.add(node[0])
        if problem.isGoalState(node[0]):
            return node[1]
        for state in problem.getSuccessors(node[0]):
            li_copy = node[1][:]
            # don't forget other heuristic
            child = ((state[0], li_copy, node[2] + state[2] + heuristic(state[0], problem) - heuristic(node[0],problem)))
            child[1].append(state[1])
            if not child[0] in explored: 
                fringe.update(child, child[2])
    return None

# checks state's position
def isInList(list, state):
    for state1 in list:
        if state1[0] == state[0]:
            return True
    return False


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
