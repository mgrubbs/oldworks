Name: Mark Grubbs UTEID: msg2772
Description: 
I did the first four problems by simply following the pseudocodes given and adding tweaks to ensure that only certain 
nodes get expanded. To get the actual actions to be returned, I put the actions in the states and returned the state's 
actions it had when a goal state was reached. I did this in all of them but the first one, where I found another way that
was far more complicated. For the Four Corners Problem, a piece of information I added to each state was the corners it 
already passed through. The difficulty in this was that if a single list of corners that were passed through was used, it 
caused Pacman to believe that it already reached all of the corners. Giving each state its own list fixed this. For the 
heuristic, I made the heuristic be the max of the manhattan distances to each corner. I used the min at first, but that 
was less efficient, perhaps for underestimating too much. For problem 7, I used the same style of heuristic as problem 6. 
Problem 8 simply was checking that a state was a state with a dot in it as the goal state and using UCS to greedily find
a path.