Mark Grubbs
msg2772

For Question 1, I mostly used the slides to figure out how to implement value iteration. 
It was a big revelation to realize that a new set of values should be made with each iteration.

For Question 2, with no noise, the highest value path is easily chosen.

For Question 3, I manipulated variables until they caused the correct path. Finding out that 
noise could not be 0.0 was helpful.

For Question 4, I again used the slides/textbook.

For Question 5, I already had this implemented when I did question 4. The hint gives how to do
it.

For Question 6, I found out that this was not possible due to no manipulation of noise.

For Question 7, I didn't do anything.

For Question 8, I applied the given formulas.