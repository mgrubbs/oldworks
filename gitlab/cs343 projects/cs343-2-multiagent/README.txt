Mark Grubbs
msg2772

For Questions 2, 3, and 4, I utilized the pseudocode given to create the code for minimax, alpha beta search
and expectimax. Question 1 I completed by simply using the manhattan distance to act as a main choice for a 
particular position, but this value would not be examined in the case that a dot is right next to Pacman, or
at highest priority, a move would result in a ghost being able to kill Pacman. I just used the score of the 
game for question 5. I ran out of time.