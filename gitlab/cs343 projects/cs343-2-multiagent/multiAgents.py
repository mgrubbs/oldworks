# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
        "*** YOUR CODE HERE ***"
        successorScore = 0
        for ghost in newGhostStates:
          if manhattanDistance(ghost.getPosition(), newPos) <= 2:
            return -1000
        if currentGameState.getFood()[newPos[0]][newPos[1]] or self.checkPellet(currentGameState.getCapsules(), newPos):
          successorScore += 10
        else:
          foodDist = self.closestFood(currentGameState.getFood(), newPos)
          successorScore += 1.0 / foodDist
        return successorScore
    
    def closestFood(self, newFood, newPos):
      import sys
      closestPos = sys.maxint
      xPos = 0
      yPos = 0
      for x in newFood:
        for y in x:
          if y:
            xyPos = [xPos, yPos]
            md = manhattanDistance(newPos, xyPos)
            closestPos = min(closestPos, md)
          yPos += 1
        yPos = 0
        xPos += 1
      return closestPos

    def checkPellet(self, pelletLoc, pos):
      for loc in pelletLoc:
        if pos == loc:
          return True
      return False

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)
        self.idealAction = 0
        self.idealValue = 0

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        import sys
        maxedMin = -sys.maxint - 1
        chosenAction = gameState.getLegalActions(0)[0]
        for action in gameState.getLegalActions(0):
          minimization = self.minValue(gameState.generateSuccessor(0, action), 0, gameState.getNumAgents() - 1, 1)
          if minimization > maxedMin:
            maxedMin = minimization
            chosenAction = action
        self.idealValue = maxedMin
        return chosenAction

    
    def maxValue(self, state, ply):
      import sys
      if state.isLose() or state.isWin() or ply == self.depth:
        return self.evaluationFunction(state)
      v = -sys.maxint - 1
      for action in state.getLegalActions(0):
        v = max(v, self.minValue(state.generateSuccessor(0, action), ply, state.getNumAgents() - 1, 1))
      return v

    def minValue(self, state, ply, ghostsToMove, ghostIndex):
      import sys
      if state.isLose() or state.isWin() or ply == self.depth:
        return self.evaluationFunction(state)
      v = sys.maxint
      for action in state.getLegalActions(ghostIndex):
        if ghostsToMove > 1:
          v = min(v, self.minValue(state.generateSuccessor(ghostIndex, action), ply, ghostsToMove - 1, ghostIndex + 1))
        else:
          v = min(v, self.maxValue(state.generateSuccessor(ghostIndex, action), ply + 1))
      return v




class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
      """
        Returns the minimax action using self.depth and self.evaluationFunction
      """
      "*** YOUR CODE HERE ***"
      import sys
      #for action in gameState.getLegalActions(0):
      self.idealValue = self.maxValue(gameState, -sys.maxint - 1, sys.maxint, 0)
      return self.idealAction
      
      
    def maxValue(self, state, alpha, beta, ply):
      import sys
      if state.isLose() or state.isWin() or ply == self.depth:
        return self.evaluationFunction(state)
      newAlpha = alpha
      v = -sys.maxint - 1
      newV = -sys.maxint - 1
      for action in state.getLegalActions(0):
        v = max(v, self.minValue(state.generateSuccessor(0, action), newAlpha, beta, ply, state.getNumAgents() - 1, 1))
        if v > beta:
          return v
        newAlpha = max(newAlpha, v)
        if ply == 0 and newV < v:
          self.idealAction = action
          newV = v
      return v
    
    def minValue(self, state, alpha, beta, ply, ghostsToMove, ghostIndex):
      import sys
      if state.isLose() or state.isWin() or ply == self.depth:
        return self.evaluationFunction(state)
      newBeta = beta
      v = sys.maxint
      for action in state.getLegalActions(ghostIndex):
        if ghostsToMove >  1:
          v = min(v, self.minValue(state.generateSuccessor(ghostIndex, action), alpha, newBeta, ply, ghostsToMove - 1, ghostIndex + 1))
        else:
          v = min(v, self.maxValue(state.generateSuccessor(ghostIndex, action), alpha, newBeta, ply + 1))
        if v < alpha:
          return v
        newBeta = min(newBeta, v)
      return v


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        self.idealValue = self.value(gameState, 0, True, 0, 0)
        
        return self.idealAction
        

    def value(self, state, ply, nextIsMax, ghostsToMove, ghostIndex):
      if state.isLose() or state.isWin() or ply == self.depth:
        return self.evaluationFunction(state)
      if nextIsMax:
        return self.maxValue(state, ply)
      else:
        return self.expValue(state, ply, ghostsToMove, ghostIndex)

    def maxValue(self, state, ply):
      import sys
      v = -sys.maxint - 1
      firstMax = -sys.maxint - 1
      for action in state.getLegalActions(0):
        v = max(v, self.value(state.generateSuccessor(0, action), ply, False, state.getNumAgents() - 1, 1))
        if ply == 0 and firstMax < v:
          firstMax = v
          self.idealAction = action
      return v

    def expValue(self, state, ply, ghostsToMove, ghostIndex):
      probability = 1.0 / len(state.getLegalActions(ghostIndex))
      v = 0
      for action in state.getLegalActions(ghostIndex):
        if ghostsToMove > 1:
          v += probability * self.value(state.generateSuccessor(ghostIndex, action), ply, False, ghostsToMove - 1, ghostIndex + 1)
        else: 
          v += probability * self.value(state.generateSuccessor(ghostIndex, action), ply + 1, True, 0, 0)
      return v

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: ran out of time; getScore succeeds 8 / 10 times
    """
    "*** YOUR CODE HERE ***"
    return currentGameState.getScore()

# Abbreviation
better = betterEvaluationFunction

