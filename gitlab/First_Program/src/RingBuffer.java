import java.util.NoSuchElementException;

/**
* CS312 Assignment 11.
*
* On MY honor, Mark Grubbs, this programming assignment is MY own work
* and I have not provided this code to any other student.
*
* Student name: Mark Grubbs
* UTEID: msg2772
* email address: siegbalicula@gmail.com
* Grader name: Fatima
* Number of slip days used on this assignment:
*
*/

public class RingBuffer {

    // each of these instance variables work how they sound
    // i.e. elements is the elements of the RingBuffer, capacity is the max capacity
    // of the RingBuffer, latestElement is the latest Element added to the
    // RingBuffer, firstElement is the first element added to the RingBuffer, and
    // size is the amount of elements actually in the buffer.
    private double[] elements;
    private int capacity;
    private int latestElement;
    private int firstElement;
    private int size;

    // initializes elements to the capacity given and the capacity to the capacity
    // given
    public RingBuffer(int capacity) {
        this.capacity = capacity;
        elements = new double[this.capacity];
    }

    // returns size
    public int size() {
        return size;
    }

    // returns whether or not the RingBuffer is empty
    public boolean isEmpty() {
        return size() == 0;
    }

    // returns whether or not the RingBuffer is full
    public boolean isFull() {
        return size() == capacity;
    }

    // places element given at the latestElement spot
    // if the RingBuffer is full, throws an exception
    // size and latestElement are incremented accordingly, with latestElement
    // wrapping back around if it needs to (latestElement == capacity)
    public void enqueue(double x) {
        if (isFull()) {
            throw new IllegalStateException();
        }
        elements[latestElement] = x;
        latestElement++;
        size++;
        if (latestElement == capacity) {
            latestElement = 0;
        }
    }

    // removes the first element that was placed in the RingBuffer and returns it.
    // decrements size and increments firstElement, wrapping firstElement back to
    // zero if it equals the capacity
    // throws exception if the RingBuffer is empty
    // first element changes as things are removed
    public double dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        double numberToReturn = elements[firstElement];
        elements[firstElement] = 0.0;
        firstElement++;
        if (firstElement == capacity) {
            firstElement = 0;
        }
        size--;
        return numberToReturn;
    }

    // returns the firstElement placed in the RingBuffer without removing it
    // throws exception if the RingBuffer is empty
    public double peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return elements[firstElement];
    }

    // case of empty RingBuffer returns []
    // any other case, theRingBuffer has elements concatenated to it from
    // firstElement to latestElement
    // front keeps track of the current first element
    // this is a fence post problem, so the end has an additional element and ends
    // with ]
    public String toString() {
        if (size == 0) {
            return "[]";
        }
        String theRingBuffer = "[";
        int counter = 0;
        int front = firstElement;
        while (counter < size - 1) {
            theRingBuffer += elements[front] + ", ";
            counter++;
            front++;
            if (front == capacity) {
                front = 0;
            }
        }
        theRingBuffer += elements[front] + "]";
        return theRingBuffer;
    }

}
