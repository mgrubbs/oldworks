import java.awt.Color;

/**
* CS312 Assignment 11.
*
* On MY honor, Mark Grubbs, this programming assignment is MY own work
* and I have not provided this code to any other student.
*
* Student name: Mark Grubbs
* UTEID: msg2772
* email address: siegbalicula@gmail.com
* Grader name: Fatima
* Number of slip days used on this assignment:
*
*/



public class Ant extends Critter {

    private Direction northOrSouth;
    private int currentStep;

    // currentStep starting at zero (default) is fine
    // northOrSouth only needs to be determined since ant only moves east and north
    // or south
    public Ant(boolean walkSouth) {
        if (walkSouth) {
            northOrSouth = Direction.SOUTH;
        } else {
            northOrSouth = Direction.NORTH;
        }
    }

    // always eats
    public boolean eat() {
        return true;
    }

    // is always the color red
    public Color getColor() {
        return Color.RED;
    }

    // The direction that the ant moves changes depending on what step it is on. In
    // either case, the steps taken in the game is incremented.
    public Direction getMove() {
        if (currentStep % 2 == 0) {
            currentStep++;
            return northOrSouth;
        }
        currentStep++;
        return Direction.EAST;
    }

    // the ant always scratches
    public Attack fight(String oppon) {
        return Attack.SCRATCH;
    }

    // the ant is printed out as a percent symbol
    public String toString() {
        return "%";
    }
}
