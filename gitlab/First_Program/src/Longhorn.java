import java.awt.Color;
import java.util.HashMap;

/**
 * CS312 Assignment 11.
 *
 * On MY honor, Mark Grubbs, this programming assignment is MY own work and I
 * have not provided this code to any other student.
 *
 * Student name: Mark Grubbs UTEID: msg2772 email address:
 * siegbalicula@gmail.com Grader name: Fatima Number of slip days used on this
 * assignment:
 *
 */

// My Longhorns have a map of the critters that maps their String to the attack that defeats them. The map adapts over time.
// Additionally, it is difficult to detect the Longhorn since the Longhorn repeatedly changes string every turn.
// The Longhorn does not eat until turn 300 to prevent the possibility of being defeated early. 
// This allows the Longhorn to eat later in the game when less critters are in the world.
// Longhorns have a random movement.

public class Longhorn extends Critter {

    // each longhorn has a map that is the same among all longhorns
    // it defaults to first containing the original animals programmed by me
    // current turn determines when the longhorn will eat
    private static HashMap<Character, Attack> enemies;
    private int currentTurn;

    // The hashmap is initialized with the critters programmed by me and has a
    // "detection limit" of 30.
    public Longhorn() {
        enemies = new HashMap<Character, Attack>(30);
        enemies.put('%', Attack.ROAR);
        enemies.put('V', Attack.SCRATCH);
        enemies.put('>', Attack.SCRATCH);
        enemies.put('<', Attack.SCRATCH);
        enemies.put('^', Attack.SCRATCH);
        for (int i = 1; i <= 9; i++)
            enemies.put((i + "").charAt(0), Attack.ROAR);
        enemies.put((0 + "").charAt(0), Attack.SCRATCH);
    }

    // don't eat until turn 300
    public boolean eat() {
        return currentTurn > 300;
    }

    // fight checks the map for the opponent and returns whatever attack will win
    // otherwise, the attack is random.
    public Attack fight(String oppo) {
        if (enemies.containsKey(oppo.charAt(0))) {
            return enemies.get(oppo.charAt(0));
        }
        final int RANDOM_ATTACK = (int) (Math.random() * 3);
        Attack[] allTheAttacks = { Attack.POUNCE, Attack.ROAR, Attack.SCRATCH };
        return allTheAttacks[RANDOM_ATTACK];
    }

    // Movement is completely random
    public Direction getMove() {
        currentTurn++;
        final int RANDOM_DIRECTION = (int) (Math.random() * 4);
        Direction[] allTheDirections = { Direction.NORTH, Direction.SOUTH, Direction.EAST, Direction.WEST };
        return allTheDirections[RANDOM_DIRECTION];
    }

    // color is orange
    public Color getColor() {
        return Color.ORANGE;
    }

    // toString is random
    public String toString() {
        final int CURR_SHAPE = (int) (Math.random() * 96) + 32;
        return (char) (CURR_SHAPE) + "";
    }

    // when the longhorn wins, the map is updated
    public void win() {
        String currentOpponent = super.getNeighbor(Direction.CENTER);
        if (fight(currentOpponent) == Attack.POUNCE) {
            enemies.put(currentOpponent.charAt(0), Attack.POUNCE);
        } else if (fight(currentOpponent) == Attack.SCRATCH) {
            enemies.put(currentOpponent.charAt(0), Attack.SCRATCH);
        } else {
            enemies.put(currentOpponent.charAt(0), Attack.ROAR);
        }
    }
}
