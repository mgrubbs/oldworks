import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;

/**
 * CS312 Assignment 3.
 *
 * Replace <NAME> with your name, stating on your honor you completed this
 * assignment on your own and that you didn't share your code with other
 * students.
 * 
 * On my honor, Mark Grubbs, this programming assignment is my own work and I
 * have not shared my solution with any other student in the class.
 *
 * A program to print out various scintillation grids and a student designed
 * drawing.
 *
 * email address: siegbalicula@gmail.com UTEID: msg2772 Number of slip days I am
 * using on this assignment:
 */

public class ScintillationGrid {

    // Main method that creates the DrawingPanel with scintillation grids.
    // Restricted to chapters 1 - 3 of Building Java Programs
    public static void main(String[] args) {
        /*
         * In the final version of the program DO NOT call method drawingOne from main
         * or anywhere else in the program
         */
        final int WIDTH = 1440;
        final int HEIGHT = 1440;
        DrawingPanel dp = new DrawingPanel(WIDTH, HEIGHT);
        Graphics g = dp.getGraphics();
        // CS312 Students add you four methods calls to draw the four
        // required scintillation grids here.
        //drawingOne();
        //g.setColor(Color.CYAN); // fills in background
        //g.fillRect(0, 0, WIDTH, HEIGHT);
        //drawGrid(g, 0, 0, 348, 3, 75);
        //drawGrid(g, 400, 50, 422, 6, 50);
        //drawGrid(g, 50, 400, 220, 1, 100);
        //drawGrid(g, 500, 500, 148, 7, 15);
        // do not alter this line of codes
        //saveDrawingPanel(dp, "grid.png");
        drawGrid(g, 0, 0, 1440, 20, 50);
    }

    // method for the student designed drawing
    // NOT restricted to chapters 1 - 3 of Building Java Programs
    // DO NOT ADD ANY PARAMETERS TO THIS METHOD!!!
    public static void drawingOne() {
        // Comment from Programmer: method Piece by Piece draws an abomination
        // DO NOT ADD ANY PARAMETERS TO THIS METHOD!!!

        // CS312 Students, you may increase the size of the drawing panel if
        // doing a non standard drawing.
        final int WIDTH = 498;
        final int HEIGHT = 800;
        DrawingPanel dp = new DrawingPanel(WIDTH, HEIGHT);
        // CS312 Students, add your code, including method calls here
        Graphics g = dp.getGraphics();
        final Color MAHOGANY = new Color(100, 19, 19);
        final Color GOLD = new Color(85, 68, 60);
        final Color LIGHT_BLUE = new Color(62, 91, 123);
        final Color SKIN = new Color(184, 125, 107); // Each color is used
        final Color SHADOW = new Color(138, 109, 91);
        final Color HAIR = new Color(19, 15, 12);
        final Color MOUTH = new Color(92, 59, 50);
        g.setColor(Color.LIGHT_GRAY); // Each fill call was done by pixel
        g.fillRect(0, 0, 498, 800);
        g.setColor(LIGHT_BLUE);
        g.fillRect(174, 422, 30, 200);
        g.fillRect(203, 376, 15, 65);
        g.fillRect(208, 399, 15, 50);
        g.fillRect(213, 407, 15, 30);
        g.fillRect(143, 476, 50, 146);
        g.fillRect(218, 421, 64, 106);
        g.fillRect(227, 376, 86, 50);
        g.setColor(Color.BLACK);
        g.fillRect(40, 440, 56, 360);
        g.fillRect(96, 420, 55, 380);
        g.fillRect(151, 420, 30, 73);
        g.fillRect(151, 493, 10, 30);
        g.fillRect(171, 420, 30, 20);
        g.fillRect(171, 440, 20, 20);
        g.fillRect(179, 525, 190, 275);
        g.fillRect(137, 606, 50, 194);
        g.fillRect(235, 472, 134, 219);
        g.fillRect(248, 421, 121, 210);
        g.fillRect(293, 396, 131, 400);
        g.setColor(SHADOW);
        g.fillOval(207, 320, 82, 97);
        g.setColor(SKIN);
        g.fillOval(180, 195, 133, 199);
        g.fillRoundRect(180, 195, 133, 99, 31, 50);
        g.fillOval(300, 272, 22, 50);
        g.fillOval(169, 272, 22, 50);
        g.setColor(HAIR);
        g.fillArc(179, 184, 134, 50, 15, 150);
        g.setColor(Color.WHITE);
        g.fillRect(195, 260, 42, 20);
        g.fillRect(258, 260, 42, 20);
        g.setColor(Color.BLUE);
        g.fillRect(216, 260, 21, 20);
        g.fillRect(258, 260, 21, 20);
        g.setColor(MOUTH);
        g.fillRect(216, 300, 63, 20);
        g.setColor(MAHOGANY);
        final int STARTING_X_ONE = 149; // for loops draw the tie with line
        final int ENDING_X_ONE = 220;
        int yOne = 593;
        for (int i = STARTING_X_ONE; i <= ENDING_X_ONE; i += 2) {
            g.drawLine(i, yOne, i, yOne - 30);
            yOne -= 1;
        }
        yOne = 593;
        for (int i = STARTING_X_ONE - 1; i <= ENDING_X_ONE - 1; i += 2) {
            g.drawLine(i, yOne, i, yOne - 30);
            yOne -= 1;
        }
        final int STARTING_X_TWO = 169;
        final int ENDING_X_TWO = 222;
        int yTwo = 517;
        for (int i = STARTING_X_TWO; i <= ENDING_X_TWO; i += 2) {
            g.drawLine(i, yTwo, i, yTwo - 20);
            yTwo -= 1;
        }
        yTwo = 517;
        for (int i = STARTING_X_TWO - 1; i <= ENDING_X_TWO - 1; i += 2) {
            g.drawLine(i, yTwo, i, yTwo - 20);
            yTwo -= 1;
        }
        final int STARTING_X_THREE = 144;
        final int ENDING_X_THREE = 168;
        int yThree = 688;
        for (int i = STARTING_X_THREE; i <= ENDING_X_THREE; i += 2) {
            g.drawLine(i, yThree, i, yThree - 35);
            yThree -= 1;
        }
        yThree = 688;
        for (int i = STARTING_X_THREE - 1; i <= ENDING_X_THREE - 1; i += 2) {
            g.drawLine(i, yThree, i, yThree - 35);
            yThree -= 1;
        }
        g.setColor(GOLD);
        final int STARTING_X_FOUR = 158;
        final int ENDING_X_FOUR = 220;
        int yFour = 558;
        for (int i = STARTING_X_FOUR; i <= ENDING_X_FOUR; i += 2) {
            g.drawLine(i, yFour, i, yFour - 35);
            yFour -= 1;
        }
        g.setColor(Color.BLUE);
        yFour = 558;
        for (int i = STARTING_X_FOUR - 1; i <= ENDING_X_FOUR - 1; i += 2) {
            g.drawLine(i, yFour, i, yFour - 35);
            yFour -= 1;
        }
        final int STARTING_X_FIVE = 144;
        final int ENDING_X_FIVE = 205;
        int yFive = 653;
        g.setColor(GOLD);
        for (int i = STARTING_X_FIVE; i <= ENDING_X_FIVE; i += 2) {
            g.drawLine(i, yFive, i, yFive - 57);
            yFive -= 1;
        }
        yFive = 653;
        g.setColor(Color.BLUE);
        for (int i = STARTING_X_FIVE - 1; i <= ENDING_X_FIVE - 1; i += 2) {
            g.drawLine(i, yFive, i, yFive - 57);
            yFive -= 1;
        }
        final int STARTING_X_SIX = 199;
        final int ENDING_X_SIX = 222;
        int ySix = 482;
        g.setColor(GOLD);
        for (int i = STARTING_X_SIX; i <= ENDING_X_SIX; i += 2) {
            g.drawLine(i, ySix, i, ySix - 40);
            ySix -= 1;
        }
        ySix = 482;
        g.setColor(Color.BLUE);
        for (int i = STARTING_X_SIX - 1; i <= ENDING_X_SIX - 1; i += 2) {
            g.drawLine(i, ySix, i, ySix - 40);
            ySix -= 1;
        }
        // Do not alter this line of code. It saves the panel to a file for later
        // viewing
        saveDrawingPanel(dp, "drawing_one.png");
    }

    public static void drawGrid(Graphics g, int xPosition, int yPosition, int dimensionsSquare,
            int amountOfVerticalAndHorizontalLines, int sizeOfSmallSquares) {
        /*
         * drawGrid draws a scintillation grid based on the parameters given. Each
         * parameter of drawGrid is used as follows: Graphics g - used to be drawn on
         * and is passed to other methods, int xPosition - used as a referential x
         * position in other variables, int yPosition - used as a referential y position
         * in other variables, int dimensionsSquare - takes in the dimensions of the
         * large black square, int amountOfVerticalAndHorizontalLines - takes in the
         * amount vertical and horizontal lines (which are equal), int
         * sizeOfSmallSquares - takes in the size of the smaller squares created by the
         * lines. Each param is passed to the called method when needed.
         */
        drawBlackSquare(g, xPosition, yPosition, dimensionsSquare);
        drawVerticalAndHorizontalLines(g, xPosition, yPosition, dimensionsSquare, amountOfVerticalAndHorizontalLines,
                sizeOfSmallSquares);
        drawCircles(g, xPosition, yPosition, dimensionsSquare, amountOfVerticalAndHorizontalLines, sizeOfSmallSquares);
    }

    public static void drawBlackSquare(Graphics g, int xPosition, int yPosition, int dimensionsSquare) {
        // Draws the large black square
        // g is drawn on, xPosition is just the x position in this case, yPosition is
        // just the y position
        // dimensionsSquare is used as the dimensions
        g.setColor(Color.BLACK);
        g.fillRect(xPosition, yPosition, dimensionsSquare, dimensionsSquare);
    }

    public static void drawVerticalAndHorizontalLines(Graphics g, int xPosition, int yPosition, int measurementAcross,
            int amountOfVerticalAndHorizontalLines, int sizeOfSmallSquares) {
        // this method draws the gray lines
        // g is used to be drawn on, xPosition is used in the x position of each line in
        // a variable, same for yPosition, amountOfVerticalAndHorizontalLines is used in
        // variables and in the for loop, and sizeOfSmallSquares is used in variables
        g.setColor(Color.GRAY);
        // Line Thickness of the lines is equal to the dimensions of the whole grid
        // minus the times size of the small squares times 1 more than the amount of
        // vertical/horizontal lines across divided by the amount of vertical/horizontal
        // lines. The plus one with the lines comes from
        // there being one more small square going across.
        final int LINE_THICKNESS = (measurementAcross - sizeOfSmallSquares * (amountOfVerticalAndHorizontalLines + 1))
                / amountOfVerticalAndHorizontalLines;
        int xPosOfVertLine = xPosition + sizeOfSmallSquares; // the x position of each vertical line changes when they
        // are placed
        int yPosOfHorizLine = yPosition + sizeOfSmallSquares; // the y position of each horizontal line changes when
        // they are placed
        for (int line = 1; line <= amountOfVerticalAndHorizontalLines; line++) { // places vertical lines
            g.fillRect(xPosOfVertLine, yPosition, LINE_THICKNESS, measurementAcross);
            xPosOfVertLine += sizeOfSmallSquares + LINE_THICKNESS; // the position of the line moves by the size of the
            // small squares and the line thickness
        }
        for (int line = 1; line <= amountOfVerticalAndHorizontalLines; line++) { // places the horizontal lines
            g.fillRect(xPosition, yPosOfHorizLine, measurementAcross, LINE_THICKNESS);
            yPosOfHorizLine += sizeOfSmallSquares + LINE_THICKNESS; // the position of the line moves by the size of the
            // small squares and the line thickness
        }
    }

    public static void drawCircles(Graphics g, int xPosition, int yPosition, int dimensionsSquare,
            int amountOfVerticalAndHorizontalLines, int sizeOfSmallSquares) {
        // draws the circles at the intersections of the lines
        // g is drawn on, xPosition and yPosition are used in coordinates,
        // dimensionsSquare, amountOfVerticalAndHorizontalLines, and sizeOfSmallSquares
        // are used in variables
        g.setColor(Color.WHITE);
        final int LINE_THICKNESS = ((dimensionsSquare - sizeOfSmallSquares * (amountOfVerticalAndHorizontalLines + 1)))
                / amountOfVerticalAndHorizontalLines; // Line Thickness uses the same equation from the lines method
        final int CIRCLE_THICKNESS = (int) (Math.max(LINE_THICKNESS * 1.4, LINE_THICKNESS + 2));
        // CIRCLE_THICKNESS is determined with math.max and the measurements given in
        // the instructions of
        // the assignment. Circle size depends on whether 1.4 * the LINE_THICKNESS is
        // bigger or if LINE_THICKNESS + 2 is bigger
        final int START_X_POSITION = xPosition + sizeOfSmallSquares - (int) (Math.max(LINE_THICKNESS * .4, 2)) / 2;
        final int START_Y_POSITION = yPosition + sizeOfSmallSquares - (int) (Math.max(LINE_THICKNESS * .4, 2)) / 2;
        // Starting X and Y positions of circles drawn needs to be offset by the max
        // method used to find the circle size
        // divided by 2. At the same time, variables currentXPos and currentYPos are
        // needed so that they can change after
        // each circle.
        int currentXPos = START_X_POSITION;
        int currentYPos = START_Y_POSITION;
        // nested for loop prints circles left to right, top to bottom
        for (int currentYOfCircle = 1; currentYOfCircle <= amountOfVerticalAndHorizontalLines; currentYOfCircle++) {
            for (int currentXOfCircle = 1; currentXOfCircle <= amountOfVerticalAndHorizontalLines; currentXOfCircle++) {
                g.fillOval(currentXPos, currentYPos, CIRCLE_THICKNESS, CIRCLE_THICKNESS);
                currentXPos += sizeOfSmallSquares + LINE_THICKNESS; // x and y positions change like the lines
            }
            currentYPos += sizeOfSmallSquares + LINE_THICKNESS;
            currentXPos = START_X_POSITION; // need to restart x after the inner for loop to return to leftmost side
        }

    }

    // Save the current drawing panel to the given file.
    // CS312 Students. Do not alter this method.
    public static void saveDrawingPanel(DrawingPanel dp, String fileName) {
        try {
            dp.save(fileName);
        } catch (IOException e) {
            System.out.println("Unable to save DrawingPanel");
        }
    }
}
