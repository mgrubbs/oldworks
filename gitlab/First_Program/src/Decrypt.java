import java.util.Scanner;

/**
 * CS312 Assignment 9.
 *
 * On my honor, Mark Grubbs, this programming assignment is my own work and I
 * have not shared my solution with any other student in the class.
 *
 * email address: UTEID: Section 5 digit ID: Grader name: Number of slip days
 * used on this assignment:
 * 
 * Program to decrypt a message that has been encrypted with a substitution
 * cipher. We assume only characters with ASCII codes from 32 to 126 inclusive
 * have been encrypted.
 */

public class Decrypt {

    // CS312 students, add your constants here
    // These constants were used for the methods displaying frequency and the key.
    public static final int FIRST_CHAR = 32;
    public static final int LAST_CHAR = 127;

    public static void main(String[] arg) {
        // CS312 Students, do not create any other Scanners connected to System.in
        Scanner keyboard = new Scanner(System.in);
        String fileName = getFileName(keyboard);
        String encryptedText = DecryptUtilities.convertFileToString(fileName);
        // The other method from DecryptUtilities you will have to use is
        // DecryptUtilities.getDecryptionKey(int[]), but first you need to
        // create an array with the frequency of all the ASCII characters in the
        // encrypted text. Count ALL characters from ASCII code 0 to ASCII code 127
        // CS312 students, add you code here.
        decryptText(encryptedText, keyboard);
        keyboard.close();
    }

    // CS312 students, add your methods here
    // This method decrypts the text as long as the user wants to
    // Takes in the encrypted text and the keyboard scanner to be passed to other
    // methods
    public static void decryptText(String encryptText, Scanner keyboard) {
        int[] charFreq = createFreqArray(encryptText);
        char[] decryptionKey = DecryptUtilities.getDecryptionKey(charFreq);
        System.out.println("The encrypted text is:");
        System.out.println(encryptText);
        showFreqOfCharacters(charFreq);
        showCurrentKey(decryptionKey);
        // decryptOneLevel(charFreq, decryptionKey);
        System.out.println();
        System.out.println("The current version of the decrypted text is: ");
        System.out.println();
        showCurrentText(encryptText, decryptionKey);
        System.out.println();
        detectiveTime(keyboard, decryptionKey, encryptText);
        showCurrentKey(decryptionKey);
        System.out.println();
        System.out.println("The final version of the decrypted text is: ");
        System.out.println();
        showCurrentText(encryptText, decryptionKey);

    }

    // Uses a while loop to make the user change the text over time until the text
    // is decrypted
    // Takes in the keyboard to get input, the decryption key so that the user can
    // change it, and the current text so that it can be displayed
    public static void detectiveTime(Scanner keyboard, char[] decryptionKey, String currentText) {
        char wantsToChange = 'Y';
        while (wantsToChange == 'Y') { // start with the assumption the user wants to change the decryption key
            System.out.println("Do you want to make a change to the key?");
            System.out.print("Enter 'Y' or 'y' to make change: ");
            wantsToChange = keyboard.nextLine().toUpperCase().charAt(0);
            if (wantsToChange == 'Y') { // if they did want to change perform the switch in the key
                System.out.print("Enter the decrypt character you want to change: ");
                char originalChar = keyboard.nextLine().charAt(0);
                System.out.print("Enter what the character " + originalChar + " should decrypt to instead: ");
                char changeTo = keyboard.nextLine().charAt(0);
                System.out.println(originalChar + "'s will now decrypt to " + changeTo + "'s and vice versa.");
                changeDecryptionKey(originalChar, changeTo, decryptionKey);
                System.out.println();
                System.out.println("The current version of the decrypted text is: ");
                System.out.println();
                showCurrentText(currentText, decryptionKey);
            }
            System.out.println();
        }
    }

    // this gets the array of frequencies of characters for use in getting the
    // initial decryption key
    public static int[] createFreqArray(String encryptText) {
        final int FREQUENCY_SIZE = 128; // has this size, but only typable characters are used i.e. you can't really
        // type \t on your keyboard
        int[] charFreq = new int[FREQUENCY_SIZE];
        Scanner encryptedTextScanner = new Scanner(encryptText);
        while (encryptedTextScanner.hasNextLine()) { // goes through the text
            String currLine = encryptedTextScanner.nextLine();
            for (int currChar = 0; currChar < currLine.length(); currChar++) {
                charFreq[currLine.charAt(currChar)]++;
            }
        }
        return charFreq;
    }

    // This displays the frequency of the characters for the introduction of the
    // output.
    // Takes in an array of ints that is the frequency of the characters to be
    // printed out in specified format.
    public static void showFreqOfCharacters(int[] charFreq) {
        System.out.println("Frequencies of characters.");
        System.out.println("Character - Frequency");
        for (int i = FIRST_CHAR; i < LAST_CHAR; i++) {
            char c = (char) (i);
            System.out.println(c + " - " + charFreq[i]);
        }
        System.out.println();
    }

    // This shows current decryption key for the beginning of the output and the end
    // of the output.
    // Takes in the decryption key to be printed out in specified format.
    public static void showCurrentKey(char[] decryptionKey) {
        System.out.println("The current version of the key for ASCII characters 32 to 126 is: ");
        for (int i = FIRST_CHAR; i < LAST_CHAR; i++) {
            System.out.println("Encrypt character: " + (char) (i) + ", decrypt character: " + decryptionKey[i]);
        }
    }

    // This changes the decryption key given two chars to switch
    // Takes in the first char and the second char to be flipped. e.g. a and b
    // passed makes them b and a in the decryption key.
    public static void changeDecryptionKey(char originalChar, char changeToThis, char[] decryptionKey) {
        int indexOfOriginal = 0;
        int indexOfChange = 0;
        // the loop searches for the indices of the char to change and the char to
        // change to.
        for (int i = 0; i < decryptionKey.length; i++) {
            if (decryptionKey[i] == originalChar) {
                indexOfOriginal = i;
            }
            if (decryptionKey[i] == changeToThis) {
                indexOfChange = i;
            }
        }
        char tempChar = decryptionKey[indexOfOriginal];
        decryptionKey[indexOfOriginal] = decryptionKey[indexOfChange];
        decryptionKey[indexOfChange] = tempChar;
    }

    // this shows the current text using the key to translate characters in the text
    // to what they are mapped to
    public static void showCurrentText(String text, char[] currentKey) {
        Scanner textScanner = new Scanner(text);
        while (textScanner.hasNextLine()) {
            String currentLine = textScanner.nextLine();
            for (int i = 0; i < currentLine.length(); i++) {
                System.out.print((char) (currentKey[currentLine.charAt(i)]));
            }
            System.out.println();
        }
    }

    // get the name of file to use
    public static String getFileName(Scanner kbScanner) {
        System.out.print("Enter the name of the encrypted file: ");
        String fileName = kbScanner.nextLine().trim();
        System.out.println();
        return fileName;
    }
}
