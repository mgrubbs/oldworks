/*
 *  CS312 Assignment 2.
 *  On my honor, <NAME>, this programming assignment is my own work.
 *
 *  A program to print out the UT Tower in ASCII art form.
 *
 *  Name: Mark Grubbs
 *  email address: siegbalicula@gmail.com
 *  UTEID: msg2772
 *  Section 5 digit ID: 50201
 *  Grader name: Fatima
 *  Number of slip days used on this assignment:
 */

public class Tower {

    // CS312 students, DO NOT ALTER THE FOLLOWING LINE except for the 
    // value of the literal int.
    // You may change the literal int assigned to SIZE to any value from 2 to 100.
    // In the final version of the program you submit set the SIZE to 3.
    public static final int SIZE = 4;
   
    public static void main(String[] args) {
        //prints whole tower in parts
        topOfTower();
        middleOfTower();
        bottomOfTower();
    }

    public static void topOfTower() {
        //prints out the top of tower with methods that use for loops
        hashtagSymbolPart();
        System.out.println();        
        verticalBarPartOfTower();          
        hashtagSymbolPart();
        System.out.println();
    }
    
    public static void spacingForTopOfTower() {
        //prints out the space created by the bottom of the tower
        //amount of spacing is 4 times the size constant given plus two due to offset top from middle
        final int SPACING_FOR_MIDDLE = 4 * SIZE + 2;
        for(int i = 1; i <= SPACING_FOR_MIDDLE; i++) {
            System.out.print(" ");
        }
    }
    
    public static void hashtagSymbolPart() {
        //prints out the top's parts that use hashtags
        spacingForTopOfTower(); //spacing method called
        //amount of # printed is 2 times the size constant given minus 1
        for(int i = 1; i <= 2 * SIZE - 1; i++) {
            System.out.print("#");
        }
    }
    
    public static void verticalBarPartOfTower() {
        //prints out the | parts of the top of the tower
        //this method uses a for loop with loops nested in it print out the | symbols of the tower's top
        //amount of bars uses dimensions (size - 1) * 2 by 2 * size - 1
        final int NUM_BAR_LEVELS = (SIZE - 1) * 2;
        final int NUM_BARS = 2 * SIZE - 1;
        for(int i = 1; i <= NUM_BAR_LEVELS; i++) {
            spacingForTopOfTower(); //spacing method called
            for(int j = 1; j <= NUM_BARS; j++) {
                System.out.print("|");
            }
            System.out.println();
        }  
    }
    
    public static void middleOfTower() {
        //calls methods that put together the middle part of the tower
        restOfMiddleAboveBottomOfMiddle();
        tildaPartOfMiddle();
    }
    
    public static void spacingForMiddleOfTower() {
        //works out spacing for middle part of tower
        //amount of spaces is 4 times the size constant given
        final int SPACES_FOR_MIDDLE = 4 * SIZE;
        for(int i = 1; i <= SPACES_FOR_MIDDLE; i++) {
            System.out.print(" ");
        }
    }
    
    public static void restOfMiddleAboveBottomOfMiddle() {
        //prints out the entire middle portion of tower other than the bottom most line
        //amount of floors on middle portion of tower is the size constant squared
        final int NUM_FLOORS = SIZE * SIZE;
        for(int i = 1; i <= NUM_FLOORS; i++) {
            tildaPartOfMiddle(); //calls tilda method
            spacingForMiddleOfTower(); //calls middle spacing method
            System.out.print("|"); //closes left side of floor
            for(int j = 1; j <= SIZE; j++) { //amount of -O on a floor is the size constant
                System.out.print("-O");
            }
            System.out.print("-|"); //closes right side of floor
            System.out.println();
        }
    }
    
    public static void tildaPartOfMiddle() {
        //prints out parts with tildas
        //important for bottom most part of the middle of tower
        spacingForMiddleOfTower();
        //amount of tildas is 2 times the size constant plus 3
        final int NUM_TILDAS = 2 * SIZE + 3;
        for(int i = 1; i <= NUM_TILDAS; i++) {
            System.out.print("~");
        }
        System.out.println();
    }
    
    
    public static void bottomOfTower() {
        //prints bottom of tower by types of symbols used
        quotesPartOfBottomOfTower();
        circlePartOfBottomOfTower();
    }
    
    public static void quotesPartOfBottomOfTower() {
        //prints out part of bottom of tower that uses ' and "
        int spaces = SIZE / 2; //variable needed because amount of spaces changes going down the tower
        final int NUM_STEP_LEVELS = SIZE / 2 + 1;
        for(int i = 1; i <= NUM_STEP_LEVELS; i++) { //SIZE / 2 + 1 is the amount of lines for this part
            for(int j = 1; j <= spaces; j++) { //spaces printed per line is 3 * (size / 2)
                System.out.print("   ");
            }
            System.out.print("/"); //starts line after spaces
            for(int j = 1; j <= SIZE * 5 - spaces * 3; j++) { //amount of "' is size times 5 minus spaces times three
                System.out.print("\"'");
            }
            System.out.print("\"\\"); //closes off line
            spaces--; //spaces variable changes with each line
            System.out.println();
        }
    }

    
    public static void circlePartOfBottomOfTower() {
        //prints O part of bottom of tower
        final int NUM_LEVELS = SIZE;
        final int NUM_QUOTE_ZEROS = SIZE * 5;
        for(int i = 1; i <= NUM_LEVELS; i++) { //amount of lines of O is equal to size constant
            System.out.print("/"); //starts the line
            for(int j = 1; j <= NUM_QUOTE_ZEROS; j++) { //amount of "O is size times 5
                System.out.print("\"O");
            }
            System.out.print("\"\\"); //ends the line
            System.out.println();
        }
    }
}

