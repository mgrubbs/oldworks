import java.util.Scanner;

/**
 * CS312 Assignment 10.
 *
 * On my honor, Mark Grubbs, this programming assignment is my own work and I
 * have not shared my solution with any other student in the class.
 *
 * email address: siegbalicula@gmail.com UTEID: msg2772 Section 5 digit ID:
 * Grader name: Fatima Number of slip days used on this assignment:
 *
 * Program that allows two people to play Connect Four.
 */

public class ConnectFour {

    // CS312 Students, add you constants here
    public static final int ROWS = 6;
    public static final int COLUMNS = 7;

    public static void main(String[] args) {
        intro();

        // complete this method
        // Recall make and use one Scanner coonected to System.in
        Scanner keyboard = new Scanner(System.in);
        getNamesAndBeginGame(keyboard);
    }

    // CS312 Students, add your methods

    // This method gets the names of the players to be passed to the next method for
    // displaying the game.
    // Of course, it takes in the Scanner keyboard.
    // It calls playGame to actually start playing the game.
    // It also creates the board.
    public static void getNamesAndBeginGame(Scanner keyboard) {
        int[][] theBoard = new int[ROWS][COLUMNS];
        System.out.print("Player 1 enter your name: ");
        String firstPlayer = keyboard.nextLine();
        System.out.println();
        System.out.print("Player 2 enter your name: ");
        String secondPlayer = keyboard.nextLine();
        System.out.println();
        playGame(keyboard, theBoard, firstPlayer, secondPlayer);
    }

    // This goes through the game from turn one to the end.
    // The parameters passed to this is the board, a keyboard scanner, and the
    // player names to be displayed.
    // variables are explained in internal comments.
    // The methods called in this method are victoryAchieved, gameTie,
    // displayCurrentBoard, and placeAPiece.
    public static void playGame(Scanner keyboard, int[][] theBoard, String firstPlayer, String secondPlayer) {
        int currentTurn = 0; // counts the turns to keep track of whose turn it is by taking the remainder
        String currentPlayerName = ""; // this changes by the turn number.
        while (!victoryAchieved(theBoard) && !gameTie(theBoard)) {
            char currentColor = ' '; // this is for placing a piece and is based on the turn number
            if (currentTurn % 2 == 0) { // currentTurn from before
                currentPlayerName = firstPlayer;
                currentColor = 'r';
            } else {
                currentPlayerName = secondPlayer;
                currentColor = 'b';
            }
            System.out.println("Current Board");
            displayCurrentBoard(theBoard);
            System.out.println();
            placeAPiece(theBoard, currentPlayerName, currentColor, keyboard);
            currentTurn++;
        }
        if (victoryAchieved(theBoard)) {
            System.out.println(currentPlayerName + " wins!!");
        } else {
            System.out.println("The game is a draw.");
        }
        System.out.println();
        System.out.println("Final Board");
        displayCurrentBoard(theBoard);
        System.out.println();
    }

    // This displays the board with a for loop using the format given in the
    // instructions
    public static void displayCurrentBoard(int[][] theBoard) {
        final int IS_EMPTY = 0;
        final int PLAYER_1 = 1;
        final int PLAYER_2 = 2;
        System.out.println("1 2 3 4 5 6 7  column numbers");
        for (int[] r : theBoard) {
            for (int c : r) {
                if (c == IS_EMPTY) {
                    System.out.print(". ");
                }
                if (c == PLAYER_1) {
                    System.out.print("r ");
                }
                if (c == PLAYER_2) {
                    System.out.print("b ");
                }
            }
            System.out.println();
        }
    }

    // placingAPiece requires checking the three validations for where a player
    // wants to put a piece. These validations are whether the input is an integer,
    // whether the input is in the board, and whether the column is full.
    public static void placeAPiece(int[][] theBoard, String currentPlayerName, char currentPiece, Scanner keyboard) {
        final String PROMPT = ", enter the column to drop your checker: "; // prompt based on sample output.
        System.out.println(currentPlayerName + " it is your turn.");
        System.out.println("Your pieces are the " + currentPiece + "'s.");
        System.out.print(currentPlayerName + PROMPT);
        int columnChoice = getInt(keyboard, currentPlayerName + PROMPT);
        System.out.println();
        int currentPlayer = 0;
        if (currentPiece == 'r') {
            currentPlayer = 1; // this converts the char to an int for insertion in the board
        } else {
            currentPlayer = 2;
        }
        // loop to ensure that the input is ok.
        while (!goThroughChecks(theBoard, columnChoice - 1, currentPlayer)) {
            System.out.print(currentPlayerName + PROMPT);
            columnChoice = getInt(keyboard, currentPlayerName + PROMPT);
            System.out.println();
        }

    }

    // simply checks if the int is not outside of the columns
    public static boolean isGoodInt(int response) {
        return response >= 0 && response <= 6;
    }

    // checks if victory has been achieved depending on checking if any connection
    // of four has occurred.
    public static boolean victoryAchieved(int[][] theBoard) {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (!(theBoard[i][j] == 0) && checkAllDirections(j, i, theBoard[i][j], theBoard)) {
                    return true;
                }
            }
        }
        return false;
    }

    // this is used in checkAllDirections for the while loop
    public static boolean inbounds(int row, int col) {
        return row < ROWS && row >= 0 && col < COLUMNS && col >= 0;
    }

    // calls checkADirection 4 times, once for each of the relationship
    public static boolean checkAllDirections(int colStart, int rowStart, int playerChecking, int[][] theBoard) {
        return checkADirection(colStart, rowStart, playerChecking, theBoard, 0)
                || checkADirection(colStart, rowStart, playerChecking, theBoard, 1)
                || checkADirection(colStart, rowStart, playerChecking, theBoard, 2)
                || checkADirection(colStart, rowStart, playerChecking, theBoard, 3);
    }

    // depending on an int that represents a direction, this method checks that
    // direction for a victory
    // the increment changes depending on the direction
    // the parameters passed to this method are where the cell starts, the player's
    // int, the board, and the direction
    public static boolean checkADirection(int colStart, int rowStart, int playerChecking, int[][] theBoard,
            int direction) {
        final int EAST = 0;
        final int SOUTHEAST = 1;
        final int SOUTH = 2;
        final int SOUTHWEST = 3;
        final int VICTORY = 4;
        int incrementRow = 0;
        int incrementCol = 0;
        int inARow = 1;
        if (direction == EAST) {
            incrementCol = 1;
        } else if (direction == SOUTHEAST) {
            incrementCol = 1;
            incrementRow = 1;
        } else if (direction == SOUTH) {
            incrementRow = 1;
        } else if (direction == SOUTHWEST) {
            incrementCol = -1;
            incrementRow = 1;
        }
        int startingCol = colStart + incrementCol;
        int startingRow = rowStart + incrementRow;
        // immediately returns false if a different number is found.
        // checks if 4 are found in a row too
        while (inbounds(startingRow, startingCol) && inARow < VICTORY) {
            if (theBoard[startingRow][startingCol] != playerChecking) {
                return false;
            }
            inARow++;
            startingCol += incrementCol;
            startingRow += incrementRow;
        }
        if (inARow == VICTORY) {
            return true;
        }
        return false;
    }

    // checks if a column is full by checking if the top row is taken.
    public static boolean columnIsFull(int[][] theBoard, int columnToPlaceIn) {
        return !(theBoard[0][columnToPlaceIn] == 0);
    }

    // takes columnIsFull method and applies it to all columns
    public static boolean gameTie(int[][] theBoard) {
        for (int i = 0; i < COLUMNS; i++) {
            if (!columnIsFull(theBoard, i)) {
                return false;
            }
        }
        return true;
    }

    // goes through the requirement for a input to be allowed.
    // also performs the gravity effect of placing a piece in a column.
    public static boolean goThroughChecks(int[][] theBoard, int columnToPlace, int currentPlayer) {
        if (!isGoodInt(columnToPlace)) { 
            System.out.println(columnToPlace + 1 + " is not a valid column.");
            return false;
        } else if (!columnIsFull(theBoard, columnToPlace)) {
            int currentRow = 5; // start at bottom and go up
            int currentColumn = theBoard[currentRow][columnToPlace];
            while (!(currentColumn == 0) && currentRow >= 0) {
                currentRow--;
                currentColumn = theBoard[currentRow][columnToPlace];
            }
            theBoard[currentRow][columnToPlace] = currentPlayer;
            return true;
        } else {
            System.out.println(columnToPlace + 1 + " is not a legal column. That column is full");
        }
        return false;
    }

    // show the intro
    public static void intro() {
        System.out.println("This program allows two people to play the");
        System.out.println("game of Connect four. Each player takes turns");
        System.out.println("dropping a checker in one of the open columns");
        System.out.println("on the board. The columns are numbered 1 to 7.");
        System.out.println("The first player to get four checkers in a row");
        System.out.println("horizontally, vertically, or diagonally wins");
        System.out.println("the game. If no player gets fours in a row and");
        System.out.println("and all spots are taken the game is a draw.");
        System.out.println("Player one's checkers will appear as r's and");
        System.out.println("player two's checkers will appear as b's.");
        System.out.println("Open spaces on the board will appear as .'s.\n");
    }

    // prompt the user for an int. The String prompt will
    // be printed out. I expect key is connected to System.in.
    public static int getInt(Scanner key, String prompt) {
        while (!key.hasNextInt()) {
            System.out.println();
            String notAnInt = key.nextLine();
            System.out.println(notAnInt + " is not an integer.");
            System.out.print(prompt);
        }
        int result = key.nextInt();
        key.nextLine();
        return result;
    }
}
