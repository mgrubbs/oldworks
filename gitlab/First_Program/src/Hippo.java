import java.awt.Color;

/**
 * CS312 Assignment 11.
 *
 * On MY honor, Mark Grubbs, this programming assignment is MY own work and I
 * have not provided this code to any other student.
 *
 * Student name: Mark Grubbs UTEID: msg2772 email address:
 * siegbalicula@gmail.com Grader name: Fatima Number of slip days used on this
 * assignment:
 *
 */

public class Hippo extends Critter {

    // The int hungry determines what the hippo looks like and whether the hippo
    // eats
    // stepsTaken determines what direction to move and changes currDirection
    private int hungry;
    private int stepsTaken;
    private Direction currDirection;

    // hungry is randomly instantiated from 0 - 9 and represents the amount of food
    // the hippo will eat
    // calling getMove() sets the currDirection
    public Hippo(int hunger) {
        hungry = hunger;
    }

    // decrement hungry and use a temporary hungry int to check if eat returns true
    public boolean eat() {
        int tempHungry = hungry;
        hungry--;
        return tempHungry > 0;
    }

    // if eat is true, then return gray.
    // else return white
    // color represents the hippo
    public Color getColor() {
        if (hungry > 0) {
            return Color.GRAY;
        }
        return Color.WHITE;
    }

    // attack with scratch if hungry and pounce otherwise
    public Attack fight(String oppon) {
        if (hungry > 0) {
            return Attack.SCRATCH;
        }
        return Attack.POUNCE;
    }

    // the hippo appears as the amount of food it wants to eat until it reaches zero
    // it then remains as zero
    public String toString() {
        if (hungry > 0) {
            return hungry + "";
        }
        return 0 + "";
    }

    // the hippo moves in random one direction 5 times then changes
    // to simulate this, the steps the hippo has taken increments each time it moves
    // and when steps taken is divisible by five, the direction changes
    public Direction getMove() {
        final int randoDirection = (int) (Math.random() * 4) + 1;
        if (stepsTaken % 5 == 0) {
            if (randoDirection == 1) {
                currDirection = Direction.NORTH;
            } else if (randoDirection == 2) {
                currDirection = Direction.EAST;
            } else if (randoDirection == 3) {
                currDirection = Direction.SOUTH;
            } else {
                currDirection = Direction.WEST;
            }
        }
        stepsTaken++;
        return currDirection;
    }

}
