import java.awt.Color;

/**
 * CS312 Assignment 11.
 *
 * On MY honor, Mark Grubbs, this programming assignment is MY own work and I
 * have not provided this code to any other student.
 *
 * Student name: Mark Grubbs UTEID: msg2772 email address:
 * siegbalicula@gmail.com Grader name: Fatima Number of slip days used on this
 * assignment:
 *
 */

public class Bird extends Critter {

    // the direction of the bird varies on the multiple of footstep
    private int stepsTaken;
    private Direction currDirection;
    private String currStringDirection;

    // stepsTaken being zero is fine. first direction is north. (first string is
    // "^")
    public Bird() {
        currStringDirection = "^";
    }

    // the bird is always blue
    public Color getColor() {
        return Color.BLUE;
    }

    // The bird is on a 12 step motion that repeats.
    // The direction and the string the depicts the bird depend on the current step.
    // 0 - 2 = North and ^, 3 - 5 = East and >, 6 - 8 = South and V, and 9 - 12
    // (else) = West and <
    public Direction getMove() {
        final int NORTH_THRESHOLD = 3; // limits on directions
        final int EAST_THRESHOLD = 6;
        final int SOUTH_THRESHOLD = 9;
        final int STEP_IN_CYCLE = stepsTaken % 12;
        if (STEP_IN_CYCLE < NORTH_THRESHOLD) {
            currDirection = Direction.NORTH; // change direction and toString
            currStringDirection = "^";
        } else if (STEP_IN_CYCLE >= NORTH_THRESHOLD && STEP_IN_CYCLE < EAST_THRESHOLD) {
            currDirection = Direction.EAST;
            currStringDirection = ">";
        } else if (STEP_IN_CYCLE >= EAST_THRESHOLD && STEP_IN_CYCLE < SOUTH_THRESHOLD) {
            currDirection = Direction.SOUTH;
            currStringDirection = "V";
        } else {
            currDirection = Direction.WEST;
            currStringDirection = "<";
        }
        stepsTaken++;
        return currDirection;
    }

    // never eats
    public boolean eat() {
        return false;
    }

    // attacks with Roc- I mean Roar if the opponent is an ant and otherwise attacks
    // with pap- I mean Pounce
    public Attack fight(String opponent) {
        if (opponent.equals("%")) {
            return Attack.ROAR;
        }
        return Attack.POUNCE;
    }

    // returns the current string of the bird depending on the direction
    public String toString() {
        return currStringDirection;
    }
}
