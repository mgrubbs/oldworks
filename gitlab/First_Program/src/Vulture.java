import java.awt.Color;

/**
 * CS312 Assignment 11.
 *
 * On MY honor, Mark Grubbs, this programming assignment is MY own work and I
 * have not provided this code to any other student.
 *
 * Student name: Mark Grubbs UTEID: msg2772 email address:
 * siegbalicula@gmail.com Grader name: Fatima Number of slip days used on this
 * assignment:
 *
 */

public class Vulture extends Bird {

    // this boolean serves to determine whether or not the vulture would eat
    private boolean isHungry;

    // calls the super and initializes isHungry to true
    public Vulture() {
        super();
        isHungry = true;
    }

    // unlike the bird, the vulture eats when it is hungry and stops eating
    // afterward until it fights. This changes isHungry to false when it is called
    public boolean eat() {
        boolean tempIsHungry = isHungry;
        isHungry = false;
        return tempIsHungry;
    }

    // color is black
    public Color getColor() {
        return Color.BLACK;
    }

    // fights like the bird, but after it fights, it becomes hungry
    public Attack fight(String oppon) {
        isHungry = true;
        return super.fight(oppon);
    }

    // returns the same toString as bird
    public String toString() {
        return super.toString();
    }

    // moves the same way as the bird
    public Direction getMove() {
        return super.getMove();
    }

}
