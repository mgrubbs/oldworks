import java.util.Scanner;

/**
 * CS312 Assignment 5.
 * 
 * On my honor, Mark Grubbs, this programming assignment is my own work and I
 * have not shared my solution with any other student in the class.
 *
 * A program to play calculate the readability of text using the Flesch Reading
 * Ease Test.
 *
 * email address: siegbalicula@gmail.com UTEID: msg2772 Unique 5 digit course
 * ID: 50160 Number of slip days used on this assignment:
 */
public class Flesch {

    /*
     * A program that calculates the Flesch Reading Ease Test for text entered by
     * the user.
     */
    public static void main(String[] args) {
        // CS312 students, do not create any other Scanners besides this one.
        // Pass this is a parameter as necessary.
        Scanner key = new Scanner(System.in);

        // TODO CS312 students add your method calls here.
        printIntroduction();
        final int TEXTS_TO_ANALYZE = getAmountOfTextToAnalyze(key); 
        System.out.println();
        key.nextLine();
        for (int currText = 1; currText <= TEXTS_TO_ANALYZE; currText++) {
            printStatistics(key, currText);
        }
        key.close();
    }

    // prints out the introduction shown at the top of sample outputs
    // includes a new line for spacing
    public static void printIntroduction() {
        System.out.println("*** Flesch Reading Ease Test ***");
        System.out.println("This program asks you for text and");
        System.out.println("then calculates and displays the");
        System.out.println("readability of the text based on the");
        System.out.println("Flesch Reading Ease Test.");
        System.out.println();
    }

    // gets amount of text to analyze and returns the number
    public static int getAmountOfTextToAnalyze(Scanner sc) {
        System.out.print("How many pieces of text do you want to analyze? ");
        return sc.nextInt();
    }

    // prints the statistics of the text given including sentences, words, and
    // syllables given the conditions that match each
    public static void printStatistics(Scanner sc, int ct) {
        System.out.print("Please enter text sample number " + ct + ": ");
        String textToAnalyze = sc.nextLine();
        final int TEXT_SENTENCES = getSentences(textToAnalyze); // each of these constants are used in FLESCH_SCORE, so
        // I made them for readability (and so that the get methods would not have to be
        // ran more than once per input)
        final int TEXT_WORDS = getWords(textToAnalyze);
        final int TEXT_SYLLABLES = getSyllables(textToAnalyze);
        System.out.println();
        System.out.println("Statistics for this text: " + textToAnalyze);
        System.out.println("Number of sentences: " + TEXT_SENTENCES);
        System.out.println("Number of words: " + TEXT_WORDS);
        System.out.println("Number of syllables: " + TEXT_SYLLABLES);
        final double FLESCH_SCORE = getFleschScore(TEXT_SENTENCES, TEXT_WORDS, TEXT_SYLLABLES);
        System.out.printf("Flesch score: %.1f\n\n", FLESCH_SCORE);
    }

    // gets number of sentences by counting the amount of .:;?! and returns that
    // number
    public static int getSentences(String s) {
        final int STRING_LENGTH = s.length();
        int amountSentence = 0;
        for (int i = 0; i < STRING_LENGTH; i++) { // runs through the entire string and checks for .:;?!
            if (s.charAt(i) == '.' || s.charAt(i) == ':' || s.charAt(i) == ';' || s.charAt(i) == '?'
                    || s.charAt(i) == '!') {
                amountSentence++;
            }
        }
        if (amountSentence == 0) { // special case for no condition met
            return 1;
        }
        return amountSentence;
    }

    // gets words by checking the each character and the character after it for a
    // delimiter (\t\nspace:;.!?)
    // \t and \n get checked with the space check and returns the number of words
    public static int getWords(String s) {
        int amountWords = 0;
        final String TEXT = " " + s; // this allows the for loop to not go out of bounds
        for (int i = 0; i < s.length(); i++) {
            if ((TEXT.charAt(i + 1) != ' ' && TEXT.charAt(i + 1) != '.' && TEXT.charAt(i + 1) != ';'
                    && TEXT.charAt(i + 1) != ':' && TEXT.charAt(i + 1) != '?' && TEXT.charAt(i + 1) != '!')
                    && (TEXT.charAt(i) == ' ' || TEXT.charAt(i) == '.' || TEXT.charAt(i) == ';' || TEXT.charAt(i) == ':'
                            || TEXT.charAt(i) == '?' || TEXT.charAt(i) == '!')) {
                amountWords++;
            }
        }
        return amountWords;
    }

    // counts number of syllables based on vowels similarly to words and returns
    // that number
    public static int getSyllables(String s) {
        int numSyllables = 0;
        final String TEXT = (" " + s).toLowerCase(); // allows the for loop to go through only lowercase letters and not
        // go out of bounds
        for (int i = 0; i < s.length(); i++) {
            if (!(TEXT.charAt(i + 1) == 'a' || TEXT.charAt(i + 1) == 'e' || TEXT.charAt(i + 1) == 'i'
                    || TEXT.charAt(i + 1) == 'o' || TEXT.charAt(i + 1) == 'u')
                    && (TEXT.charAt(i) == 'a' || TEXT.charAt(i) == 'e' || TEXT.charAt(i) == 'i' || TEXT.charAt(i) == 'o'
                            || TEXT.charAt(i) == 'u')) {
                numSyllables++;
            }
        }
        return numSyllables;
    }

    // returns the Flesch Score using variables tse (amount of sentences), w (amount
    // of words), and tsy (amount of syllables)
    public static double getFleschScore(int tse, int w, int tsy) {
        final double NUM_SUB_FROM = 206.835; // avoid magic numbers
        final double WORD_SENTENCE = 1.015;
        final double SYLLABLE_WORD = 84.6;
        return NUM_SUB_FROM - (WORD_SENTENCE * w / tse) - (SYLLABLE_WORD * tsy / w);
    }
}