
/**
 * CS312 Assignment 8.
 * 
 * On my honor, Mark Grubbs, this programming assignment is my own work and I have
 * not shared my solution with any other student in the class.
 *
 * A program to read a file with raw data from a Keirsey personality test
 * and print out the results.
 *
 *  email address: siegbalicula@gmail.com
 *  UTEID: msg2772
 *  Unique 5 digit course ID: 50160
 *  Grader name: Fatima
 *  Number of slip days used on this assignment:
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Personality {

    // CS312 Student- Add your constants here.
    // These constants are here to make things easily editable.
    public final static int AMOUNT_OF_ANSWERS = 70;
    public final static int AMOUNT_OF_EXTRO_OR_INTRO = 10;
    public final static int AMOUNT_OF_SENS_OR_INTUI = 20;
    public final static int AMOUNT_OF_THINK_OR_FEEL = 20;
    public final static int AMOUNT_OF_JUDGE_OR_PERCEIVE = 20;
    public final static String A_RESULTS = "ESTJ";
    public final static String B_RESULTS = "INFP";
    public final static String NO_ANSWER = "no answers";
    public final static String EQUAL_RESULTS = "X";
    // The main method to process the data from the personality tests
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in); // do not make any other Scanners connected to System.in
        Scanner fileScanner = getFileScanner(keyboard);

        // CS312 students - your code and methods calls go here
        getPersonalities(fileScanner);
        fileScanner.close();
        keyboard.close();
    }

    // CS312 Students add you methods here
    // The prints the names along with their percentages and personalities. 
    // Takes in the file to be analyzed.
    public static void getPersonalities(Scanner fileScanner) {
        int numberOfPersonalities = getNumberOfPersonalities(fileScanner);
        // uses a for loop since we know how many names to go through
        for (int currentPerson = 0; currentPerson < numberOfPersonalities; currentPerson++) {
            System.out.printf("%30s:", fileScanner.nextLine());
            int[] arrayOfPersonalities = getArrayOfPersonalities(fileScanner.nextLine());
            // In this case, the index does not matter.
            for (int currentPercent : arrayOfPersonalities) {
                if (currentPercent < 0) {
                    System.out.printf(" %10S", NO_ANSWER);
                } else {
                    System.out.printf("%11d", currentPercent);
                }
            }
            System.out.print(" = " + showPersonality(arrayOfPersonalities));
            System.out.println();
        }
    }

    // this gets the number of personalities and skips to the next line.
    // takes in the file to be analyzed
    public static int getNumberOfPersonalities(Scanner fileScanner) {
        final int numPersonalities = fileScanner.nextInt();
        fileScanner.nextLine();
        return numPersonalities;
    }

    // Using the offsets for questions given in the instructions, this
    // returns an array of ints that are the percentages of B answers.
    public static int[] getArrayOfPersonalities(String answers) {
        int[] extOrInt = getAnswers(answers, 0);
        int extOrIntPercentB = (int) Math.round(100.0 * (1.0 * extOrInt[1] / (AMOUNT_OF_EXTRO_OR_INTRO - extOrInt[0])));
        int[] sensOrIntui = getAnswers(answers, 1);
        int sensOrIntuiPercentB = (int) Math
                .round(100.0 * (1.0 * sensOrIntui[1] / (AMOUNT_OF_SENS_OR_INTUI - sensOrIntui[0])));
        int[] thinkOrFeel = getAnswers(answers, 3);
        int thinkOrFeelPercentB = (int) Math
                .round(100.0 * (1.0 * thinkOrFeel[1] / (AMOUNT_OF_THINK_OR_FEEL - thinkOrFeel[0])));
        int[] judgeOrPerceive = getAnswers(answers, 5);
        int judgeOrPerceivePercentB = (int) Math
                .round(100.0 * (1.0 * judgeOrPerceive[1] / (AMOUNT_OF_JUDGE_OR_PERCEIVE - judgeOrPerceive[0])));
        return new int[] { extOrIntPercentB, sensOrIntuiPercentB, thinkOrFeelPercentB, judgeOrPerceivePercentB };
    }

    // gets the times B was answered and no answer was given to be used in
    // getting the array of personalities. It is passed a string of answers
    // and an offset for where the questions start.
    public static int[] getAnswers(String answers, int offset) {
        int noAnswer = 0;
        int answersB = 0;
        // the loops go in groups of 7, 10 times. However, the answers of the
        // string go towards different personalities. That's why there is an
        // offset.
        final int startPosition = 0 + offset;
        // As a way of dealing with the times where answers were completely
        // blank, I created a special case in which the percentage became negative.
        // If the offset was above 0, then it was not apart of the personality of
        // extrovert or introvert, so there were multiple answers.
        if (startPosition > 0) {
            for (int i = startPosition; i < AMOUNT_OF_ANSWERS; i += 7) {
                final char currAnswer1 = answers.toUpperCase().charAt(i);
                final char currAnswer2 = answers.toUpperCase().charAt(i + 1);
                if (currAnswer1 == '-') {
                    noAnswer++;
                } else if (currAnswer1 == 'B') {
                    answersB++;
                }
                if (currAnswer2 == '-') {
                    noAnswer++;
                } else if (currAnswer2 == 'B') {
                    answersB++;
                }
            }
            if (noAnswer == 20) {
                noAnswer = 21;
                answersB = 21;
            }
        } else {
            for (int i = startPosition; i < AMOUNT_OF_ANSWERS; i += 7) {
                final char currAnswer = answers.toUpperCase().charAt(i);
                if (currAnswer == '-') {
                    noAnswer++;
                } else if (currAnswer == 'B') {
                    answersB++;
                }
            }
            if (noAnswer == 10) {
                noAnswer = 11;
                answersB = 21;
            }
        }
        return new int[] { noAnswer, answersB };
    }

    // This converts the personality percentages from getArrayOfPersonalities to
    // show the different personalities of the current person. The string returned
    // is the personalities.
    public static String showPersonality(int[] percentBs) {
        String personality = "";
        // goes through the percents of the amount answered with B and figures out the
        // personality
        for (int currB = 0; currB < percentBs.length; currB++) {
            if (percentBs[currB] < 0) {
                personality = personality + "-";
            } else if (percentBs[currB] > 50) {
                personality = personality + B_RESULTS.charAt(currB);
            } else if (percentBs[currB] < 50 && percentBs[currB] >= 0) {
                personality = personality + A_RESULTS.charAt(currB);
            } else {
                personality = personality + EQUAL_RESULTS;
            }
        }
        return personality;
    }

    // Method to choose a file.
    // Asks user for name of file.
    // If file not found create a Scanner hooked up to a dummy set of data
    // Example use:
    public static Scanner getFileScanner(Scanner keyboard) {
        Scanner result = null;
        try {
            System.out.print("Enter the name of the file with the personality data: ");
            String fileName = keyboard.nextLine().trim();
            System.out.println();
            result = new Scanner(new File(fileName));
        } catch (FileNotFoundException e) {
            System.out.println("Problem creating Scanner: " + e);
            System.out.println("Creating Scanner hooked up to default data " + e);
            String defaultData = "1\nDEFAULT DATA\n" + "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                    + "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            result = new Scanner(defaultData);
        }
        return result;
    }
}