import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Work {
    
    public static void main(String[]args) throws FileNotFoundException {
        File file = new File("text.txt");
        Scanner phial = new Scanner(file);
        System.out.println(ratioOfIntsToDoubles(phial));
    }
    
    public static double ratioOfIntsToDoubles(Scanner phial) {
        int doubles = 0;
        int ints = 0;
        while(phial.hasNextLine()) {
            Scanner str = new Scanner(phial.nextLine());
            while(str.hasNext()) {
                if(str.hasNextInt()) {
                    ints++;
                } else if(str.hasNextDouble()) {
                    doubles++;
                }
                str.next();
            }
        }
        return 1.0 * ints / doubles;
    }
}
