/*
 * CS312 Assignment 1.
 * On my honor, Mark Grubbs, this programming assignment is my own work.
 *
 * A program to print out the lyrics to the
 * children's song, "There was an old woman".
 *
 *  Name: Mark Grubbs
 *  email address: siegbalicula@gmail.com
 *  UTEID: msg2772
 *  Section 5 digit ID: 50201
 *  Grader name: Fatima
 *  Number of slip days used on this assignment:
 */

public class Song {

    public static void main(String[] args) {
        // print out the lyrics of "There was an old woman ... " by animal introduced
        flyPart();
        spiderPart();
        birdPart();
        catPart();
        dogPart();
        goatPart();
        cowPart();
        horsePart();
    }

    public static void swallowedThatFlyDie() {
        // prints out the lines "I don't know why... ...she'll die." and a blank line.
        System.out.println("I don't know why she swallowed that fly,");
        System.out.println("Perhaps she'll die.");
        System.out.println();
    }
    
    public static void spiderAndBird() {
        //prints out the middle of the bird part to be repeated
        System.out.println("She swallowed the bird to catch the spider,");
        System.out.println("She swallowed the spider to catch the fly,");
    }
    
    public static void spiderBirdCat() {
        //prints out middle of cat part
        System.out.println("She swallowed the cat to catch the bird,");
        spiderAndBird();
    }
    
    public static void spiderBirdCatDog() {
        //prints out middle of dog part
        System.out.println("She swallowed the dog to catch the cat,");
        spiderBirdCat();
        
    }
    
    public static void spiderBirdCatDogGoat() {
        //prints out middle of goat part
        System.out.println("She swallowed the goat to catch the dog,");
        spiderBirdCatDog();
        
    }
    
    public static void spiderBirdCatDogGoatCow() {
        //prints out middle of cow part
        System.out.println("She swallowed the cow to catch the goat,");
        spiderBirdCatDogGoat();
        
    }

    public static void flyPart() {
        //prints out the verse where fly is introduced
        System.out.println("There was an old woman who swallowed a fly.");
        swallowedThatFlyDie();
    }
    
    public static void spiderPart() {
        //prints out the verse where spider is introduced
        System.out.println("There was an old woman who swallowed a spider,");
        System.out.println("That wriggled and iggled and jiggled inside her.");
        System.out.println("She swallowed the spider to catch the fly,");
        swallowedThatFlyDie();
    }
    
    public static void birdPart() {
        //prints out the verse where bird is introduced
        System.out.println("There was an old woman who swallowed a bird,");
        System.out.println("How absurd to swallow a bird.");
        spiderAndBird();
        swallowedThatFlyDie();
    }
    
    public static void catPart() {
        //prints out the verse where cat is introduced
        System.out.println("There was an old woman who swallowed a cat,");
        System.out.println("Imagine that to swallow a cat.");
        spiderBirdCat();
        swallowedThatFlyDie();
    }
    
    public static void dogPart() {
        //prints out the verse where dog is introduced
        System.out.println("There was an old woman who swallowed a dog,");
        System.out.println("What a hog to swallow a dog.");
        spiderBirdCatDog();
        swallowedThatFlyDie();
    }
    
    public static void goatPart() {
        //prints out the verse where goat is introduced
        System.out.println("There was an old woman who swallowed a goat,");
        System.out.println("She just opened her throat to swallow a goat.");
        spiderBirdCatDogGoat();
        swallowedThatFlyDie();
    }
    
    public static void cowPart() {
        //prints out the verse where cow is introduced
        System.out.println("There was an old woman who swallowed a cow,");
        System.out.println("I don't know how she swallowed a cow.");
        spiderBirdCatDogGoatCow();
        swallowedThatFlyDie();
    }
    
    public static void horsePart() {
        //prints out the end of the song
        System.out.println("There was an old woman who swallowed a horse,");
        System.out.println("She died of course.");
    }
}
