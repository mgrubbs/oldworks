
/**
 * CS312 Assignment 11.
 *
 * On MY honor, Mark Grubbs, this programming assignment is MY own work and I
 * have not provided this code to any other student.
 *
 * Student name: Mark Grubbs 
 * UTEID: msg2772 
 * email address: siegbalicula@gmail.com 
 * Grader name: Fatima 
 * Number of slip days used on this assignment:
 */

public class GuitarString {

    // has instance variables RingBuffer guitar for reverberating frequencies, int N
    // for the RingBuffer capacity and other features, and the time
    private RingBuffer guitar;
    private int N;
    private int time;

    // constructs a new GuitarString by taking in a double frequency, which dictates
    // what N is and has the N be the capacity for the RingBuffer. Also, the
    // RingBuffer guitar is enqueued to start with all zeroes.
    public GuitarString(double frequency) {
        final int SAMPLING_RATE = 44100;
        N = (int) (Math.round(SAMPLING_RATE / frequency));
        guitar = new RingBuffer(N);
        for (int i = 0; i < N; i++) {
            guitar.enqueue(0.0);
        }
    }

    // uses the double[] init parameter given by the client to make a RingBuffer.
    // Makes the RingBuffer have the same size and elements as init.
    public GuitarString(double[] init) {
        guitar = new RingBuffer(init.length);
        for (double val : init) {
            guitar.enqueue(val);
        }
    }

    // replaces each element in the RingBuffer with a new value ranging from -0.5 to
    // 0.5 by dequeueing them and enqueueing the new value
    public void pluck() {
        for (int i = 0; i < N; i++) {
            final double NEW_VALUE = Math.random() - Math.random() / 2;
            guitar.dequeue();
            guitar.enqueue(NEW_VALUE);
        }
    }

    // uses the energy decay factor given along with the next element to be dequeued
    // to make a new element that is the average of the dequeued element and the new
    // next element multiplied by the energy decay factor. This element is then
    // enqueued and time is incremented
    public void tic() {
        final double ENERGY_DECAY_FACTOR = 0.994;
        final double FRONT = guitar.dequeue();
        final double AVERAGE = ((FRONT + sample()) / 2) * ENERGY_DECAY_FACTOR;
        guitar.enqueue(AVERAGE);
        time++;
    }

    // peeks at the next element in the RingBuffer
    public double sample() {
        return guitar.peek();
    }

    // returns the times tic has ran
    public int time() {
        return time;
    }
}
