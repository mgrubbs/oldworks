import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * CS312 Assignment 11.
 *
 * On MY honor, Mark Grubbs, this programming assignment is MY own work and I
 * have not provided this code to any other student.
 *
 * Student name: Mark Grubbs UTEID: msg2772 email address:
 * siegbalicula@gmail.com Grader name: Fatima Number of slip days used on this
 * assignment:
 */

public class GuitarHero {

    // constant for total keys on the "keyboard"
    public static final int TOTAL_KEYS = 37;

    // main makes a "guitar" along with keys and runs the StdDraw to take in keys
    // that play a particular tune.
    // it also calls drawStuff() to draw the keys that you can press
    // making a "guitar" entails mapping the keys to the guitar along with their
    // frequencies
    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        GuitarString[] guitar = new GuitarString[TOTAL_KEYS];
        char[] keys = new char[TOTAL_KEYS];
        mapKeys(guitar, keys);
        drawStuff();
        while (true) {
            if (StdDraw.hasNextKeyTyped()) {
                char key = StdDraw.nextKeyTyped();
                final int INDEX = findIndex(keys, key);
                if (INDEX > -1) {
                    guitar[INDEX].pluck();
                }
            }
            final double superposition = getSuperposition(guitar);
            StdAudio.play(superposition);
            ticGuitarStrings(guitar);
        }
//        Scanner keyboard = new Scanner(System.in);
//        System.out.print("give me a file ");
//        File phial = new File(keyboard.next());
//        playTune(phial, guitar, keys);
//        
//        keyboard.close();
    }
    
    private static void playTune(File phial, GuitarString[] guitar, char[] keys ) throws FileNotFoundException, InterruptedException {
        Scanner phiala = new Scanner(phial);
        while(phiala.hasNext()) {
            char nextNote = phiala.next().charAt(0);
            final int INDEX = findIndex(keys, nextNote);
            guitar[INDEX].pluck();
            final double superposition = getSuperposition(guitar);
            StdAudio.play(superposition);
            TimeUnit.SECONDS.sleep(1);
            ticGuitarStrings(guitar);
        }
        
    }

    // maps each keyboard key to a frequency on the guitar
    // takes in a GuitarString array and a char array and changes them.
    // frequencies get higher from left to right
    public static void mapKeys(GuitarString[] guitar, char[] keys) {
        final String KEYBOARD = "q2we4r5ty7u8i9op-[=zxdcfvgbnjmk,.;/' ";
        final double BASE_FREQ = 440;
        for (int i = 0; i < TOTAL_KEYS; i++) {
            final double FREQ_FACTOR = Math.pow(1.05956, i - 24);
            final double CURRENT_FREQUENCY = BASE_FREQ * FREQ_FACTOR;
            guitar[i] = new GuitarString(CURRENT_FREQUENCY);
            keys[i] = KEYBOARD.charAt(i);
        }
    }

    // finds the index to play from the GuitarString array
    public static int findIndex(char[] keys, char key) {
        for (int i = 0; i < keys.length; i++) {
            if (keys[i] == key) {
                return i;
            }
        }
        return -1;
    }

    // calculates superposition by adding all the samples of the guitar strings
    public static double getSuperposition(GuitarString[] guitar) {
        double superposition = 0.0;
        for (GuitarString string : guitar) {
            superposition += string.sample();
        }
        return superposition;
    }

    // calls tic() on every GuitarString
    public static void ticGuitarStrings(GuitarString[] guitar) {
        for (GuitarString string : guitar) {
            string.tic();
        }
    }

    // draws the text
    public static void drawStuff() {
        StdDraw.setPenColor(Color.RED);
        StdDraw.text(0.5 / 12, .9999, "2");
        StdDraw.text(3.5 / 12, .9999, "4");
        StdDraw.text(4.5 / 12, .9999, "5");
        StdDraw.text(6.5 / 12, .9999, "7");
        StdDraw.text(7.5 / 12, .9999, "8");
        StdDraw.text(8.5 / 12, .9999, "9");
        StdDraw.text(10.5 / 12, .9999, "-");
        StdDraw.text(11.5 / 12, .9999, "=");
        StdDraw.setPenColor(new Color(50, 128, 50));
        StdDraw.text(0.5 / 13, .6999, "q");
        StdDraw.text(1.5 / 13, .6999, "w");
        StdDraw.text(2.5 / 13, .6999, "e");
        StdDraw.text(3.5 / 13, .6999, "r");
        StdDraw.text(4.5 / 13, .6999, "t");
        StdDraw.text(5.5 / 13, .6999, "y");
        StdDraw.text(6.5 / 13, .6999, "u");
        StdDraw.text(7.5 / 13, .6999, "i");
        StdDraw.text(8.5 / 13, .6999, "o");
        StdDraw.text(9.5 / 13, .6999, "p");
        StdDraw.text(10.5 / 13, .6999, "[");
        StdDraw.setPenColor(new Color(40, 40, 175));
        StdDraw.text(2.5 / 12, .3999, "d");
        StdDraw.text(3.5 / 12, .3999, "f");
        StdDraw.text(4.5 / 12, .3999, "g");
        StdDraw.text(6.5 / 12, .3999, "j");
        StdDraw.text(7.5 / 12, .3999, "k");
        StdDraw.text(9.5 / 12, .3999, ";");
        StdDraw.text(10.5 / 12, .3999, ",");
        StdDraw.setPenColor(new Color(175, 175, 0));
        StdDraw.text(0.5 / 13, .0999, "z");
        StdDraw.text(1.5 / 13, .0999, "x");
        StdDraw.text(2.5 / 13, .0999, "c");
        StdDraw.text(3.5 / 13, .0999, "v");
        StdDraw.text(4.5 / 13, .0999, "b");
        StdDraw.text(5.5 / 13, .0999, "n");
        StdDraw.text(6.5 / 13, .0999, "m");
        StdDraw.text(7.5 / 13, .0999, ",");
        StdDraw.text(8.5 / 13, .0999, ".");
        StdDraw.text(9.5 / 13, .0999, "/");
        StdDraw.text(10.5 / 12, .0999, "space", 90.0);

    }
}
