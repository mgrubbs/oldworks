import java.util.Scanner;

/**
 * CS312 Assignment 4.
 *
 * On my honor, Mark Grubbs, this programming assignment is my own work and I
 * have not shared my solution with any other student in the class.
 *
 * A program to play Rock Paper Scissors
 *
 * email address: siegbalicula@gmail.com UTEID: msg2772 Number of slip days used
 * on this assignment:
 */

public class RockPaperScissors {

    /*
     * A program to allow a human player to play rock - paper - scissors against the
     * computer. If args.length != 0 then we assume the first element of args can be
     * converted to an int
     */

    // defines ROCK, PAPER, and SCISSORS as ints in order to more easily check
    // (instead of using strings)
    // and for clarity
    public static final int ROCK = 1;
    public static final int PAPER = 2;
    public static final int SCISSORS = 3;

    // calls playGame method
    public static void main(String[] args) {
        // CS312 Students. Do not change the following line.
        RandomPlayer computerPlayer = buildRandomPlayer(args);

        // CS312 students do no change the following line. Do not create any other
        // Scanners.
        Scanner keyboard = new Scanner(System.in);
        playGame(keyboard, computerPlayer); // passes scanner and randomplayer
        keyboard.close();
    }

    // gets player's name with scanner
    public static String takeInPlayerName(Scanner sc) {
        String name = sc.next();
        return name;
    }

    // gets number of rounds the player wants to play (expected > 0)
    public static int getNumRounds(Scanner sc) {
        int numRounds = sc.nextInt();
        return numRounds;
    }

    // plays the game and starts with the introduction
    // sc is passed to other methods to take in user input; expected integer put in
    // is greater than 0
    // rp uses a RandomPlayer object to get random choices to play against
    public static void playGame(Scanner sc, RandomPlayer rp) {
        System.out.println("Welcome to ROCK PAPER SCISSORS. I, Computer, will be your opponent.");
        System.out.print("Please type in your name and press return: ");
        String name = takeInPlayerName(sc); // sets player's name for use in other methods
        System.out.println();
        System.out.println("Welcome " + name + ".");
        System.out.println();
        System.out.println("All right " + name + ". " + "How many rounds would you like to play?");
        System.out.print("Enter the number of rounds you want to play and press return: ");
        int rounds = getNumRounds(sc); // sets amount of rounds player wants to play for use in other methods (expected
        // > 0)
        System.out.println();
        playRounds(sc, rp, rounds, name); // game actually starts playing the rounds with given parameters
    }

    // plays the rounds of the game based on user's amount of games they want to
    // play
    // sc is used to take in what the player wants to choose. Expected values range
    // from 1 - 3.
    // rp gets a random variable 1 - 3 to challenge the player
    // rounds is self-explanatory; used in other methods too (expected > 0)
    // name is the player's name
    public static void playRounds(Scanner sc, RandomPlayer rp, int rounds, String name) {
        int playerWins = 0; // these three variables are used later in displayResults
        int compWins = 0;
        for (int round = 1; round <= rounds; round++) { // runs amount of rounds given by player
            System.out.println("Round " + round + ".");
            System.out.println(name + ", please enter your choice for this round.");
            System.out.print("1 for ROCK, 2 for PAPER, and 3 for SCISSORS: ");
            final int playerChoice = sc.nextInt(); // player choice is expected to be 1 - 3
            final int computerChoice = rp.getComputerChoice(); // same for computer choice
            System.out.println("Computer picked " + getPlayerChoice(computerChoice) + ", " + name + " picked "
                    + getPlayerChoice(playerChoice) + "."); // get player choice turns the integer into its string
                                                            // equivalent
            System.out.println();
            int outcome = determineWinner(playerChoice, computerChoice); // calls method determineWinner with params
            // checks and prints outcome of round
            if (outcome == 0) {
                System.out.println("We picked the same thing! This round is a draw.");
            } else if (outcome == 1) {
                System.out.println("PAPER covers ROCK. I win.");
                compWins++;
            } else if (outcome == 2) {
                System.out.println("ROCK breaks SCISSORS. You win.");
                playerWins++;
            } else if (outcome == 3) {
                System.out.println("PAPER covers ROCK. You win.");
                playerWins++;
            } else if (outcome == 4) {
                System.out.println("SCISSORS cuts PAPER. I win.");
                compWins++;
            } else if (outcome == 5) {
                System.out.println("SCISSORS cut PAPER. You win.");
                playerWins++;
            } else if (outcome == 6) {
                System.out.println("ROCK breaks SCISSORS. I win.");
                compWins++;
            }
            System.out.println();
        }
        System.out.println(displayResults(compWins, playerWins, rounds, name));

    }

    // determine winner uses the player's choice and the computer's choice to
    // determine who wins
    // expected outcomes range from 0 - 6 and are labeled next to identifier winner
    // there are nine total outcomes, but three are the same (draw) so they lead to
    // the same int returned
    public static int determineWinner(int pc, int cc) {
        int winner = 0; // "We picked the same thing." (by default)
        if (pc == ROCK && cc == PAPER) {
            winner = 1; // "PAPER covers ROCK. I win."
        } else if (pc == ROCK && cc == SCISSORS) {
            winner = 2; // "ROCK breaks SCISSORS. You win."
        } else if (pc == PAPER && cc == ROCK) {
            winner = 3; // "PAPER covers ROCK. You win."
        } else if (pc == PAPER && cc == SCISSORS) {
            winner = 4; // "SCISSORS cuts PAPER. I win."
        } else if (pc == SCISSORS && cc == PAPER) {
            winner = 5; // "SCISSORS cut PAPER. You win."
        } else if (pc == SCISSORS && cc == ROCK) {
            winner = 6; // "ROCK breaks SCISSORS. I win."
        }
        return winner;
    }

    // converts int to string equivalent for printing
    public static String getPlayerChoice(int choice) {
        if (choice == ROCK) {
            return "ROCK";
        }
        if (choice == PAPER) {
            return "PAPER";
        }
        return "SCISSORS";
    }

    // prints results total rounds played, computer wins, player wins, and draws
    // also returns master of rock paper scissors
    public static String displayResults(int cW, int pW, int rounds, String name) {
        System.out.println(); // used to match output
        System.out.println("Number of games of ROCK PAPER SCISSORS: " + rounds);
        System.out.println("Number of times Computer won: " + cW);
        System.out.println("Number of times " + name + " won: " + pW);
        System.out.println("Number of draws: " + (rounds - (cW + pW)));
        if (cW > pW) {
            return "I, Computer, am a master at ROCK, PAPER, SCISSORS.";
        }
        if (cW < pW) {
            return "You, " + name + ", are a master at ROCK, PAPER, SCISSORS.";
        }
        return "We are evenly matched.";
    }

    /*
     * Build the random player. If args is length 0 then the default RandomPlayer is
     * built that follows a predictable sequence. If args.length > 0 then we assume
     * we can convert the first element to an int and build the RandomPlayer with
     * that initial value.
     */
    public static RandomPlayer buildRandomPlayer(String[] args) {
        if (args.length == 0) {
            return new RandomPlayer();
        } else {
            int seed = Integer.parseInt(args[0]);
            return new RandomPlayer(seed);
        }
    }
}