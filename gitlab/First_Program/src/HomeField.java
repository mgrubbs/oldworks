import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * CS312 Assignment 7.
 * 
 * On my honor, Mark Grubbs, this programming assignment is my own work and I have
 * not shared my solution with any other student in the class.
 *
 * A program to play determine the extend of home field advantage in sports.
 *
 *  email address: siegbalicula@gmail.com
 *  UTEID: msg2772
 *  Unique 5 digit course ID: 50160
 *  Grader name: Fatima
 *  Number of slip days used on this assignment: 1
 */

/**
 * Analysis of results. Include your write-up of results and analysis here:
 *  wbb14.txt had the statistics for the NCAA Women's Basketball games for the
 * 2013 - 2014 season. In the results of this set of data, the percentage of
 * games that were games with a home team was 90.6% with 14,305 of the 15,790
 * games having this quality. The number of games the home team won was 8,471
 * yielding a win percentage of 59.2%. The average difference in team scores was
 * 4.24. Playing at home gave a slight advantage. 
 *  wscc10.txt had the statistics for the NCAA Women's Soccer games for the 2010 
 * season. With 9,941 games being home team games out of the 10,593 games, 93.8% 
 * of the games were home team games. Of these home team games, 5,392 games were 
 * won by the home team giving a 54.2% win rate. The average difference in team 
 * scores was 0.51. Playing at home gave a slight advantage. 
 *  mlb12.txt had the statistics for the Major League Baseball games for the 2012 
 * season. 2,465 of the 2,467 games were home team games, which equated to 99.9% 
 * being home team games. Of the home team games, 1,312 games were won by the home 
 * team showing a 53.2% win rate. The average difference in team scores was 0.16. 
 * Similarly to previous results, playing at home gave a slight advantage. 
 *  mscc06.txt had the statistics for College Men's Soccer games in 2006. The number 
 * of games with a home team were 7,373 out of the 8,380 games played, yielding 
 * 88.0% games with a home team. The amount of games the home team won was 3,962, 
 * yielding a 57.3% win rate. The average difference in scores was 0.51. Again, 
 * like the other results, playing at home gave a slight advantage.
 *  In a smaller view, such as the 2016 NFL football season, 163 of the 166 games
 * were home football games, meaning 98.2% of games had a home team. Of the home
 * team games, 93 were won by the home team, equaling a 57.1% win rate. The 
 * average difference in team scores was 2.91. Yet again, playing at home gave 
 * a slight advantage.
 *  Overall, with this data, it can be observed that the "home field advantage" 
 * in sports is incredibly slight with increases of win rate less than 10%.
 * This result can be seen across many different sports such as basketball,
 * soccer, baseball, and football, meaning that this trait does not discriminate
 * among sports. Additionally, the level of professional play does not matter
 * either, as seen with this result still occurring in more pro play such as
 * the NFL but also lower levels of professional sports such as Collegiate 
 * soccer.
 */

public class HomeField {

    // Ask the user for the name of a data file and process
    // it until they want to quit.
    public static void main(String[] args) throws IOException {
        System.out.println("A program to analyze home field advantage in sports.");
        System.out.println();
        // CS312 students. Do not create any other Scanners connected to System.in.
        // Pass keyboard as a parameter to all the methods that need it.
        Scanner keyboard = new Scanner(System.in);

        // CS312 students - Add your code here
        analyzeFile(keyboard);
        keyboard.close();
    }

    // CS312 Students - Add your methods here.
    
    // Uses a while loop to accept files from the user until they want to stop
    // calls printOutHeader, printOutResults, getSportRes 
    // arg is keyboard which is passed to other methods and used to take in 
    // input
    public static void analyzeFile(Scanner keyboard) throws FileNotFoundException {
        char haveFiles = 'Y'; // start with Y already true
        while (haveFiles == 'Y' || haveFiles == 'y') {
            File sportsDoc = getSportRes(keyboard);
            System.out.println();
            Scanner fileScanner = new Scanner(sportsDoc);
            printOutHeader(fileScanner);
            System.out.println();
            printOutResults(fileScanner);
            System.out.println("Do you want to check another data set?");
            System.out.print("Enter Y or y to analyze another file, anything else to quit: ");
            haveFiles = (keyboard.nextLine()).charAt(0);
            System.out.println();
        }
    }
    
    // uses a while loop to ensure that the user gives a valid file name.
    // takes in keyboard scanner to get input and returns a file
    public static File getSportRes(Scanner keyboard) {
        System.out.print("Enter the file name: ");
        String fileString = keyboard.nextLine();
        File file = new File(fileString);
        while (!file.exists()) {
            System.out.println("Sorry, that file does not exist");
            System.out.print("Enter the file name: ");
            fileString = keyboard.nextLine();
            file = new File(fileString);
        }
        return file;
    }

    // prints out the header using a scanner connected to the file
    // given. Scanner param is of course a scanner for the file.
    public static void printOutHeader(Scanner fileScanner) {
        System.out.print("**********   "); 
        System.out.print(fileScanner.nextLine());
        System.out.print(" --- ");
        System.out.print(fileScanner.nextLine());
        System.out.print("   **********");
        System.out.println();
    }

    // takes in the param filescanner to scan the files of data
    // uses a while loop to perform line based analysis of the file
    public static void printOutResults(Scanner fileScanner) {
        System.out.println("HOME FIELD ADVANTAGE RESULTS");
        System.out.println();
        int gamesPlayed = 0;
        int homeGames = 0;
        int homeTeamScore = 0;
        int awayTeamScore = 0;
        int homeVictories = 0;
        int differenceBetweenScores = 0;
        while (fileScanner.hasNextLine()) {
            gamesPlayed++;
            String currentGame = fileScanner.nextLine();
            Scanner gameScanner = new Scanner(currentGame);
            gameScanner.next();
            // if the current game has a home game add to home
            // games
            if (currentGame.contains("@")) {
                homeGames++;
                String firstTeam = gameScanner.next();
                // if the first team is the home team, make their 
                // the home team and the next score the away team
                if (firstTeam.contains("@")) {
                    homeTeamScore = getTeamScore(gameScanner);
                    awayTeamScore = getTeamScore(gameScanner);
                // do the exact opposite in else
                } else {
                    awayTeamScore = getTeamScore(gameScanner);
                    homeTeamScore = getTeamScore(gameScanner);
                }
                homeVictories += didHomeWin(homeTeamScore, awayTeamScore);
                differenceBetweenScores += homeTeamScore - awayTeamScore;
            }
        }
        showGamesStats(gamesPlayed, homeGames, homeVictories, differenceBetweenScores);
    }

    // goes through the string scanner game to find the nearest score
    // with a while loop
    public static int getTeamScore(Scanner game) {
        int pointsToIncrease = 0;
        while (!game.hasNextInt()) {
            game.next();
        }
        pointsToIncrease = game.nextInt();
        return pointsToIncrease;
    }

    // in order to avoid putting another if statement in the
    // printOutResults method, I made this to check if the
    // home team won. returns one for count if they did or
    // zero if they didn't
    public static int didHomeWin(int homeScore, int awayScore) {
        if (homeScore > awayScore) {
            return 1;
        }
        return 0;
    }
    
    // takes in the ints from printOutResults as they are named
    // uses these args to show a formatted version that is like
    // the sample output
    public static void showGamesStats(int gamesPlayed, int homeGames, int homeVictories, int differenceBetweenScores) {
        System.out.printf("Total number of games: %,d%n", gamesPlayed);
        System.out.printf("Number of games with a home team: %,d%n", homeGames);
        System.out.printf("Percentage of games with a home team: %.1f%%%n", (1.0 * homeGames / gamesPlayed) * 100);
        System.out.printf("Number of games the home team won: %,d%n", homeVictories);
        System.out.printf("Home team win percentage: %.1f%%%n", (1.0 * homeVictories / homeGames) * 100);
        System.out.printf("Home team average margin: %.2f%n", 1.0 * differenceBetweenScores / homeGames);
        System.out.println();
    }
}
