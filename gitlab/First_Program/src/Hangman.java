
/**
 * CS312 Assignment 6.
 * 
 * On my honor, <NAME>, this programming assignment is my own work and I have
 * not shared my solution with any other student in the class.
 *
 * A program to play Hangman.
 *
 *  email address:
 *  UTEID:
 *  Unique 5 digit course ID:
 *  Number of slip days used on this assignment:
 */

import java.util.Scanner;

public class Hangman {

    public static final int GUESSES = 5;

    public static void main(String[] args) {
        PhraseBank phrases = buildPhraseBank(args);
        // CS312 Students -- Do not create any additional Scanners.
        Scanner keyboard = new Scanner(System.in);

        // CS312 students: add your code here
        playGame(phrases, keyboard);
        keyboard.close();
    }

    // CS312 students: add your methods here.

    // plays the game hangman a certain amount of times depending on how many times
    // the player wants to play; opens with intro and sends a PhraseBank and a
    // Scanner to other methods
    public static void playGame(PhraseBank pb, Scanner sc) {
        intro();
        playRounds(pb, sc);
    }

    // prints out the topic and spaces in the format shown in the given sample
    // outputs
    // uses PhraseBank object to get topic
    public static void showTopic(PhraseBank pb) {
        System.out.println();
        System.out.println("I am thinking of a " + pb.getTopic() + " ...");
        System.out.println();
    }

    // acquires a phrase from PhraseBank and returns it
    public static String getPhraseToGuess(PhraseBank pb) {
        return pb.getNextPhrase();
    }

    // runs a while loop that plays an undefined amount of rounds
    // passes pb and sc to other methods
    // methods called - showTopic, getPhraseToGuess, and playThePhrase
    public static void playRounds(PhraseBank pb, Scanner sc) {
        char currentlyWantToPlay = 'Y';
        while (currentlyWantToPlay == 'Y' || currentlyWantToPlay == 'y') {
            showTopic(pb);
            String currPhrase = (getPhraseToGuess(pb)).toUpperCase();
            playThePhrase(currPhrase, sc);
            System.out.println("Do you want to play again?");
            System.out.print("Enter 'Y' or 'y' to play again: ");
            currentlyWantToPlay = (sc.next()).charAt(0);
            sc.nextLine();
        }
    }

    // plays the last phrase that was returned from getPhraseToGuess
    // player will give input and other methods will error check input
    // cp is current phrase and scanner is used to take in input
    // printlns are formatted to match sample output
    // updates update what they show
    public static void playThePhrase(String cp, Scanner sc) {
        int amountOfWrongGuessesTaken = 0;
        final String comparableString = cp.toUpperCase();
        String lettersToPick = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // group of letters that can be chosen (changes
                                                             // dynamically)
        String secretPhrase = asteriskedPhrase(cp); // start with phrase being just asterisks
        System.out.println("The current phrase is " + secretPhrase);
        System.out.println();
        while (!secretPhrase.equals(comparableString) && amountOfWrongGuessesTaken < GUESSES) {
            showChoices(lettersToPick);
            System.out.print("Enter your next guess: ");
            String guess = sc.nextLine();
            char realGuess = guess.toUpperCase().charAt(0); // extracts char guess and makes it uppercase
            if (!lettersToPick.contains(guess.toUpperCase().substring(0, 1))) {
                badChoice(guess); // prints out that the input is bad
            } else if (!cp.contains(guess.toUpperCase().substring(0, 1))) {
                System.out.println();
                System.out.println("You guessed " + realGuess + ".");
                amountOfWrongGuessesTaken++;
                wrongChoice(amountOfWrongGuessesTaken); // prints out that the input is wrong but works
                lettersToPick = updateOptions(lettersToPick, realGuess);
                System.out.println("The current phrase is " + secretPhrase);
                System.out.println();
            } else {
                System.out.println();
                System.out.println("You guessed " + realGuess + ".");
                goodChoice(amountOfWrongGuessesTaken); // prints out that the input is in the phrase
                secretPhrase = updatePhrase(realGuess, secretPhrase, cp);
                lettersToPick = updateOptions(lettersToPick, realGuess);
                if (!secretPhrase.equals(comparableString)) {
                    System.out.println("The current phrase is " + secretPhrase);
                    System.out.println();
                }
            }
        }
        determineVictor(secretPhrase.equals(comparableString), comparableString); // prints out win or lose
    }

    // starts the hangman game off with a hidden phrase
    public static String asteriskedPhrase(String cp) {
        String asteriskedPhrase = "";
        for (int i = 0; i < cp.length(); i++) {
            if (cp.charAt(i) == '_') {
                asteriskedPhrase = asteriskedPhrase + "_";
            } else {
                asteriskedPhrase = asteriskedPhrase + "*";
            }
        }
        return asteriskedPhrase;
    }

    // the next three methods print out the different types of outcomes of an input:
    // bad - input is not in letters that can be chosen, wrong - input that is not
    // in the phrase, and good - input that is in the phrase

    public static void badChoice(String guess) {
        System.out.println();
        System.out.println(guess + " is not a valid guess.");
    }

    public static void wrongChoice(int amtWrong) {
        System.out.println();
        System.out.println("That is not present in the secret phrase.");
        System.out.println();
        System.out.println("Number of wrong guesses so far: " + amtWrong);
    }

    public static void goodChoice(int amtWrong) {
        System.out.println();
        System.out.println("That is present in the secret phrase.");
        System.out.println();
        System.out.println("Number of wrong guesses so far: " + amtWrong);
    }

    // changes the letters that you can choose over the course of the rounds
    public static String updateOptions(String letters, char rg) {
        if (letters.indexOf(rg) < letters.length()) {
            return letters.substring(0, letters.indexOf(rg)) + letters.substring(letters.indexOf(rg) + 1);
        }
        return letters.substring(0, letters.indexOf(rg));
    }

    // changes the hidden phrase as letters are guessed correctly
    public static String updatePhrase(char rg, String sp, String cp) {
        String newPhrase = sp;
        for (int i = 0; i < cp.length(); i++) {
            if (cp.charAt(i) == rg) {
                newPhrase = newPhrase.substring(0, i) + rg + newPhrase.substring(i + 1);
            }
        }
        return newPhrase;
    }

    // victory is determined by a boolean that is passed to this method; the boolean
    // is an equals method that checks equality between the phrase by the end of the
    // game and the actual phrase
    // cs is the phrase in uppercase; it is printed out
    public static void determineVictor(boolean check, String cs) {
        if (check) {
            System.out.println("The phrase is " + cs + ".");
            System.out.println("You win!!");
        } else {
            System.out.println("You lose. The secret phrase was " + cs);
        }
    }

    // shows the letter options the player has with hyphens in between
    public static void showChoices(String choices) {
        String fc = choices.charAt(0) + "";
        for (int i = 1; i < choices.length(); i++) {
            fc = fc + "--" + choices.charAt(i);
        }
        System.out.println("The letters you have not guessed yet are: ");
        System.out.println(fc);
        System.out.println();
    }

    // Build the PhraseBank.
    // If args is empty or null, build the default phrase bank.
    // If args is not null and has a length greater than 0
    // then the first elements is assumed to be the name of the
    // file to build the PhraseBank from.
    public static PhraseBank buildPhraseBank(String[] args) {
        PhraseBank result;
        if (args == null || args.length == 0 || args[0] == null || args[0].length() == 0) {
            result = new PhraseBank();
            // CS312 students, uncomment this line if you would like less predictable
            // behavior
            // from the PhraseBank during testing. NOTE, this line MUST be commented out
            // in the version of the program you turn in or your will fail all tests.
            // result = new PhraseBank("HangmanMovies.txt", true); // MUST be commented out
            // in version you submit.
        } else {
            result = new PhraseBank(args[0]);
        }

        return result;
    }

    // show the intro to the program
    public static void intro() {
        System.out.println("This program plays the game of hangman.");
        System.out.println();
        System.out.println("The computer will pick a random phrase.");
        System.out.println("Enter letters for your guess.");
        System.out.println("After 5 wrong guesses you lose.");
    }
}